using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class Fresco_Parametor_EtcParam_importer : AssetPostprocessor
{
    private static readonly string filePath = "Assets/Resources/Xml/Fresco_Parametor.xls";
    private static readonly string[] sheetNames = { "EtcParam", };
    
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (string asset in importedAssets)
        {
            if (!filePath.Equals(asset))
                continue;

            using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read))
            {
                var book = new HSSFWorkbook(stream);

                foreach (string sheetName in sheetNames)
                {
                    var exportPath = "Assets/Resources/Xml/" + sheetName + ".asset";
                    
                    // check scriptable object
                    var data = (EtcParam)AssetDatabase.LoadAssetAtPath(exportPath, typeof(EtcParam));
                    if (data == null)
                    {
                        data = ScriptableObject.CreateInstance<EtcParam>();
                        AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
                        data.hideFlags = HideFlags.NotEditable;
                    }
                    data.param.Clear();

					// check sheet
                    var sheet = book.GetSheet(sheetName);
                    if (sheet == null)
                    {
                        Debug.LogError("[QuestData] sheet not found:" + sheetName);
                        continue;
                    }

                	// add infomation
                    for (int i=2; i<= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        ICell cell = null;
                        
                        var p = new EtcParam.Param();
			
					cell = row.GetCell(0); p.NAME = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(1); p.COMMENT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.P0 = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(3); p.P1 = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(4); p.P2 = (float)(cell == null ? 0 : cell.NumericCellValue);

                        data.param.Add(p);
                    }
                    
                    // save scriptable object
                    ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
                    EditorUtility.SetDirty(obj);
                }
            }

        }
    }
}
