using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class Fresco_Parametor_PlayerData_importer : AssetPostprocessor
{
    private static readonly string filePath = "Assets/Resources/Xml/Fresco_Parametor.xls";
    private static readonly string[] sheetNames = { "PlayerData", };
    
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (string asset in importedAssets)
        {
            if (!filePath.Equals(asset))
                continue;

            using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read))
            {
                var book = new HSSFWorkbook(stream);

                foreach (string sheetName in sheetNames)
                {
                    var exportPath = "Assets/Resources/Xml/" + sheetName + ".asset";
                    
                    // check scriptable object
                    var data = (PlayerData)AssetDatabase.LoadAssetAtPath(exportPath, typeof(PlayerData));
                    if (data == null)
                    {
                        data = ScriptableObject.CreateInstance<PlayerData>();
                        AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
                        data.hideFlags = HideFlags.NotEditable;
                    }
                    data.param.Clear();

					// check sheet
                    var sheet = book.GetSheet(sheetName);
                    if (sheet == null)
                    {
                        Debug.LogError("[QuestData] sheet not found:" + sheetName);
                        continue;
                    }

                	// add infomation
                    for (int i=2; i<= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        ICell cell = null;
                        
                        var p = new PlayerData.Param();
			
					cell = row.GetCell(0); p.ID = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(1); p.WorldRank = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(2); p.Name_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.Name_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(4); p.Name_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(5); p.Nation = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(6); p.Gender = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(7); p.Height = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(8); p.Waight = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(9); p.Hand_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(10); p.Hand_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(11); p.Hand_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(12); p.BestShot_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(13); p.BestShot_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(14); p.BestShot_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(15); p.Power = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(16); p.Aggressive = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(17); p.Character = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(18); p.Rhythm = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(19); p.Technique = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(20); p.Stamina = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(21); p.Profile_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(22); p.Profile_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(23); p.Profile_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(24); p.Award_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(25); p.Award_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(26); p.Award_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(27); p.Level = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(28); p.FileName = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(29); p.Note = (cell == null ? "" : cell.StringCellValue);

                        data.param.Add(p);
                    }
                    
                    // save scriptable object
                    ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
                    EditorUtility.SetDirty(obj);
                }
            }

        }
    }
}
