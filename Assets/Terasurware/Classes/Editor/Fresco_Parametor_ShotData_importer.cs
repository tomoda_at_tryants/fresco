using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class Fresco_Parametor_ShotData_importer : AssetPostprocessor
{
    private static readonly string filePath = "Assets/Resources/Xml/Fresco_Parametor.xls";
    private static readonly string[] sheetNames = { "ShotData", };
    
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (string asset in importedAssets)
        {
            if (!filePath.Equals(asset))
                continue;

            using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read))
            {
                var book = new HSSFWorkbook(stream);

                foreach (string sheetName in sheetNames)
                {
                    var exportPath = "Assets/Resources/Xml/" + sheetName + ".asset";
                    
                    // check scriptable object
                    var data = (ShotData)AssetDatabase.LoadAssetAtPath(exportPath, typeof(ShotData));
                    if (data == null)
                    {
                        data = ScriptableObject.CreateInstance<ShotData>();
                        AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
                        data.hideFlags = HideFlags.NotEditable;
                    }
                    data.param.Clear();

					// check sheet
                    var sheet = book.GetSheet(sheetName);
                    if (sheet == null)
                    {
                        Debug.LogError("[QuestData] sheet not found:" + sheetName);
                        continue;
                    }

                	// add infomation
                    for (int i=2; i<= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        ICell cell = null;
                        
                        var p = new ShotData.Param();
			
					cell = row.GetCell(0); p.ID = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(1); p.SWINGTYPE_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.SWINGTYPE_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.SWINGTYPE_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(4); p.COMMENT_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(5); p.COMMENT_ENG = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(6); p.Howto_JPN = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(7); p.COMMENT_PRT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(8); p.POINT = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(9); p.COND_BEGIN_ACC_Y_1 = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(10); p.COND_UNDER = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(11); p.COND_BEGIN_ACC_Y_2 = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(12); p.COND_OVER = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(13); p.END_ACC_X = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(14); p.END_OVER_OR_UNDER_X = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(15); p.END_ACC_Y = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(16); p.END_OVER_OR_UNDER_Y = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(17); p.END_ACC_Z = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(18); p.END_OVER_OR_UNDER_Z = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(19); p.SE_HIT = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(20); p.SE_REACTION = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(21); p.SPEED = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(22); p.ACC_SPEED_PERFECT = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(23); p.ACC_SPEED_GOOD = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(24); p.ACC_SPEED_POOR = (int)(cell == null ? 0 : cell.NumericCellValue);

                        data.param.Add(p);
                    }
                    
                    // save scriptable object
                    ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
                    EditorUtility.SetDirty(obj);
                }
            }

        }
    }
}
