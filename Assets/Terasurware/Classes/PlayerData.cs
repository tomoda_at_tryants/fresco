using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerData : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public int WorldRank;
		public string Name_JPN;
		public string Name_ENG;
		public string Name_PRT;
		public string Nation;
		public string Gender;
		public int Height;
		public int Waight;
		public string Hand_JPN;
		public string Hand_ENG;
		public string Hand_PRT;
		public string BestShot_JPN;
		public string BestShot_ENG;
		public string BestShot_PRT;
		public float Power;
		public float Aggressive;
		public int Character;
		public float Rhythm;
		public float Technique;
		public float Stamina;
		public string Profile_JPN;
		public string Profile_ENG;
		public string Profile_PRT;
		public string Award_JPN;
		public string Award_ENG;
		public string Award_PRT;
		public int Level;
		public string FileName;
		public string Note;
	}
}