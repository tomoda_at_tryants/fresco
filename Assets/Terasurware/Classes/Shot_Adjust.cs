using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shot_Adjust : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public string ADJUST;
		public float MIN;
		public float MAX;
	}
}