using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtkChangeData : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public float Key;
		public int ChallengeTimes;
		public int NeedShotTimes_Min;
		public int NeedShotTimes_Max;
	}
}