using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EtcParam : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public string NAME;
		public string COMMENT;
		public float P0;
		public float P1;
		public float P2;
	}
}