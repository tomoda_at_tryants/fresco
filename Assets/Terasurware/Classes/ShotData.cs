using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShotData : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public string SWINGTYPE_JPN;
		public string SWINGTYPE_ENG;
		public string SWINGTYPE_PRT;
		public string COMMENT_JPN;
		public string COMMENT_ENG;
		public string Howto_JPN;
		public string COMMENT_PRT;
		public int POINT;
		public int COND_BEGIN_ACC_Y_1;
		public string COND_UNDER;
		public int COND_BEGIN_ACC_Y_2;
		public string COND_OVER;
		public int END_ACC_X;
		public string END_OVER_OR_UNDER_X;
		public int END_ACC_Y;
		public string END_OVER_OR_UNDER_Y;
		public int END_ACC_Z;
		public string END_OVER_OR_UNDER_Z;
		public string SE_HIT;
		public string SE_REACTION;
		public int SPEED;
		public int ACC_SPEED_PERFECT;
		public int ACC_SPEED_GOOD;
		public int ACC_SPEED_POOR;
	}
}