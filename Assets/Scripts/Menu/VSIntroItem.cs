﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VSIntroItem : MonoBehaviour {

	public UISprite			background;
	public List<UITexture>	iconList;
	public UILabel			rankLabel;
	public List<UILabel>	nameLabelList;
	public UILabel			pointLabel;

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="p_Icon"></param>
	/// <param name="p_RankLabel"></param>
	/// <param name="p_NameLabel"></param>
	public void		Set(List<GameInfo.stProfile> p_ProfileList)
	{
		int a_nIndex = 0;
		foreach(GameInfo.stProfile a_Profile in p_ProfileList)
		{
			if (!string.IsNullOrEmpty(a_Profile.fileName))
			{
				//	テクスチャをロード
				UITexture	a_Tex = ResourceManager.TextureLoadAndInstantiate("PlayerIcon/" + a_Profile.fileName);
				iconList[a_nIndex].mainTexture = a_Tex.mainTexture;

				Destroy(a_Tex.gameObject);
			}
            else
            {
                //	テクスチャをロード
                UITexture a_Tex = ResourceManager.TextureLoadAndInstantiate("PlayerIcon/" + "player_0");
                iconList[a_nIndex].mainTexture = a_Tex.mainTexture;

                Destroy(a_Tex.gameObject);
            }
			if(a_nIndex == 0)	rankLabel.text = a_Profile.worldRank.ToString();
			nameLabelList[a_nIndex].text = a_Profile.name;

			a_nIndex++;
		}
	}

	/// <summary>
	/// 点数を設定する
	/// </summary>
	/// <param name="p_nPonit"></param>
	public void	PointSet(int p_nPonit)
	{
		pointLabel.text = p_nPonit.ToString();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
