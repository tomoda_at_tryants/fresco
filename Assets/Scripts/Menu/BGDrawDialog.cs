﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGDrawDialog : MonoBehaviour {

	/// <summary>
	/// 列
	/// </summary>
	public static int		c_nRow		= 2;

	public GameObject	container;
	List<GameBGDraw>	m_BGList;

	public UIButtonTween	backButton;

	public UITexture		bgTex;

	public delegate void	CloseCallBack();
	/// <summary>
	/// 閉じた時に呼ばれるコールバック
	/// </summary>
	CloseCallBack	closeCallBack;

	/// <summary>
	/// 閉じた時に呼ばれるコールバックを設定する
	/// </summary>
	/// <param name="?"></param>
	public void	CloseCallBackSet(CloseCallBack p_CallBack)
	{
		closeCallBack += p_CallBack;
	}

	// Use this for initialization
	void	Start () {

		//	背景の読み込み
		bgTex.mainTexture = ResourceManager.TextureLoad("Bg/" + "bg_racketedit", false);
        GameUtils.WidgetSize_Set(bgTex.gameObject, def.c_BGSize);

		m_BGList = new List<GameBGDraw>();

		GameInfo.stBGDData	a_Data		= new GameInfo.stBGDData();
		GameBGDraw			a_BGDraw	= null;
		int					a_nID		= 0;
		//	オリジナル背景生成
		for (int i = 0; i < def.c_nOriginalBGNum; i++)
		{
			//	選択用の背景
			a_Data		= new GameInfo.stBGDData(a_nID, GameInfo.eBGType.Original, "");
			a_BGDraw	= BGDraw_Create(a_Data);
			m_BGList.Add(a_BGDraw);

			a_nID++;
		}

		//	デフォルト背景生成
		for (int i = 0; i < def.c_nDefaultBGNum; i++)
		{
			int	a_nBGNo = i + 1;

			//	選択用の背景
			string	a_FileName = string.Format("Bg/bg_{0}", a_nBGNo);
			a_Data = new GameInfo.stBGDData(a_nID, GameInfo.eBGType.Default, a_FileName);

			//	生成
			a_BGDraw = BGDraw_Create(a_Data);
			m_BGList.Add(a_BGDraw);

			a_nID++;
		}

		//	設定している背景の色を変える
		BGDraw_ViewUpdate();
	}

	//	背景データ生成
	public GameBGDraw BGDraw_Create(GameInfo.stBGDData p_BGData)
	{
		//	選択用の背景
		GameBGDraw	a_BGDraw = GameBGDraw.Create(p_BGData, container.transform);
		return a_BGDraw;
	}

	/// <summary>
	/// 設定している背景の色を変える
	/// </summary>
	public void BGDraw_ViewUpdate()
	{
		GameInfo.GameBGInfo gameBGInfo = GameInfo.instance.gameBGInfo;

		//	現在設定している背景の色を変える
		foreach (GameBGDraw a_BG in m_BGList)
		{
			if (gameBGInfo.BGData.id == a_BG.BGData.id)
			{
				a_BG.background.color = Color.yellow;
			}
			else
			{
				a_BG.background.color = Color.white;
			}
		}
	}

	bool m_bAnim;

	/// <summary>
	/// スタート時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator StartAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisable.instance.SetDisable(true);

		//	非表示
		gameObject.SetActive(true);

		//	Ｙ座標指定
		CSTransform.SetPos_Y(transform, -1136);

		yield return 0;

		float a_fTime = 0.6f;
		iTween.MoveTo(gameObject, iTween.Hash("y", 0, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeOutBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		m_bAnim = false;

		//	タッチ有効
		TouchDisable.instance.SetDisable(false);

		yield return 0;
	}

	/// <summary>
	/// 終了時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator EndAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisable.instance.SetDisable(true);

		float a_fTime = 0.6f;
		iTween.MoveTo(gameObject, iTween.Hash("y", -1136, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeInBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		//	タッチ有効
		TouchDisable.instance.SetDisable(false);

		//	コールバック
		if(closeCallBack != null)
		{
			closeCallBack();
		}
	}

	public void	BackButtonDidPush()
	{
		StartCoroutine(EndAnim());
	}
	
	// Update is called once per frame
	void Update () {
		//	設定されている背景の表示を更新する
		BGDraw_ViewUpdate();
	}
}
