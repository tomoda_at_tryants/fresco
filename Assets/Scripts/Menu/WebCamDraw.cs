﻿using UnityEngine;
using System.Collections;
using System.IO;

public class WebCamDraw : MonoBehaviour {

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",				"Menu/MenuBg",				false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("WebCamDraw",		"Editor/WebCamDraw",		false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		new Vector3(0, 0, -0.01f)),
		new PrefInstantiater.Layout("ShotButtonRoot",	"Editor/ShotButtonRoot",	false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		new Vector3(0, 0, -0.02f)),
	};

	static PrefInstantiater c_Instantiator;

	WebCamTexture	m_ViewTexture;

	ShotButtonRoot	m_ShotButtonRoot;

	byte[]	m_CurrentTexData;
	/// <summary>
	/// 現在の画像データ（撮影すると自動で更新されてしまうので）
	/// </summary>
	public byte[] currentTexData
	{
		get { return m_CurrentTexData; }
	}

	/// <summary>
	/// カメラの向き
	/// </summary>
	public enum eDirection
	{
		Out,
		In,

		Max
	}

	eDirection m_CamDirection;
	/// <summary>
	/// カメラの向き
	/// </summary>
	public eDirection	camDirection
	{
		get { return m_CamDirection; }
	}

	Texture2D	m_PhotoTex;
	/// <summary>
	/// 撮影したテクスチャ
	/// </summary>
	public Texture2D photoTex
	{
		get { return m_PhotoTex; }
	}

	int m_nID;
	/// <summary>
	/// ＩＤ
	/// </summary>
	public int	id
	{
		get { return m_nID; }
		set { m_nID = value; }
	}

	public System.Action<WebCamDraw>	ShotComplete;
	public System.Action				ShotCancel;

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static WebCamDraw Create(int p_nID, System.Action p_CancelCallBack, System.Action<WebCamDraw> p_CompleteCallback, eDirection p_Direction)
	{
		c_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		c_Instantiator.Build(c_Prefs);

		WebCamDraw	me = c_Instantiator.Get<WebCamDraw>("WebCamDraw");
		me.id = p_nID;
		me.Init(p_Direction);

		//	コールバックを設定
		me.ShotComplete = p_CompleteCallback;
		me.ShotCancel	= p_CancelCallBack;

		return me;
	}

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init(eDirection p_Direction)
    {
        //	カメラの向きを設定
        m_CamDirection = p_Direction;

        Vector3 a_iPhoneRev = Vector3.one;
        if (m_CamDirection == eDirection.Out)
        {
            transform.eulerAngles = new Vector3(0, -90, 90);

#if UNITY_IPHONE
            a_iPhoneRev = new Vector3(1, 1, -1);
#endif
        }
        else
        if (m_CamDirection == eDirection.In)
        {
            transform.eulerAngles = new Vector3(180, -90, 90);

#if UNITY_IPHONE
                        a_iPhoneRev = new Vector3(-1, 1, -1);
#endif
        }

        float   a_fScale	= (def.c_ScreenSize.y + 5) / 10f;	//	「+5」は実機で見たときに上下が少し足りないので
		transform.localScale    = new Vector3(a_fScale * a_iPhoneRev.x, 1 * a_iPhoneRev.y, a_fScale * a_iPhoneRev.z);

		ShotButtonRoot_Create();

		//	カメラの映像を表示
		StartCoroutine(PhotShot());

		//	タッチエフェクト制御
		TouchEffect.instance.enabled = false;
	}

	/// <summary>
	/// カメラ画面を表示する
	/// </summary>
	public IEnumerator PhotShot()
	{
		WebCamDevice[]	devices = WebCamTexture.devices;
		if (devices.Length == 0) yield break;

		//	カメラのインデックス（０：外側のカメラ　１：内側のカメラ）
		int	a_nDeviceIndex = (m_CamDirection == eDirection.Out) ? 0 : 1;

		m_ViewTexture = new WebCamTexture(devices[a_nDeviceIndex].name, (int)def.c_ScreenSize.x, (int)def.c_ScreenSize.y, def.c_FPS);
		Renderer a_Renderer = transform.GetComponent<Renderer>();

		//	テクスチャ設定
		a_Renderer.material.mainTexture = m_ViewTexture;

		//	表示
		m_ViewTexture.Play();

		//	開始するまで待機
		while (!m_ViewTexture.isPlaying) yield return 0;

		ShotButtonRoot_Create();
	}

	/// <summary>
	/// 撮影時に関するオブジェクト生成
	/// </summary>
	public void	ShotButtonRoot_Create()
	{
		m_ShotButtonRoot = c_Instantiator.Get<ShotButtonRoot>("ShotButtonRoot");

		//	イベント設定
		m_ShotButtonRoot.shotButton.onClickEventSet(this, "ShotButtonDidPush", "", 0);

		//	イベント設定
		m_ShotButtonRoot.cancelButton.onClickEventSet(this, "CancelButtonDidPush", "", 0);

		//	イベント設定
		m_ShotButtonRoot.switchButton.onClickEventSet(this, "SwitchButtonDidPush", "", 0);
	}

	/// <summary>
	/// 切り替えボタンが押された時の処理
	/// </summary>
	public void	SwitchButtonDidPush(UIButtonTween p_Sender)
	{
		//	現在起動しているカメラを停止させる
		if(m_ViewTexture != null)	m_ViewTexture.Stop();

		m_CamDirection = (eDirection)(((int)m_CamDirection + 1) % (int)eDirection.Max);

		//	初期化
		Init(m_CamDirection);
	}

	/// <summary>
	/// 撮影ボタンが押された時に呼ばれる
	/// </summary>
	public void ShotButtonDidPush(UIButtonTween p_Sender)
	{
		//	スクリーンショットを撮る
		StartCoroutine(ScreenShot());
	}

	/// <summary>
	/// スクリーンショットを撮る
	/// </summary>
	/// <returns></returns>
	public IEnumerator	ScreenShot()
	{
		//	ボタン類は非表示にする
		m_ShotButtonRoot.gameObject.SetActive(false);

		//	現在保存されている画像データを取得しておく
		bool	a_bExist = File.Exists(def.OriginalBGFilePath_Get(m_nID));
		m_CurrentTexData = (a_bExist) ? File.ReadAllBytes(def.OriginalBGFilePath_Get(m_nID)) : ResourceManager.TextureLoad(def.c_BG_NoImageName, false).EncodeToPNG();

		if(a_bExist)
		{
			File.Delete(def.OriginalBGFilePath_Get(m_nID));
		}

		//	スクリーンショットを撮る
		Application.CaptureScreenshot(def.OriginalBGFileName_Get(m_nID));

		//	インジケーターを表示
		GameUtils.ActiveIndicator(true);

		// スクリーンショットが保存されるまで待機 
		long	filesize		= 0;
		float	a_fTime			= 0f;
		float	a_fTimeLimit	= 20;
		while (filesize == 0)
		{
			a_fTime += Time.deltaTime;

			//	１０秒経過しても確認できないなら終了
			if(a_fTime >= a_fTimeLimit)	break;

			yield return 0;

			//	ファイルの存在を確認できるまで待機
			if(!File.Exists(def.OriginalBGFilePath_Get(m_nID)))
			{
				continue;
			}

			//ファイルのサイズを取得 
			System.IO.FileInfo	fi = new System.IO.FileInfo(def.OriginalBGFilePath_Get(m_nID));
			if (fi != null)	filesize = fi.Length;
		}
		//	１秒待機（上記で存在の確認ができても画像ファイルが正しく読み込み出来ないのでウェイトをかける）
		yield return new WaitForSeconds(1f);

		// インジケーター非表示
		GameUtils.ActiveIndicator(false);

		//	画像ファイルの存在を確認できないならタイトルシーンに遷移
		if (a_fTime >= a_fTimeLimit)	StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));

		//	画像データの読み込み
		byte[]	a_NewData	= File.ReadAllBytes(def.OriginalBGFilePath_Get(m_nID));
		//	画像データをローディング
		m_PhotoTex = new Texture2D(0, 0);
		m_PhotoTex.LoadImage(a_NewData);

		//	カメラを停止する
		if(m_ViewTexture != null)	m_ViewTexture.Stop();

		//	撮影が終了した時に呼ばれるコールバック（「FullScreenBGDraw.cs->ShotEnd()」）
		ShotComplete(this);

		yield return 0;
	}

	/// <summary>
	/// キャンセルボタンが押された時に呼ばれる
	/// </summary>
	public void CancelButtonDidPush(UIButtonTween p_Sender)
	{
//	バナー広告表示
#if !UNITY_EDITOR
		if(def.AdManager.bannerView != null)	def.AdManager.bannerView.Show();
#endif

		this.Destroy();
		//	カメラを停止する
		if (m_ViewTexture != null)	m_ViewTexture.Stop();

		//	タッチエフェクト表示
		TouchEffect.instance.enabled = true;

		//	キャンセルした時に呼ばれるコールバック
		ShotCancel();
	}

	/// <summary>
	/// 関連するオブジェクトを削除する
	/// </summary>
	public void	Destroy()
	{
		if(m_ShotButtonRoot != null)
		{
			m_ShotButtonRoot.gameObject.SetActive(true);
			Destroy(m_ShotButtonRoot.gameObject);
		}
		Destroy(gameObject);
	}
}
