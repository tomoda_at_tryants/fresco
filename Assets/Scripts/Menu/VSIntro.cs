﻿using UnityEngine;
using System.Collections;

public class VSIntro : MonoBehaviour {

	public VSIntroItem	playerItem;
	public VSIntroItem	userItem;

	/// <summary>
	/// イントロのアニメーションを実行する
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Animation_Exec(string p_Trigger)
	{
		Animator	a_Anim = transform.GetComponent<Animator>();

		//	アニメーション開始
		a_Anim.SetTrigger(p_Trigger);

		AnimationEvent	a_Event = GetComponent<AnimationEvent>();
		//	アニメーションが再生されるまで待機
		while(!a_Event.isAnimation)
		{
			yield return 0;
		}

		//	アニメーションが終了するまで待機
		yield return GameUtils.AnimEndWait(gameObject);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
