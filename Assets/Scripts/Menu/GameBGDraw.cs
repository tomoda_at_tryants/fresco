﻿using UnityEngine;
using System.Collections;
using System.IO;

public class GameBGDraw : MonoBehaviour {

	GameInfo.stBGDData			m_BGData;
	/// <summary>
	/// 背景データ
	/// </summary>
	public GameInfo.stBGDData	BGData
	{
		get { return m_BGData; }
	}

	public UITexture		tex;
	public UILabel			titleLabel;
	public UIButtonTween	BgButton;
	public UISprite			background;
	public UISprite			cameraIcon;

    /// <summary>
    /// オリジナル画像が存在するか
    /// </summary>
    public bool OriginalBG_ExistImage
    {
        get
        {
            bool    m_bExistImage = false;
            if(m_BGData.type == GameInfo.eBGType.Original)
            {
                //	背景のパス
                string  a_BGPath    = def.OriginalBGFilePath_Get(m_BGData.id);
                //  パス先に画像が存在しない
                if (!File.Exists(a_BGPath)) return false;

                byte[]  a_Byte      = File.ReadAllBytes(a_BGPath);
                m_bExistImage       = (a_Byte.Length > 0);
            }
   
            return m_bExistImage;
        }
    }

	/// <summary>
	/// サイズ
	/// </summary>
	public Vector2	size
	{
		get
		{
			if (background == null)	return Vector2.zero;
			return new Vector2(background.width, background.height);
		}
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <param name="p_TextureName"></param>
	/// <param name="p_Title"></param>
	/// <returns></returns>
	public static GameBGDraw Create(GameInfo.stBGDData p_BGData, Transform p_Parent)
	{
		GameObject	a_Obj	= ResourceManager.PrefabLoadAndInstantiate("Editor/GameBGDraw", Vector3.zero, p_Parent);
		GameBGDraw	me		= a_Obj.GetComponent<GameBGDraw>();
		//	設定
		me.Set(p_BGData);

		return	me;
	}

	/// <summary>
	/// 設定
	/// </summary>
	/// <param name="p_TextureName"></param>
	/// <param name="p_Title"></param>
	public void Set(GameInfo.stBGDData p_BGData)
	{
		//	背景データ
		m_BGData = p_BGData;
		
		//	オリジナル
		if (m_BGData.type == GameInfo.eBGType.Original)
		{
			//	背景のパス
			string  a_BGPath = def.OriginalBGFilePath_Get(m_BGData.id);
	
			//	スクリーンショットの読み込み 
			if (File.Exists(a_BGPath))
			{
				byte[]  a_Byte = File.ReadAllBytes(a_BGPath);
				if (a_Byte.Length > 0)
				{
					Texture2D   a_LoadTex = new Texture2D(0, 0);
					a_LoadTex.LoadImage(a_Byte);
					tex.mainTexture = a_LoadTex;
				}
			}
            else
            {
                //  テクスチャ設定
                SetTexture(def.c_BG_NoImageName);
            }

			HMLocalize	a_Localize = GetComponent<HMLocalize>();
			a_Localize.labelInvalid = true;	//	オリジナル背景のみラベル設定を無効にする
			a_Localize.targetLabel	= null;

			//タイトルの設定
			string	a_Key = string.Format("OriginalBG_Title_{0}", GameInfo.instance.language.ToString());
			titleLabel.text = Localization.Get(a_Key);
		}
		else
		//	デフォルト
		if (m_BGData.type == GameInfo.eBGType.Default)
		{
            //  テクスチャ設定
            SetTexture();
            //	カメラアイコンは非表示にする
            cameraIcon.gameObject.SetActive(false);
		}
		
		//	「GeneralData」設定
		GeneralData	a_Data	= BgButton.GetComponent<GeneralData>();
		//	テクスチャ名設定
		a_Data.stringValue	= m_BGData.texName;
		//	背景のタイプを設定
		a_Data.objValue		= m_BGData.texName;

		//	アニメーションタイプ設定
		BgButton.animationType = 1;
	}

    public void SetTexture(string p_TexName = "")
    {
        string  a_TexName = (string.IsNullOrEmpty(p_TexName)) ? m_BGData.texName : p_TexName;
        //	テクスチャの設定
        tex.mainTexture = ResourceManager.TextureLoad(a_TexName, false);
    }

	/// <summary>
	/// 背景が押された時の処理
	/// </summary>
	public void	BGButtonDidPush()
	{
		UITexture			a_UITex		= (m_BGData.type == GameInfo.eBGType.Default) ? null : tex;
		Texture2D			a_Tex		= (a_UITex != null) ? (Texture2D)a_UITex.mainTexture : null;

		//	フルサイズの背景生成
		FullScreenBGDraw.Create(this, a_Tex);
	}
}
