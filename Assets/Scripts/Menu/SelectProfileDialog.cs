﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectProfileDialog : MonoBehaviour
{
	/// <summary>
	///	表示している情報
	/// </summary>
	public enum eViewType : int
	{
		PLAYER,
		USER
	}

	eViewType	m_ViewType;
	/// <summary>
	/// 表示している情報
	/// </summary>
	public eViewType		viewType
	{
		get { return m_ViewType; }
	}

	int m_nID;
	/// <summary>
	/// ＩＤ
	/// </summary>
	public int	id
	{
		get { return m_nID; }
	}

	public UISprite		background;
	public UILabel		rankLabel;
	public GameObject	fade;
	public GameObject	winObj;

	List<SelectProfileDraw>			m_ProfileDrawList;
	public List<SelectProfileDraw>	profileDrawList
	{
		get { return m_ProfileDrawList; }
	}

	List<GameInfo.stProfile>		m_ProfileList;
	public List<GameInfo.stProfile>	profileList
	{
		get { return m_ProfileList; }
	}

    public GameObject listObj
    {
        get { return m_ListObj; }
        set { m_ListObj = value; }
    }
    GameObject  m_ListObj;

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static SelectProfileDialog Create(GameObject p_List, int p_nID, eViewType p_Type, SelectProfileList.eViewType p_ListViewType, List<GameInfo.stProfile> p_ProfileList, Transform p_Parent, 
											 SelectProfileDraw.ChangePartnerOpenCallBack p_ChangePartnerOpenCallBack = null, SelectProfileDraw.ChangePartnerSetCallBack p_ChangePartnerSetCallBack = null)
	{
		//	プロフィール表示オブジェクト生成
		GameObject			a_Obj	= ResourceManager.PrefabLoadAndInstantiate("Select/SelectProfileDialog", Vector2.zero, p_Parent);
		SelectProfileDialog me		= a_Obj.GetComponent<SelectProfileDialog>();

        me.listObj = p_List;

		me.Init(p_nID, p_Type, p_ListViewType, p_ProfileList, p_ChangePartnerOpenCallBack, p_ChangePartnerSetCallBack);

		return  me;
	}

	/// <summary>
	/// フェードの設定を行う
	/// </summary>
	/// <param name="p_ProfileList"></param>
	public void	FadeSet(SelectProfileList.eViewType p_ListViewType, List<GameInfo.stProfile> p_ProfileList)
	{
        //	パートナー用に表示している場合はマスクをかけない
        int		a_nUserRank		= PlayerInfo.instance.userRank;
		bool	a_bTookRank1	= PlayerInfo.instance.isRank1;
		foreach (GameInfo.stProfile p_Profile in p_ProfileList)
		{
			//	まだ倒してない選手はフェードを非表示にする
			if ((p_ListViewType != SelectProfileList.eViewType.Partner && (a_nUserRank - 1 > p_Profile.worldRank)) ||
				((p_ListViewType == SelectProfileList.eViewType.Partner) && (a_nUserRank > p_Profile.worldRank)))
			{
				if(!a_bTookRank1)
				{
					fade.SetActive(true);
				}
			}
			else
			//	既に倒した選手のダイアログは勝利用マスクをかける（対戦選手表示時のみ）
			if (a_nUserRank < p_Profile.worldRank && p_ListViewType == SelectProfileList.eViewType.Game)
			{
				winObj.SetActive(true);
				GameObject a_Fade = winObj.transform.FindChild("fade").gameObject;
				a_Fade.SetActive(true);
			}
		}
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(int p_nID, eViewType p_Type, SelectProfileList.eViewType p_ListViewType, List<GameInfo.stProfile> p_ProfileList, SelectProfileDraw.ChangePartnerOpenCallBack p_ChangePartnerOpenCallBack = null, 
					 SelectProfileDraw.ChangePartnerSetCallBack p_ChangePartnerSetCallBack = null)
	{
		m_nID = p_nID;

		m_ViewType = p_Type;
		if(m_ViewType == eViewType.USER)
		{
            //  スプライト更新
            background.spriteName = "img_base_y_a";
        }
		else
		{
			//	フェードの設定
			FadeSet(p_ListViewType, p_ProfileList);
		}

		//	ランク設定
		rankLabel.text = p_ProfileList[0].worldRank.ToString();

		//	ＣＯＭの場合はランクを「 - 」にする
		PlayerData.Param	a_Param = def.instance.playerTable.Param_Get(p_ProfileList[0].id);
		if (a_Param != null && def.instance.playerTable.isCOM(a_Param))	rankLabel.text = " - ";

		//	ユーザーのパートナー情報
		GameInfo.stProfile	a_PartnerProfile	= GameInfo.instance.partnerProfile;
		bool				a_bSetPartner		= false;

		m_ProfileDrawList	= new List<SelectProfileDraw>();
		m_ProfileList		= new List<GameInfo.stProfile>();
		int a_nIndex = 0;
		foreach(GameInfo.stProfile a_Profile in p_ProfileList)
		{
			//	生成
			SelectProfileDraw   a_Draw = SelectProfileDraw.Create(a_nIndex, this, background.transform, a_Profile);

			//	Ｙ座標調整
			float	a_fY = (a_nIndex == 0) ? 20 : -60;
			CSTransform.SetPos_Y(a_Draw.transform, a_fY);

			//	ユーザーはアイコンのタッチを制御する
			if(m_ViewType == eViewType.USER)
			{
                //  スプライト更新
                a_Draw.background.spriteName = "img_base_plate_b";

				if(a_nIndex == 0)
				{
					//	コライダーをoffにする
					a_Draw.iconButton.GetComponent<BoxCollider>().enabled = false;
				}
				else
				//	ユーザーのパートナーは変更ボタンを表示する
				if(a_nIndex == 1)
				{
					//	既にパートナーが設定されている場合は更新
					if (a_PartnerProfile.isEnableData)
					{
						string	a_Name = def.instance.playerTable.Name_Get(a_PartnerProfile.id);
						a_PartnerProfile.name = a_Name;

						a_Draw.Init(a_nIndex, a_PartnerProfile);
						a_bSetPartner = true;
					}

					a_Draw.changeButton.gameObject.SetActive(true);
					if(p_ChangePartnerOpenCallBack != null)
					{
						//	ボタンが押された時のデリゲートを設定する
						a_Draw.ChangePartnerOpenCallBack_Set(p_ChangePartnerOpenCallBack);
					}
				}
			}
			else
			{
				//	パートナー設定画面は設定ボタンを表示する
				if (p_ChangePartnerSetCallBack != null && p_ListViewType == SelectProfileList.eViewType.Partner)
				{
					a_Draw.partnerSetButton.gameObject.SetActive(true);
					//	デリゲート設定
					a_Draw.ChangePartnerSetCallBack_Set(p_ChangePartnerSetCallBack);
				}
			}

			m_ProfileDrawList.Add(a_Draw);
			m_ProfileList.Add((a_bSetPartner) ? a_PartnerProfile : a_Profile);

			a_nIndex++;
		}
	}

	/// <summary>
	/// 「Win」を表示するアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Lose_Exec()
	{
		//	選手のランクを１つ下げる
		int	a_nRank = m_ProfileList[0].worldRank;
		a_nRank++;
		rankLabel.text = a_nRank.ToString();
		
		winObj.transform.localScale = new Vector3(2, 2);
		winObj.SetActive(true);

		iTween.ScaleTo(winObj, iTween.Hash("x", 1, "y", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));

		yield return new WaitForSeconds(0.2f);

		GameObject	a_Fade = winObj.transform.FindChild("fade").gameObject;
		a_Fade.SetActive(true);
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	
	}
}
