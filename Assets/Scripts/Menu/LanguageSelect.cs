﻿using UnityEngine;
using System.Collections;

public class LanguageSelect : MonoBehaviour {

	public GameObject		background;
	public GameObject		buttonRoot;

	public UITexture		bgTex;

	public UIButtonTween	JPNButton;
	public UIButtonTween	ENGButton;
	public UIButtonTween	PRTButton;
	public UIButtonTween	BackButton;

	bool	m_bAnim;

	bool	m_bOpen;

	/// <summary>
	/// 開いているか
	/// </summary>
	public bool	isOpen
	{
		get { return m_bOpen; }
	}

	/// <summary>
	/// サイズ
	/// </summary>
	Vector2	m_Size
	{
		get
		{
			Vector2	a_Size = GameUtils.WidgetSize_Get(background);
			return	a_Size;
		}
	}
	
	/// <summary>
	/// Ｙ座標の初期位置
	/// </summary>
	float	m_DefaultPos_Y
	{
		get
		{
			return	-(def.c_ScreenSize.y / 2) + -(m_Size.y / 2);
		}
	}

	// Use this for initialization
	void Start()
	{
		bgTex.mainTexture = ResourceManager.TextureLoad("Common/" + "wnd_laug", false);
		bgTex.GetComponent<UIWidget>().MakePixelPerfect();
	}

	/// <summary>
	/// ボタンを無効にする
	/// </summary>
	/// <param name="p_bDisable"></param>
	public void	TouchDisableSet(bool p_bDisable)
	{
		BoxCollider[]	a_Col = background.GetComponentsInChildren<BoxCollider>();

		foreach(BoxCollider col in a_Col)
		{
			col.enabled = !p_bDisable;
		}
	}

	/// <summary>
	/// ボタンが押された時に呼ばれる処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	ButtonDidPush(UIButtonTween p_Sender)
	{
		//	アニメーション中はタッチできない
		if(m_bAnim)	return;

		GeneralData	a_Data	= p_Sender.GetComponent<GeneralData>();
		string		a_Key	= a_Data.stringValue;

		GameInfo.eLanguage	a_Language = GameUtils.EnumGet<GameInfo.eLanguage>(a_Key);

		//	言語を設定
		GameInfo.instance.Language_Set(a_Language);

		//	ウィンドウを閉じる
		StartCoroutine(EndAnim());
	}

	/// <summary>
	/// スタート時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	StartAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisableSet(true);

		//	非表示
		gameObject.SetActive(true);

		//	Ｙ座標指定
		CSTransform.SetPos_Y(background.transform, m_DefaultPos_Y);

		yield return 0;

		float	a_fTime = 0.6f;
		iTween.MoveTo(background, iTween.Hash("y", 0, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeOutBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		m_bAnim = false;
		m_bOpen = true;

		//	タッチ許可
		TouchDisableSet(false);

		yield return 0;
	}

	public void	BackButtonDidPush()
	{
		StartCoroutine(EndAnim());
	}

	/// <summary>
	/// 終了時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	EndAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisableSet(true);

		float	a_fTime = 0.6f;
		iTween.MoveTo(background, iTween.Hash("y", m_DefaultPos_Y, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeInBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		//	タッチ許可
		TouchDisableSet(false);

		//	非表示
		gameObject.SetActive(false);

		m_bAnim = false;
		m_bOpen = false;
	}
}
