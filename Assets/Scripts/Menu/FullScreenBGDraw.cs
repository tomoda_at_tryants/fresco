﻿using UnityEngine;
using System.Collections;
using System.IO;

/// <summary>
/// 「scn_editor」で選択された背景をフルスクリーンで表示するためのクラス
/// </summary>
public class FullScreenBGDraw : MonoBehaviour {

	/// <summary>
	/// 背景を選択した時に表示するルート
	/// </summary>
	public GameObject				touchRoot;

	/// <summary>
	/// 撮影後に表示するルート
	/// </summary>
	public GameObject				shotRoot;

	public UITexture				tex;
	public UIButtonTween			setButton;
	public UIButtonTween			backButton;
	public UIButtonTween			photoButton;

	/// <summary>
	/// 背景データ
	/// </summary>
	public GameInfo.stBGDData	BGData
	{
		get 
		{
			if (m_BGDraw == null) return new GameInfo.stBGDData();
			return m_BGDraw.BGData;
		}
	}

	GameBGDraw	m_BGDraw;
	/// <summary>
	///	背景画像の一覧に表示されているオブジェクト
	/// </summary>
	public GameBGDraw	BGDraw
	{
		get { return m_BGDraw; }
		set { m_BGDraw = value; }
	}

	WebCamDraw	m_WebCamDraw;
	/// <summary>
	/// 撮影データ
	/// </summary>
	public WebCamDraw	webCamDraw
	{
		get { return m_WebCamDraw; }
	}

	/// <summary>
	///	撮影されたテクスチャデータ
	/// </summary>
	byte[]	m_CurrentShotTexData;

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",				"Menu/MenuBg",				false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("FullScreenBGDraw",	"Editor/FullScreenBGDraw",	false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
	};

	static PrefInstantiater c_Instantiator;

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static FullScreenBGDraw Create(GameBGDraw p_GameBGDraw, Texture2D p_Tex = null)
	{
		c_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		c_Instantiator.Build(c_Prefs);

		FullScreenBGDraw me = c_Instantiator.Get<FullScreenBGDraw>("FullScreenBGDraw");

		me.BGDraw	= p_GameBGDraw;
		me.Init(me.BGData, p_Tex);

		return	me;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(GameInfo.stBGDData p_BGData, Texture2D p_Tex = null)
	{
		StartCoroutine(Set(p_BGData, p_Tex));
	}

	/// <summary>
	/// 設定
	/// </summary>
	/// <param name="p_TextureName"></param>
	/// <returns></returns>
	public IEnumerator Set(GameInfo.stBGDData p_BGData, Texture2D p_Tex = null)
	{
		if(p_Tex == null)
		{
			if(m_BGDraw.BGData.type == GameInfo.eBGType.Default)
			{
				bool a_bWait = true;
				//	テクスチャの設定
				ResourceManager.TextureLoadAsync(p_BGData.texName, "png", 0, (res, tag) =>
				    {
					    tex.mainTexture = res;
					    a_bWait = false;
				    },
				false);

                GameUtils.WidgetSize_Set(tex.gameObject, def.c_BGSize);

				//	画像のローディングが終了するまで待機
				while (a_bWait) yield return 0;
			}
			else
			{
				setButton.gameObject.SetActive(false);
			}
		}
		else
		{
			tex.mainTexture = p_Tex;
            //  テクスチャがnullかオリジナル背景が見つからない
			if(tex.mainTexture == null || !BGDraw.OriginalBG_ExistImage)
			{
				setButton.gameObject.SetActive(false);
			}
		}

		//	開く
		StartCoroutine(Open());
	}

    /// <summary>
    /// 既に撮影したデータが存在するか
    /// </summary>
    bool m_bCurrentExistTex;

	/// <summary>
	/// 「撮影」ボタンが押された時の処理
	/// </summary>
	public void	PhotoButtonDidPush()
	{
        //  バナー広告を非表示にする
        def.AdManager.Banner_SetActive(false);

        gameObject.SetActive(false);	//	自身を非表示にする
		m_WebCamDraw = WebCamDraw.Create(m_BGDraw.BGData.id, ShotCancel, ShotComplete, WebCamDraw.eDirection.Out);

        //  既に撮影した画像が存在するか
        m_bCurrentExistTex = m_BGDraw.OriginalBG_ExistImage;
	}

	/// <summary>
	/// 撮影が終了した時に呼ばれる処理
	/// </summary>
	/// <returns></returns>
	public void	ShotComplete(WebCamDraw p_WebCamDraw)
	{
		m_WebCamDraw = p_WebCamDraw;

		//	撮影した画像を反映させる
		tex.mainTexture = m_WebCamDraw.photoTex;

		//	撮影後のボタンを表示する
		touchRoot.SetActive(false);
		shotRoot.SetActive(true);

		//	自身を表示する
		gameObject.SetActive(true);

		//	テクスチャデータを取得する
        if(m_bCurrentExistTex)
        {
            m_CurrentShotTexData = p_WebCamDraw.currentTexData;
        }

		//	撮影に使用していたオブジェクトを全て削除する
		p_WebCamDraw.gameObject.SetActive(false);

		//	タッチエフェクト表示
		TouchEffect.instance.enabled = true;
	}

	/// <summary>
	/// 撮影をキャンセルした場合の処理
	/// </summary>
	public void	ShotCancel()
	{
		Destroy(gameObject);
	}

	/// <summary>
	/// 「セット」ボタンが押された時の処理
	/// </summary>
	public void SetButtonDidPush()
	{
		GameInfo.instance.gameBGInfo.Set(m_BGDraw.BGData);
		StartCoroutine(Close());
	}

	/// <summary>
	/// 「戻る」ボタンが押された時の処理
	/// </summary>
	public void	BackButtonDidPush(UIButtonTween p_Sender)
	{
		GeneralData	a_Data = p_Sender.GetComponent<GeneralData>();

		bool	a_bExistData = (m_WebCamDraw != null && m_WebCamDraw.currentTexData != null) ? true : false;
		//	撮影後に表示されているボタンの時はファイルに画像データを書き込む
		if(a_Data.stringValue == "Shot" && a_bExistData)
		{
			//	現在保存されているものは撮影された画像に更新されてしまっているので
			//	撮影する前の画像データに戻しておく
			File.WriteAllBytes(def.OriginalBGFilePath_Get(m_BGDraw.BGData.id), m_WebCamDraw.currentTexData);

            //	バナー広告表示
            def.AdManager.Banner_SetActive(true);

            if (m_CurrentShotTexData == null)
            {
                //  ファイルを削除する
                File.Delete(def.OriginalBGFilePath_Get(m_BGDraw.BGData.id));
            }
        }

        StartCoroutine(Close());
	}

	/// <summary>
	/// 「保存」ボタンが押された時の処理
	/// </summary>
	public void SaveButtonDidPush()
	{
        //	バナー広告表示
        def.AdManager.Banner_SetActive(true);

        //	一覧で表示している背景画像を更新する
        m_BGDraw.tex.mainTexture = m_WebCamDraw.photoTex;

		//	閉じる
		StartCoroutine(Close());
	}

	/// <summary>
	/// やり直しボタンが押された時の処理
	/// </summary>
	public void	RetryButtonDidPush()
	{
        if(m_CurrentShotTexData == null)
        {
            //  ファイルを削除する
            File.Delete(def.OriginalBGFilePath_Get(m_BGDraw.BGData.id));
        }
        else
        {
            //	現在保存されているものは撮影された画像に更新されてしまっているので
            //	撮影する前の画像データに戻しておく
            File.WriteAllBytes(def.OriginalBGFilePath_Get(m_BGDraw.BGData.id), m_CurrentShotTexData);
        }

		//	撮影用オブジェクト作成
		m_WebCamDraw = WebCamDraw.Create(m_BGDraw.BGData.id, ShotCancel, ShotComplete, WebCamDraw.eDirection.Out);

		//	自身を非表示にする
		gameObject.SetActive(false);
	}

	/// <summary>
	/// 開くアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Open(bool p_bAnim = true)
	{
		if(p_bAnim)
		{
			Vector2 a_Size = GameUtils.WidgetSize_Get(tex.gameObject);
			CSTransform.SetPos_Y(transform, -a_Size.y);

			//	ボタンは非表示
			touchRoot.SetActive(false);

			yield return StartCoroutine(Animation_Exec(true));

			//	ボタンを表示
			touchRoot.SetActive(true);

			//	デフォルト画像の時はボタンを表示しない
			if (BGData.type == GameInfo.eBGType.Default)
			{
				photoButton.gameObject.SetActive(false);
			}
		}
		else
		{
			transform.localPosition = Vector3.zero;
		}
	}

	/// <summary>
	/// 閉じるアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Close()
	{
		yield return StartCoroutine(Animation_Exec(false));

		//	自身を削除
		Destroy(gameObject);
	}

	/// <summary>
	/// アニメーション
	/// </summary>
	/// <param name="p_bOpen"></param>
	/// <returns></returns>
	public IEnumerator	Animation_Exec(bool p_bOpen)
	{
		float	a_fTime = 0.5f;
		Vector2 a_Size	= GameUtils.WidgetSize_Get(tex.gameObject);
		float	a_fTo	= (p_bOpen) ? 0 : -a_Size.y;
		iTween.EaseType a_Type = (p_bOpen) ? iTween.EaseType.easeOutBack : iTween.EaseType.easeInBack;

		iTween.MoveTo(gameObject, iTween.Hash("y", a_fTo, "time", a_fTime, "easetype", a_Type, "islocal", true));

		//	タッチ不可
		TouchDisable.instance.SetDisable(true);

		yield return new WaitForSeconds(a_fTime + 0.1f);

		//	タッチ許可
		TouchDisable.instance.SetDisable(false);

		yield return 0;
	}
}
