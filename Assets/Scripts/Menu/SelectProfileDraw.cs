﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 選択画面で表示される選手プロフィールを扱うクラス
/// </summary>
public class SelectProfileDraw : MonoBehaviour {

	GameInfo.stProfile		m_Profile;
	/// <summary>
	///	情報
	/// </summary>
	public GameInfo.stProfile	info
	{
		get { return m_Profile; }
	}

	/// <summary>
	/// パートナーでない選手の枠色
	/// </summary>
	Color	m_NonPartnerColor	= new Color(158f / 255f, 251f / 255f, 255f / 255f);

	/// <summary>
	/// パートナー選手の枠色
	/// </summary>
	 Color	m_PartnerColor		= new Color(255f / 255f, 111f / 255f, 0f / 255f);

	public UITexture		icon;
	public UISprite			background;
	public UIButtonTween	iconButton;
	public UIButtonTween	changeButton;
	public UIButtonTween	partnerSetButton;
    public UISprite         partnerOn;
	public UILabel			nameLabel;

	public delegate void	ChangePartnerOpenCallBack();
	/// <summary>
	/// パートナーの変更を行うデリゲート
	/// </summary>
	ChangePartnerOpenCallBack	changePartnerOpenCallBack;

	/// <summary>
	/// デリゲートの設定を行う
	/// </summary>
	public void ChangePartnerOpenCallBack_Set(ChangePartnerOpenCallBack p_CallBack)
	{
		changePartnerOpenCallBack += p_CallBack;
	}

	public delegate void ChangePartnerSetCallBack(SelectProfileDraw p_Draw);
	/// <summary>
	/// パートナーの変更を行うデリゲート
	/// </summary>
	ChangePartnerSetCallBack changePartnerSetCallBack;

	/// <summary>
	/// デリゲートの設定を行う
	/// </summary>
	/// <param name="p_CallBack"></param>
	public void	ChangePartnerSetCallBack_Set(ChangePartnerSetCallBack p_CallBack)
	{
		changePartnerSetCallBack += p_CallBack;
	}

    public SelectProfileDialog  dialog
    {
        get { return m_Dialog; }
        set { m_Dialog = value; }
    }
    SelectProfileDialog m_Dialog;

	/// <summary>
	/// 生成
	/// </summary>
	/// <param name="p_Parent"></param>
	/// <param name="p_Profile"></param>
	/// <returns></returns>
	public static SelectProfileDraw	Create(int p_nIndex, SelectProfileDialog p_Dialog, Transform p_Parent, GameInfo.stProfile p_Profile)
	{
		//	プロフィール表示オブジェクト生成
		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate("Select/SelectProfileDraw", Vector2.zero);
		a_Obj.name = "SelectProfileDraw";
		GameUtils.AttachChild(p_Parent.gameObject, a_Obj);

		SelectProfileDraw	a_Draw = a_Obj.GetComponent<SelectProfileDraw>();
        //	初期化
        a_Draw.dialog = p_Dialog;
        a_Draw.Init(p_nIndex, p_Profile);

		return	a_Draw;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(int p_nIndex, GameInfo.stProfile p_Profile)
	{
		m_Profile = p_Profile;

		//	ラベルの設定
		nameLabel.text = m_Profile.name;

        if (!string.IsNullOrEmpty(p_Profile.fileName))
        {
            SetIcon(p_Profile.fileName);
        }
        else
        {
            SetIcon("player_0");
        }
	}

    public void SetIcon(string p_TexName)
    {
        //	テクスチャをロード
        UITexture   a_Tex = PlayerTable.IconCreate(p_TexName);
        icon.mainTexture = a_Tex.mainTexture;

        Destroy(a_Tex.gameObject);
    }

	/// <summary>
	/// アイコンがタッチされた時の処理
	/// </summary>
	public void	DetailButtonDidPush()
	{
		StartCoroutine(PlayerDetailDraw_Create());
	}

	public IEnumerator PlayerDetailDraw_Create()
	{
		//	プレイヤーの詳細を表示する
		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate("Select/PlayerDetailDraw", Vector2.zero);

        //  バナーを非表示にする
        def.AdManager.Banner_SetActive(false);

        //	オブジェクトが生成されるまで待機
        while (a_Obj == null) yield return 0;

		GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, a_Obj);

		//	プレイヤーデータ取得
		PlayerData.Param	a_Param		= def.instance.playerTable.paramList[m_Profile.id];

		PlayerDetailDraw	a_Detail	= a_Obj.GetComponent<PlayerDetailDraw>();

		//	初期化
		a_Detail.Init(this, a_Param);
	}

	/// <summary>
	/// パートナー設定ボタンが押された時に呼ばれる処理
	/// </summary>
	public void	PartnerSetButtonDidPush()
	{
		changePartnerSetCallBack(this);
	}

	/// <summary>
	/// 「変更」ボタンが押された時の処理
	/// </summary>
	public void	ChangeButtonDidPush()
	{
		//	パートナーの変更を行うデリゲートを呼び出す
		changePartnerOpenCallBack();
	}

	/// <summary>
	/// パートナー情報が更新された時に呼ぶ
	/// </summary>
	public void	Partner_Set(bool p_bPartner)
	{
        partnerOn.gameObject.SetActive(p_bPartner);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
