﻿using UnityEngine;
using System.Collections;

public class PlayerDetailDraw : MonoBehaviour {

	public GameObject		front;
    public GameObject       back;
    public UITexture        background_front;
    public UITexture		profileTex_front;
	public UIButtonTween	exitButton;
	public UILabel			nameLabel_front;
	public UILabel			worldRankLabel;
	public UILabel			genderLabel;
	public UILabel			handLabel_front;
    public UILabel          aggressiveLabel;
    public GameObject       aggressiveGaugeRoot;
    public UILabel          techniqueLabel;
    public GameObject       techniqueGaugeRoot;
    public UILabel          staminaLabel;
    public GameObject       staminaGaugeRoot;
    public UILabel          rhythmLabel;
    public GameObject       rhythmGaugeRoot;
    public UILabel			powerLabel;
    public GameObject       powerGaugeRoot;
	public UISprite		    flagSprite_front;

    public UITexture        background_back;
    public UITexture        profileTex_back;
    public UISprite         flagSprite_back;
    public UILabel          nameLabel_back;
    public UILabel          countryLabel_back;
    public UILabel          handLabel_back;
    public UILabel          shotLabel_back;
    public UILabel          commentLabel_back;
    public UILabel          awardLabel_back;

    PlayerData.Param		m_Param;

    int     m_nPage;
    int     m_nPageMax;

	bool	m_bAnim;

	/// <summary>
	/// 国旗画像の幅（固定）
	/// </summary>
	public const float		c_fNationalFlag_Width = 120;

    public SelectProfileDraw profileDraw
    {
        get { return m_ProfileDraw; }
        set { m_ProfileDraw = value; }
    }
    SelectProfileDraw m_ProfileDraw;

	/// <summary>
	/// サイズ
	/// </summary>
	Vector2 m_Size
	{
		get
		{
			Vector2 a_Size = GameUtils.WidgetSize_Get(front);
			return a_Size;
		}
	}

	/// <summary>
	/// Ｙ座標の初期位置
	/// </summary>
	float m_DefaultPos_Y
	{
		get
		{
			return -(def.c_ScreenSize.y / 2) + -(m_Size.y / 2);
		}
	}

	/// <summary>
	/// ボタンを無効にする
	/// </summary>
	/// <param name="p_bDisable"></param>
	public void TouchDisableSet(bool p_bDisable)
	{
		BoxCollider[] a_Col = front.GetComponentsInChildren<BoxCollider>();

		foreach (BoxCollider col in a_Col)
		{
			col.enabled = !p_bDisable;
		}
	}

	/// <summary>
	/// ボタンが押された時に呼ばれる処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void ButtonDidPush(UIButtonTween p_Sender)
	{
		//	アニメーション中はタッチできない
		if (m_bAnim) return;

		GeneralData a_Data = p_Sender.GetComponent<GeneralData>();
		string a_Key = a_Data.stringValue;

		GameInfo.eLanguage a_Language = GameUtils.EnumGet<GameInfo.eLanguage>(a_Key);

		//	言語を設定
		GameInfo.instance.Language_Set(a_Language);
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="p_Param"></param>
	/// <param name="p_bStartAnim"></param>
	public void	Init(SelectProfileDraw p_ProfileDraw, PlayerData.Param p_Param, bool p_bStartAnim = true)
	{
		m_Param = p_Param;

        m_nPageMax = 2;
        m_nPage = 0;

        m_ProfileDraw = p_ProfileDraw;

        //	各ラベルの設定
        //  名前
        GameInfo.eLanguage  a_Lan = (GameInfo.instance.language == GameInfo.eLanguage.JPN || GameInfo.instance.language == GameInfo.eLanguage.ENG) ? GameInfo.eLanguage.ENG : GameInfo.eLanguage.PRT;
		nameLabel_front.text	= def.instance.playerTable.Name_Get(p_Param.ID, a_Lan);
        nameLabel_back.text     = def.instance.playerTable.Name_Get(p_Param.ID);

        //  世界ランキング
        if(def.instance.playerTable.isCOM(p_Param))
        {
            worldRankLabel.text = "-";
        }
        else
        {
            worldRankLabel.text = m_Param.WorldRank.ToString();
        }

        //  性別
		genderLabel.text	    = m_Param.Gender;

        //  利き腕
        handLabel_front.text          = def.instance.playerTable.Hand_Get(p_Param.ID);

        //  パワー
		powerLabel.text			= m_Param.Power.ToString();
        ParamGauge_Set((int)m_Param.Power, powerGaugeRoot);

        //  攻撃性
        aggressiveLabel.text	= m_Param.Aggressive.ToString();
        ParamGauge_Set((int)m_Param.Aggressive, aggressiveGaugeRoot);

        //  リズム
        rhythmLabel.text		= m_Param.Rhythm.ToString();
        ParamGauge_Set((int)m_Param.Rhythm, rhythmGaugeRoot);

        //  テクニック
        techniqueLabel.text		= m_Param.Technique.ToString();
        ParamGauge_Set((int)m_Param.Technique, techniqueGaugeRoot);

        //  スタミナ
        staminaLabel.text		= m_Param.Stamina.ToString();
        ParamGauge_Set((int)m_Param.Stamina, staminaGaugeRoot);

        //  出身国
        countryLabel_back.text  = m_Param.Nation;

        //  得意ショット
        shotLabel_back.text     = def.instance.playerTable.BestShot_Get(p_Param.ID);

        //  コメント
        commentLabel_back.text	= def.instance.playerTable.Profile_Get(p_Param.ID);

        //  受賞
		awardLabel_back.text	= def.instance.playerTable.Award_Get(p_Param.ID);

		//	出身国の略称を取得
		string	a_Abbreviation	= def.c_CountryAbbreviation[p_Param.Nation];
		string	a_SpName		= string.Format("img_flag_{0}", a_Abbreviation);

        //	国旗画像の読み込み
        flagSprite_front.spriteName = a_SpName;
        flagSprite_back.spriteName  = a_SpName;

		if (!string.IsNullOrEmpty(m_Param.FileName))
		{
			//	テクスチャをロード
			UITexture	a_Tex = PlayerTable.TexCreate(m_Param.FileName);
			profileTex_front.mainTexture = a_Tex.mainTexture;

            Destroy(a_Tex.gameObject);

            a_Tex = PlayerTable.TexCreate(m_Param.FileName + "b");
            profileTex_back.mainTexture = a_Tex.mainTexture;

            Destroy(a_Tex.gameObject);
        }

        //  枠画像のロード
        if(background_front.mainTexture == null)
        {
            background_front.mainTexture = ResourceManager.TextureLoad("PlayerDetail/" + "img_card_front", false);
        }

        if (background_back.mainTexture == null)
        {
            background_back.mainTexture = ResourceManager.TextureLoad("PlayerDetail/" + "img_card_back", false);
        }

        if (p_bStartAnim)	StartCoroutine(StartAnim());
    }

    public void ParamGauge_Set(int p_nParam, GameObject p_Root)
    {
        for (int i = 0 ; i < p_nParam ; i++)
        {
            GameObject  a_Gauge = ResourceManager.PrefabLoadAndInstantiate("Select/GaugeSprite", Vector3.zero);
            a_Gauge.transform.parent = p_Root.transform;
            a_Gauge.transform.localScale = Vector3.one;
        }
    }

	/// <summary>
	/// スタート時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator StartAnim()
	{
		m_bAnim = true;
		
		//	タッチ無効
		TouchDisableSet(true);

		//	非表示
		gameObject.SetActive(true);

		//	Ｙ座標指定
		CSTransform.SetPos_Y(front.transform, m_DefaultPos_Y);

		yield return 0;

		float a_fTime = 0.6f;
		iTween.MoveTo(front, iTween.Hash("y", 0, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeOutBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

        //  プレイヤーリストを非表示にする
        m_ProfileDraw.dialog.listObj.SetActive(false);

        m_bAnim = false;

		//	タッチ許可
		TouchDisableSet(false);

		yield return 0;
	}

    public void PageButtonDidPush()
    {
        Page_Add();
    }

	public void BackButtonDidPush()
	{
		StartCoroutine(EndAnim());
	}

	public void	TexButtonDidPush()
	{
//        Page_Add();
    }

    public void Page_Add()
    {
        m_nPage++;
        m_nPage %= m_nPageMax;

        if(m_nPage == 0)
        {
            front.SetActive(true);
            back.SetActive(false);
        }
        else
        if (m_nPage == 1)
        {
            front.SetActive(false);
            back.SetActive(true);
        }
    }

	/// <summary>
	/// 終了時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator EndAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisableSet(true);

        //  プレイヤーリストを表示する
        m_ProfileDraw.dialog.listObj.SetActive(true);

        float a_fTime = 0.6f;
		iTween.MoveTo(gameObject, iTween.Hash("y", m_DefaultPos_Y, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeInBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

        //  バナーを表示
        def.AdManager.Banner_SetActive(true);

        //	タッチ許可
        TouchDisableSet(false);

		//	非表示
		gameObject.SetActive(false);

		m_bAnim = false;

		//	自身を削除
		Destroy(gameObject);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
