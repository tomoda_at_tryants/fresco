﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectProfileList : MonoBehaviour {

	/// <summary>
	/// 表示タイプ
	/// </summary>
	public enum eViewType
	{
		Game,		//	トーナメント用
		Partner,	//	パートナー選択用
	}

	eViewType		m_ViewType;
	/// <summary>
	/// 表示タイプ
	/// </summary>
	public eViewType	viewType
	{
		get { return m_ViewType; }
	}

	public GameObject	dialogRoot;
	public UIPanel		panel;
	public UIGrid		container;
	public UITexture	background;
    public UISprite     seleceSceneSprite;

	List<SelectProfileDialog>	m_DialogList;

	public delegate void	CloseCallBack();
	/// <summary>
	/// 閉じる時に呼ばれるコールバック
	/// </summary>
	CloseCallBack			closeCallBack;

	/// <summary>
	/// 閉じる時に呼ばれるコールバックを設定する
	/// </summary>
	public void	CloseCallBack_Set(CloseCallBack p_CallBack)
	{
		closeCallBack += p_CallBack;
	}

	/// <summary>
	/// 表示数
	/// </summary>
	public static int	viewNum
	{
		get
		{
			PlayerTable	a_Table = def.instance.playerTable;
			if (a_Table == null) return 0;

			int a_nViewNum = a_Table.paramList.Count;

			//	トーナメントモードの時はユーザー分を足しておく
			if(GameInfo.instance.gameMode == GameInfo.eGameMode.TOURNAMENT)
			{
				a_nViewNum += 1;
			}

			return a_nViewNum;
		}
	}

	/// <summary>
	/// １画面に完全に表示される数
	/// </summary>
	public int	viewNumScreen
	{
		get
		{
			//	スクリーンの縦サイズ
			float	a_ScrHeight		= panel.GetViewSize().y;
			float	a_DialogSize	= container.cellHeight;

			int a_nNum = (int)a_ScrHeight / (int)a_DialogSize;
			return	a_nNum;
		}
	}

	/// <summary>
	/// ダイアログのサイズ（正しくは、ダイアログを１つ表示するサイズ）
	/// </summary>
	public Vector2	dialogSize
	{
		get
		{
			if (container == null)	return Vector2.zero;

			return	new Vector2(container.cellWidth, container.cellHeight);
		}
	}

	/// <summary>
	/// ユーザーのダイアログを取得する
	/// </summary>
	public SelectProfileDialog	userDialog
	{
		get
		{
			if(m_DialogList == null)
			{
				DebugManager.Log("Error because null to 「m_DialogList」.");
				return	null;
			}

			foreach(SelectProfileDialog a_Dialog in m_DialogList)
			{
				if(a_Dialog.viewType == SelectProfileDialog.eViewType.USER)
				{
					return a_Dialog;
				}
			}

			DebugManager.Log("Error because not found to useDialog");
			return	null;
		}
	}

	/// <summary>
	/// プレイヤーのダイアログを取得する
	/// </summary>
	public SelectProfileDialog PlayerDialog_Get(int p_nID)
	{
		if (m_DialogList == null)
		{
			DebugManager.Log("Error because null to 「m_DialogList」.");
			return null;
		}

		foreach (SelectProfileDialog a_Dialog in m_DialogList)
		{
			if (a_Dialog.id == p_nID && a_Dialog.viewType == SelectProfileDialog.eViewType.PLAYER)
			{
				return a_Dialog;
			}
		}

		DebugManager.Log("Error because not found to useDialog");
		return null;
	}

	/// <summary>
	/// 表示タイプによってオブジェクトのアクティブ等を設定する
	/// </summary>
	public void	EveryViewTypeSet()
	{
		switch(m_ViewType)
		{
			case eViewType.Game:
				{
					dialogRoot.gameObject.SetActive(false);
				}
				break;

			case eViewType.Partner:
				{
					dialogRoot.gameObject.SetActive(true);

					//	設定されている言語
					string	a_LanStr = GameInfo.instance.language.ToString();

					//	座標調整
					CSTransform.SetPos_Y(dialogRoot.transform, -transform.localPosition.y);
				}
				break;
		}
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init(eViewType p_ViewType, bool p_bOpenAnim, SelectProfileDraw.ChangePartnerOpenCallBack p_ChangePartnerOpenCallBack, SelectProfileDraw.ChangePartnerSetCallBack p_ChangePartnerSetCallBack)
	{
		//	表示タイプ
		m_ViewType = p_ViewType;
			
		//	表示タイプ毎の設定
		EveryViewTypeSet();

        background.mainTexture = ResourceManager.TextureLoad("Bg/" + "bg_partneredit", false);

        //	トーナメント時はユーザーのダイアログが表示される位置に調整する
        //  パートナー選択時は倒したプレイヤーが表示される位置に調整する
        {
            float   a_Max_Add   = 0;
            float   a_Max_Add2  = 0;
            if(m_ViewType == eViewType.Game)
            {
                //  位置調整
                CSTransform.SetPos_Y(panel.transform, -70);
                panel.SetRect(0, 0, 640, 710);
                panel.clipOffset = new Vector2(0, 70);
            }
            else
            {
                seleceSceneSprite.spriteName = "img_sgn_partneredit_j";

                //  位置調整
                CSTransform.SetPos_Y(panel.transform, -65);
                panel.SetRect(0, 0, 640, 780);
                panel.clipOffset = new Vector2(0, 35);

                a_Max_Add = 70;
                a_Max_Add2 = 30;
            }

            int     a_nSub      = (m_ViewType == eViewType.Game) ? 2 : 1;
            int     a_nAdd      = PlayerInfo.instance.userRank - a_nSub;
            Vector2 a_PanelSize = panel.GetViewSize();

            float   a_fSetY_Pos     = (container.cellHeight * a_nAdd) + panel.transform.localPosition.y;
            float   a_fSetY_Offset  = (-container.cellHeight * a_nAdd) + panel.clipOffset.y;

            float   a_fMax_Y        = panel.GetViewSize().y - container.cellHeight - a_Max_Add;

            if (a_fSetY_Pos > a_fMax_Y)     a_fSetY_Pos     = a_fMax_Y;
            if (a_fSetY_Offset < -a_fMax_Y) a_fSetY_Offset  = -a_fMax_Y;

            panel.transform.localPosition = new Vector2(panel.transform.localPosition.x, a_fSetY_Pos - a_Max_Add2);
            panel.clipOffset = new Vector2(panel.clipOffset.x, a_fSetY_Offset);
        }

        m_DialogList = new List<SelectProfileDialog>();

		PlayerTable	a_Table	= def.instance.playerTable;
		//	選手数（ユーザー分は含まない）
		int a_nPlayerNum = a_Table.paramList.Count / 2;

		int		a_nID			= 0;
		bool	a_bEnableUser	= false;
		//	ダイアログを生成
		for (int i = 0; i < a_nPlayerNum ; i++)
		{
			//	パラメータ取得
			PlayerData.Param	a_Param	= a_Table.paramList[i * 2];

			//	コンピュータはパートナー選択時のみ表示される
			//	選手は必ず生成
			if(!a_Table.isCOM(a_Param) ||
			   (a_Table.isCOM(a_Param) && (m_ViewType == eViewType.Partner)))
			{
				//	選手のダイアログ生成
				PlayerDialog_Create(a_nID, a_Param, a_bEnableUser, p_ChangePartnerSetCallBack);
			}

			a_nID++;

			//	ユーザーのダイアログを生成するタイミング
			if (PlayerInfo.instance.userRank == a_Param.WorldRank + 1 && m_ViewType == eViewType.Game)
			{
				//	ユーザーのダイアログ生成
				UserDialog_Create(a_nID, p_ChangePartnerOpenCallBack);

				a_bEnableUser = true;
				a_nID++;
			}
		}

		//	プレイヤーのダイアログが生成されなかった時は生成
		if (!a_bEnableUser && m_ViewType == eViewType.Game)
		{
			UserDialog_Create(a_nID, p_ChangePartnerOpenCallBack);
		}

		//	パートナー関連の更新
		if(m_ViewType == eViewType.Partner)	PartnerUpdate();

		if(p_bOpenAnim)
		{
			StartCoroutine(StartAnim());
		}
	}

	/// <summary>
	/// 選手のダイアログを生成する
	/// </summary>
	/// <param name="a_nID"></param>
	/// <param name="a_Param"></param>
	/// <param name="p_bEnableUser"></param>
	public void PlayerDialog_Create(int a_nID, PlayerTable.Param a_Param, bool p_bEnableUser, SelectProfileDraw.ChangePartnerSetCallBack p_ChangePartnerSetCallBack)
	{
		List<GameInfo.stProfile>	a_ProfileList	= new List<GameInfo.stProfile>();
		GameInfo.stProfile			a_Data			= new GameInfo.stProfile();
		PlayerTable				a_Table			= def.instance.playerTable;

		//	選手データ生成
		int		a_nAdd = (p_bEnableUser) ? 1 : 0;	//	既にユーザーのダイアログが表示されているか
		string	a_Name = def.instance.playerTable.Name_Get(a_Param.ID);
		a_Data = new GameInfo.stProfile(a_Param.ID, a_Param.WorldRank + a_nAdd, a_Param.WorldRank.ToString(), a_Name, a_Param.FileName);
		a_ProfileList.Add(a_Data);

		//	ペア選手のデータを生成する（同ランクの選手）
		foreach (PlayerData.Param p_Param in a_Table.paramList)
		{
			if (isPare(a_Param, p_Param))
			{
				a_Name = def.instance.playerTable.Name_Get(p_Param.ID);
				a_Data = new GameInfo.stProfile(p_Param.ID, p_Param.WorldRank + a_nAdd, p_Param.WorldRank.ToString(), a_Name, p_Param.FileName);
				a_ProfileList.Add(a_Data);

				break;
			}
		}

		//	ダイアログ生成
		SelectProfileDialog a_Dialog = SelectProfileDialog.Create(gameObject, a_nID, SelectProfileDialog.eViewType.PLAYER, m_ViewType, a_ProfileList, container.transform, null, p_ChangePartnerSetCallBack);
		//	リストに追加
		m_DialogList.Add(a_Dialog);
	}

	/// <summary>
	/// ペアか
	/// </summary>
	/// <param name="p_Param0"></param>
	/// <param name="p_Param1"></param>
	/// <returns></returns>
	public bool	isPare(PlayerTable.Param p_Param0, PlayerTable.Param p_Param1)
	{
		if (p_Param0.ID != p_Param1.ID && (p_Param0.WorldRank) == (p_Param1.WorldRank))
		{
			return true;
		}

		return false;
	}

	/// <summary>
	/// ユーザーのダイアログを生成する
	/// </summary>
	public void UserDialog_Create(int p_nID, SelectProfileDraw.ChangePartnerOpenCallBack p_ChangePartnerOepnCallBack)
	{
		List<GameInfo.stProfile>	a_ProfileList	= new List<GameInfo.stProfile>();
		GameInfo.stProfile			a_Data			= new GameInfo.stProfile();
		PlayerTable				a_Table			= def.instance.playerTable;
		//	選手数（ユーザー分は含まない）
		int							a_nPlayerNum	= a_Table.paramList.Count / 2;

		//	プロフィールリストをクリア
		a_ProfileList.Clear();

		//	プレイヤー用のダイアログを作成する
		string	a_Key	= string.Format("User_DefaultName_{0}", GameInfo.instance.language.ToString());
		string	a_Name	= Localization.Get(a_Key);
		a_Data = new GameInfo.stProfile(a_nPlayerNum, PlayerInfo.instance.userRank, "F", a_Name, "");
		a_ProfileList.Add(a_Data);

		//	こっちは選手を選択できるようにする
		a_Data = new GameInfo.stProfile(a_nPlayerNum + 1, PlayerInfo.instance.userRank, "", "", "");
		a_ProfileList.Add(a_Data);

		//	生成
		SelectProfileDialog	a_Dialog = SelectProfileDialog.Create(gameObject, p_nID, SelectProfileDialog.eViewType.USER, eViewType.Game, a_ProfileList, container.transform, p_ChangePartnerOepnCallBack, null);

		m_DialogList.Add(a_Dialog);
	}

	/// <summary>
	/// パートナー関連の更新
	/// </summary>
	public void PartnerUpdate()
	{
		//	ユーザーのパートナー情報
		GameInfo.stProfile	a_PartnerProfile = GameInfo.instance.partnerProfile;
		//	パートナーが設定されていない場合は終了
		if (!a_PartnerProfile.isEnableData)	return;

		foreach (SelectProfileDialog a_Dialog in m_DialogList)
		{
			foreach(SelectProfileDraw a_Draw in a_Dialog.profileDrawList)
			{
				bool	a_bPartner = (a_PartnerProfile.isEnableData && a_PartnerProfile.Compare(a_Draw.info)) ? true : false;
				//	パートナーに設定されている選手の枠の色を変える
				a_Draw.Partner_Set(a_bPartner);
			}
		}
	}

	bool	m_bAnim;

	/// <summary>
	/// サイズ
	/// </summary>
	public Vector2	size
	{
		get
		{
			Vector2	a_Size = GameUtils.WidgetSize_Get(background.gameObject);
			return	a_Size;
		}
	}

	/// <summary>
	/// スタート時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator StartAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisable.instance.SetDisable(true);

		//	表示
		gameObject.SetActive(true);

		//	Ｙ座標指定
		CSTransform.SetPos_Y(transform, -size.y);

		yield return 0;

		float	a_fTime = 0.6f;
		iTween.MoveTo(gameObject, iTween.Hash("y", 20, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeOutBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		m_bAnim = false;

		//	タッチ有効
		TouchDisable.instance.SetDisable(false);

		yield return 0;
	}

	/// <summary>
	/// 終了時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator EndAnim()
	{
		m_bAnim = true;

		//	タッチ無効
		TouchDisable.instance.SetDisable(true);

		float a_fTime = 0.6f;
		iTween.MoveTo(gameObject, iTween.Hash("y", -1136, "islocal", true, "time", a_fTime, "easetype", iTween.EaseType.easeInBack));

		//	アニメーションが終了するまで待機
		yield return new WaitForSeconds(a_fTime);

		//	タッチ有効
		TouchDisable.instance.SetDisable(false);

		//	閉じる時に呼ばれるコールバック
		if (closeCallBack != null)
		{
			closeCallBack();
		}

		//	削除
		Destroy(gameObject);
	}

	/// <summary>
	/// パートナーをセットするボタンを押した時の処理
	/// </summary>
	public void CloseButtonDidPush()
	{
		StartCoroutine(EndAnim());
	}

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
	}

}
