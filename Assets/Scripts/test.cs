﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	public AudioSource	source;

	// Use this for initialization
	void Start () {
		source.rolloffMode	= AudioRolloffMode.Linear;
		source.minDistance	= 0;
		source.maxDistance	= 15;
		source.spatialBlend = 1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
