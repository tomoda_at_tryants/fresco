﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BallManager : MonoBehaviour {

	static string eTAG = "BallManager";

	public enum eState : int
	{
		None,

		State_Init,
		State_Serve,
		State_Rally,
		State_Miss,

		Max
	}

	/// <summary>
	/// ボールのレシーバー
	/// </summary>
	public enum eReceiver : int
	{
		NONE,

		PLAYER,		//	プレイヤー
		CPU			//	ＣＰＵ
	}

	/// <summary>
	/// 使用用途
	/// </summary>
	public enum eUseType : int
	{
		NONE,

		Game,	//	ゲーム
		Check	//	チェック
	}

	/// <summary>
	/// ボールオブジェクト（スケール等はこっちを使用する）
	/// </summary>
	public GameObject	ball;
	public GameObject	guide;
	public	UITexture	ballTex;
	public	UITexture	guideTex;

	eReceiver	m_Receiver;
	/// <summary>
	/// ボールを打ち返す側
	/// </summary>
	public	eReceiver	receiver
	{
		get { return m_Receiver; }
		set { m_Receiver = value; }
	}

	/// <summary>
	/// レシーバーがプレイヤーか
	/// </summary>
	public bool	isReceivePlayer
	{
		get { return (m_Receiver == eReceiver.PLAYER) ? true : false; }
	}

	/// <summary>
	/// レシーバーがＣＰＵか
	/// </summary>
	public bool isReceiveCPU
	{
		get { return (m_Receiver == eReceiver.CPU) ? true : false; }
	}

	bool m_bServed;
	/// <summary>
	///	ラケットに当たったか
	/// </summary>
	public bool isServed
	{
		get { return m_bServed; }
		set { m_bServed = value; }
	}

	bool m_bReady;
	/// <summary>
	/// 準備が出来たか
	/// </summary>
	public bool	isReady
	{
		get { return m_bReady; }
	}

	int		m_nSpeed;
	/// <summary>
	/// スピード
	/// </summary>
	public int	speed
	{
		get { return m_nSpeed; }
	}

	int m_nRallyCount;
	/// <summary>
	/// ラリー回数
	/// </summary>
	public int rallyCount
	{
		get { return m_nRallyCount; }
	}

	int m_nAtkChangeCount;
	/// <summary>
	/// 攻守交替回数
	/// </summary>
	public int	atkChangeCount
	{
		get { return m_nAtkChangeCount; }
	}

	/// <summary>
	/// ボールが到達するまでの時間
	/// </summary>
	float	m_fArrivalTime;
	public float	arrivalTime
	{
		get { return m_fArrivalTime; }
	}

	float m_fGraceTime;
	/// <summary>
	/// 到達してからmissになるまでの猶予時間
	/// </summary>
	public float	graceTime
	{
		get { return m_fGraceTime; }
	}

	/// <summary>
	/// ミスしたか
	/// </summary>
	public bool	isMiss
	{
		get 
		{
			//	・ラリー中	・ゲームが終了していない　・到達時間と猶予時間が共に０
			return (m_UseType == eUseType.Game		&&
					m_State == eState.State_Rally	&&
					m_Receiver == eReceiver.PLAYER	&&
					!Header.instance.isTimeUp		&&
					m_fArrivalTime <= 0f			&&
					m_fGraceTime <= 0f) ? true : false;
		}
	}

	/// <summary>
	/// ボールが打てるタイミングか
	/// </summary>
	public bool isShotTiming
	{
		get
		{
			bool a_bRes = false;
			if(m_Receiver == eReceiver.CPU)
			{
				a_bRes = (m_fArrivalTime <= 0) ? true : false;
			}
			if (m_Receiver == eReceiver.PLAYER)
			{
				if (!m_bServed)	return true;	//	サーブ時は常にtrue
				a_bRes = (ball.transform.localScale.x <= 2f) ? true : false;
			}

			return a_bRes;
		}
	}

	/// <summary>
	/// 攻守交替か
	/// </summary>
	public bool isChangeAttack
	{
		get
		{
			return ((m_UseType == eUseType.Game) && (m_nAtkChangeCount >= m_nChangeChallengeCount)) ? true : false;
		}
	}

	int	m_nChangeChallengeCount;
	/// <summary>
	/// 攻守交替を行うまでの回数
	/// </summary>
	public int	changeChallengeCount
	{
		get { return m_nChangeChallengeCount; }
		set { m_nChangeChallengeCount = value; }
	}

	Shot_AdjustTable.eAdjust m_Adjust;
	/// <summary>
	/// 判定
	/// </summary>
	public Shot_AdjustTable.eAdjust	adjust
	{
		get { return m_Adjust; }
	}

	bool	m_bServeAnim;
	/// <summary>
	/// サーブ時のアニメーションが実行されているか
	/// </summary>
	public bool isServeAnim
	{
		get { return m_bServeAnim; }
	}

	bool		m_bChangeAttack_Suc;
	/// <summary>
	/// 攻守交替が成功したか
	/// </summary>
	public bool	isChangeAttack_Suc
	{
		get { return m_bChangeAttack_Suc; }
	}

	int m_nChangeAttack_MissCount;
	/// <summary>
	/// 攻守交替に失敗した回数
	/// </summary>
	public int	changeAttack_MissCount
	{
		get { return m_nChangeAttack_MissCount; }
	}

    int m_nRallySENum;
    /// <summary>
    /// ラリー時に鳴らすSE番号
    /// </summary>
    public int rallySENum
    {
        get { return m_nRallySENum; }
    }

    int m_nRallySEInterval;
    /// <summary>
    /// SEを鳴らすラリー回数
    /// </summary>
    public int rallySEInterval
    {
        get { return m_nRallySEInterval; }
    }

    int m_nRallySEMax;
    /// <summary>
    /// ラリー時に鳴らすSE数
    /// </summary>
    public int rallySEMax
    {
        get { return m_nRallySEMax; }
    }

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	//	ステート関連
	eState	m_State;
	eState	m_NextState;
	string	m_Coroutine;

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
		if (p_bContinus)
		{
			State_Check();
		}
	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
			{
				StopCoroutine(m_Coroutine);
				m_Coroutine = null;
			}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

			DebugManager.Log(string.Format("{0} Call {1}()", eTAG, m_Coroutine));

			return true;
		}

		return false;
	}

	static			BallManager	c_Instance;
	public static	BallManager	instance
	{
		get { return c_Instance; }
	}

	eUseType	m_UseType;
	/// <summary>
	/// 使用タイプ
	/// </summary>
	public eUseType	useType
	{
		get { return m_UseType; }
		set { m_UseType = value; }
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static BallManager	Create(eUseType p_UseType)
	{
		if (c_Instance != null)	return c_Instance;

		c_Instance			= ResourceManager.PrefabLoadAndInstantiate<BallManager>("Game/BallManager", Vector3.zero);
		c_Instance.useType	= p_UseType;

		c_Instance.Init();

		return	c_Instance;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init()
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}

		State_Next(eState.State_Init, true);

		ball.transform.localScale = def.c_DefaultBallScale;

		//	親設定
		GameUtils.AttachChild(PanelManager.instance.alignmanMid.alignCenter.gameObject, gameObject);

		//	攻守交替に必要なスイング回数を設定

		//	パートナーのパラメータ取得
		int					a_nPartnerID	= GameInfo.instance.partnerProfile.id;
		PlayerTable.Param	a_PartnerParam	= def.instance.playerTable.Param_Get(a_nPartnerID);

		//	攻守交替に必要なスイング回数設定
		m_nChangeChallengeCount = def.instance.atkChangeTable.ChallengeTimes_Get(a_PartnerParam.Aggressive);

        m_nRallySEMax       = 3;
        m_nRallySEInterval  = 5;
	}

	/// <summary>
	/// 初期化を行う
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Init()
	{
		if(guideTex.mainTexture == null)
		{
			//	入力判定ガイド
			guideTex.mainTexture = ResourceManager.TextureLoad("Game/" + "ball_guide", false);
		}

		//	サイズ設定
		GameUtils.WidgetSize_Set(guideTex.gameObject, new Vector2(550, 550));
		//	非表示
		guideTex.gameObject.SetActive(false);

		if(ballTex.mainTexture == null)
		{
			//	ボール
			ballTex.mainTexture = ResourceManager.TextureLoad("Game/" + "ball", false);
		}

		//	ウィジェットサイズ設定
		GameUtils.WidgetSize_Set(ballTex.gameObject, new Vector2(ballTex.mainTexture.width, ballTex.mainTexture.height));
		//	非表示
		ballTex.gameObject.SetActive(false);

		//	サイズ設定
		ball.transform.localScale = def.c_DefaultBallScale;

		//	座標初期化
		ball.transform.localPosition = Vector3.zero;

		//	カラー
		ballTex.color = Color.white;

		//	変数の初期化

		//	サーブはプレイヤーに設定
		m_Receiver		= eReceiver.PLAYER;
		m_Adjust		= Shot_AdjustTable.eAdjust.NONE;

		m_nSpeed			= 0;
		m_nRallyCount		= 0;
		m_nAtkChangeCount	= 0;
		m_fGraceTime		= 0f;
		m_fArrivalTime		= 0f;

		m_bServed			= false;
		m_bReady			= false;
		m_bServeAnim		= false;
		m_bChangeAttack_Suc = false;

		State_Next(eState.State_Serve, true);

		yield return 0;
	}

	/// <summary>
	/// サーブ時の処理
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Serve()
	{
		//	タッチを有効にする
		TouchDisable.instance.SetDisable(false);

		//	ボールを表示する
		ballTex.gameObject.SetActive(true);

		//	ゲーム開始
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_whistle_start);
		//	ホイッスルが鳴り終わるまで待機
		yield return new WaitForSeconds(0.5f);

		//	準備が出来た
		m_bReady = true;

		//	ゲーム時のみ
		if(m_UseType == eUseType.Game)
		{
			//	速度表示を反映
			Header.instance.Speed_Update(m_nSpeed);
		}

		//	サーブ前のボールを動かす処理
		yield return StartCoroutine(BallMoving_Exec());

		m_bReady = false;

		State_Next(eState.State_Rally);

		yield return 0;
	}

	public IEnumerator	BallMoving_Exec()
	{
		Vector3 a_LowPassPos = Vector3.zero;	//	ローパス用
		//	サーブが打たれるまで待機
		while (!m_bServed)
		{
			if (m_bServeAnim || Time.timeScale == 0)
			{
				yield return 0;
				continue;
			}

			//	１フレーム前の座標と現在のフレームの座標にローパスフィルターをかける
			Vector3 a_RevAcc = GameUtils.Acceleration_LowPass(a_LowPassPos);
			Vector2 a_BallSize = new Vector2(ballTex.localSize.x * ball.transform.localScale.x, ballTex.localSize.y * ball.transform.localScale.y);

			a_RevAcc.x = Mathf.Clamp(a_RevAcc.x, (-(def.c_ScreenSize.x / 2) + (a_BallSize.x / 2)), ((def.c_ScreenSize.x / 2) - (a_BallSize.x / 2)));
			a_RevAcc.y = Mathf.Clamp(a_RevAcc.y, (-(def.c_ScreenSize.y / 2) + (a_BallSize.y / 2)), ((def.c_ScreenSize.y / 2) - (a_BallSize.y / 2)));

			//	ボールに反映
			ball.transform.localPosition = a_RevAcc;

			//	ローパス用に保存
			a_LowPassPos = a_RevAcc;

			yield return 0;
		}
	}

	/// <summary>
	/// ラリーが続いてる時の処理
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Rally()
	{
		while(true)
		{
			//	ミスしたらステートを移動
			if (isMiss)	State_Next(eState.State_Miss);

			yield return 0;
		}
	}

	/// <summary>
	/// ミス時の処理
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Miss()
	{
		//	ミス時の効果音
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_miss);

		//	振動
		GameUtils.Vibrate();

		//	タッチを無効にする
		TouchDisable.instance.SetDisable(true);

		//	平均点（ミス時も加算される）
		EvaluationManager.instance.TechniqueCheck(Shot_AdjustTable.eAdjust.NONE);

		//	レシーバー初期化
		m_Receiver		= BallManager.eReceiver.NONE;
		m_bServed		= false;
		m_bReady		= false;
		m_bServeAnim	= false;
		m_nRallyCount	= 0;

		//	ガイドは非表示
		guideTex.gameObject.SetActive(false);

		//	ミス時のアニメーション
		yield return StartCoroutine(Miss_Animation());

		//	アニメーションが終了したら非表示にしておく
		ballTex.gameObject.SetActive(false);

		yield return new WaitForSeconds(0.5f);

		//	ラリー回数初期化
		Header.instance.RallyTimes_Update(0);

		//	ボールのサイズを戻す
//		ball.transform.localScale = def.c_DefaultBallScale;

		State_Next(eState.State_Init, true);

		yield return 0;
	}

	/// <summary>
	/// サーブの所から始める
	/// </summary>
	public void	Restart()
	{
		State_Next(eState.State_Init, true);
	}

	/// <summary>
	/// ミスした時のアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Miss_Animation()
	{
		//	ボールが落ちるアニメーション
		float	a_fTime = 0.7f;
		iTween.MoveTo(ball, iTween.Hash("y", -800, "time", a_fTime, "islocal", true, "easetype", iTween.EaseType.linear));

		yield return new WaitForSeconds(a_fTime);
	}

	/// <summary>
	/// ボールが当たった時の処理
	/// </summary>
	public void Hit_Exec(SwingManager p_SwingMan, SwingManager.stResShotData p_Res, System.Action p_ChangeAttack, System.Action p_CallBack)
	{
		StartCoroutine(Hit_Exec0(p_SwingMan, p_Res, p_ChangeAttack, p_CallBack));
	}

	/// <summary>
	/// ボールが当たった時の処理
	/// </summary>
	/// <param name="p_SwingMan"></param>
	/// <param name="p_Res"></param>
	/// <returns></returns>
	public IEnumerator	Hit_Exec0(SwingManager p_SwingMan, SwingManager.stResShotData p_Res, System.Action p_ChangeAttack, System.Action p_CallBack)
	{
		if(m_UseType == eUseType.Game)
		{
			//	打球数を増やす
			EvaluationManager.instance.HitCount_Add(m_Receiver);
		}

		//	速度設定
		//	速度が０の時は初速度を設定
		if (m_nSpeed == 0) m_nSpeed = p_Res.shot.SPEED;

		//	ラリー中は加速度を加算していく
		if (m_nSpeed > 0)
		{
			//	判定結果が指定されている（ＣＰＵ）
			if (p_Res.adjust != Shot_AdjustTable.eAdjust.NONE)
			{
				m_Adjust = p_Res.adjust;
			}
			//	判定が指定されていない（プレイヤー）
			else
			{
				//	判定取得
				float	a_fSize = ball.transform.localScale.x;
				m_Adjust = def.instance.adjustTable.Adjust_Get(a_fSize);
			}

			//	加速値を取得して加算
			int a_nAccSpd = AccSpeed_Get(m_Adjust, p_Res);
			m_nSpeed += a_nAccSpd;

			//	速度が上がっているなら加点（プレイヤー）
			if (m_Receiver == eReceiver.PLAYER && m_UseType == eUseType.Game)
			{
				EvaluationManager.instance.AttackCheck(a_nAccSpd);
			}

			//	最高速度と最低速度の制限
			if (m_nSpeed > def.c_nBallSpeed_Max)
			{
				m_nSpeed = def.c_nBallSpeed_Max;
			}
			if (m_nSpeed < def.c_nBallSpeed_Min)
			{
				m_nSpeed = def.c_nBallSpeed_Min;
			}

			//	ラリー回数を増やす
			m_nRallyCount++;

			if (m_UseType == eUseType.Game)
			{
				//	表示ラリー回数更新
				Header.instance.RallyTimes_Update(m_nRallyCount);
				//	ラリーの評価チェック
				EvaluationManager.instance.RallyCheck(m_nRallyCount);
			}
		}

		if (m_UseType == eUseType.Game)
		{
			//	速度表示を反映
			Header.instance.Speed_Update(m_nSpeed);
		}

		SoundHelper.eNo	a_SeNo = SoundHelper.eNo.Dummy;

		bool	a_bADChange = false;	//	攻守交替したか
		//	プレイヤーが打った時は判定結果を表示する
		if (receiver == eReceiver.PLAYER)
		{
			//	判定表示用ラベルを生成
			DecisionLabel.CreateAndPlay(p_Res.shot, m_Adjust);

			if (m_UseType == eUseType.Game)
			{
				//	評価のチェック
				EvaluationManager.instance.EnergyCheck(m_Adjust);
				//	平均点算出
				EvaluationManager.instance.TechniqueCheck(m_Adjust);
				//	判定カウンタ増加
				EvaluationManager.instance.shotAdjustCounter_Add(m_Adjust);
			}

			//	パートナーのパラメータ取得
			int					a_nPartnerID	= GameInfo.instance.partnerProfile.id;
			PlayerTable.Param	a_PartnerParam	= def.instance.playerTable.Param_Get(a_nPartnerID);

			//	ＣＰＵのボールに対応出来たか
			if (isChangeAttack)
			{
				//	攻守交代判定
				AttackChange_Decision();

				if (m_UseType == eUseType.Game)
				{
					//	バランス評価のチェック
					EvaluationManager.instance.BalanceCheck(m_Adjust);
				}

				//	攻守交替成功
				if(m_bChangeAttack_Suc || scn_game.c_bAutoPlay)
				{
					m_nAtkChangeCount	= 0;
					a_bADChange			= true;

					m_bChangeAttack_Suc = false;

					//	攻守交替に必要なスイング回数設定
					m_nChangeChallengeCount = def.instance.atkChangeTable.ChallengeTimes_Get(a_PartnerParam.Aggressive);
				}
				else
				{
					m_nChangeAttack_MissCount++;

					//	続けて要求する回数
					int a_nMissCount = def.instance.atkChangeTable.keyList[a_PartnerParam.Aggressive].ChallengeTimes;
					//	攻守交替に連続で失敗したらあきらめる
					if(m_nChangeAttack_MissCount >= a_nMissCount)
					{
						m_nAtkChangeCount			= 0;
						m_nChangeAttack_MissCount	= 0;

						//	攻守交替に必要なスイング回数設定
						m_nChangeChallengeCount = def.instance.atkChangeTable.ChallengeTimes_Get(a_PartnerParam.Aggressive);
					}
				}
			}
		}
        //  CPUが打った時
		else
		{
			//	攻守交替のタイミング用のカウンタを増やす
			if(!isChangeAttack)	m_nAtkChangeCount++;

			//	攻守交替のタイミング
			if (isChangeAttack)
			{
				float	a_fWaitTime = 1.2f;
				string	a_TexName	= (p_SwingMan.state == SwingManager.eState.Offence) ? "sgn_smash" + def.c_TextureLanguageStr : "sgn_dust" + def.c_TextureLanguageStr;

				//	攻守によってＳＥを変える
				a_SeNo = (p_SwingMan.state == SwingManager.eState.Deffence) ? SoundHelper.eNo.se_change : SoundHelper.eNo.se_change2;
				SoundHelper.instance.SePlay(a_SeNo);

                //	ボールのアニメーションを一時停止
/*                Move_Stop();

                if (m_UseType == eUseType.Game)
                {
                    //	時計を止める
                    Header.instance.GamePause();
                }

                yield return new WaitForSeconds(a_fWaitTime);

                //	ボールのアニメーションを再開
                Move_Resume();

                if (m_UseType == eUseType.Game)
                {
                    //	時計を動かす
                    Header.instance.GameStart();
                }
*/

                yield return 0;
			}
		}

		//	ヒット時のＳＥを鳴らす
		StartCoroutine(ShotSE_Exec(a_bADChange, p_SwingMan, p_Res));

        //  コンボによるSE
        if(m_nRallyCount % m_nRallySEInterval == 0)
        {
            m_nRallySENum++;
            m_nRallySENum %= m_nRallySEMax;

            try
            {
                string  a_SEName    = string.Format("se_combo{0}", m_nRallySENum);
                a_SeNo  = (SoundHelper.eNo)Enum.Parse(typeof(SoundHelper.eNo), a_SEName);

                //	SE:ヒット音
                SoundHelper.instance.SePlay(a_SeNo, false);
            }
            catch
            {
                a_SeNo = SoundHelper.eNo.se_combo1;
                SoundHelper.instance.SePlay(a_SeNo, false);
            }
        }

		//	レシーバーを変える
		Receiver_Change();

		//	アニメーション処理
		StartCoroutine(Animation_Exec(p_SwingMan, p_Res.shot));

		//	プレイヤーが打つ時はガイドを表示する
		StartCoroutine(GuideAnimation());

		if (a_bADChange)	p_ChangeAttack();	//	攻守交替処理

		p_CallBack();
	}

    /// <summary>
    /// ボールの動きを停止する
    /// </summary>
    public void Move_Stop()
    {
        if (ball == null) return;
        iTween.Stop(ball);
    }

    /// <summary>
    /// ボールの動きを再開する
    /// </summary>
    public void Move_Resume()
    {
        if (ball == null)   return;
        iTween.Resume(ball);
    }

	/// <summary>
	/// ガイドアニメーション
	/// </summary>
	/// <returns></returns>
	public IEnumerator	GuideAnimation()
	{
		float	a_fAniTime = 0.25f;
		if(m_Receiver == eReceiver.CPU)
		{
			//	アルファのアニメーション
			StartCoroutine(GameUtils.AlphaAnim(guideTex.color, a_fAniTime, 1, 0, (p_Color) =>
				{
					guideTex.color = p_Color;
				}
			));

			yield return new WaitForSeconds(a_fAniTime);

			//	アルファを元に戻す
			guideTex.color = GameUtils.Alpha_Set(guideTex.color, 150f / 255f);
			//	非表示にする
			guideTex.gameObject.SetActive(false);
		}
		if(m_Receiver == eReceiver.PLAYER)
		{
			guideTex.gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// 攻守交替判定
	/// </summary>
	public void	AttackChange_Decision()
	{
		//	判定が「Perfect」か「Good」なら攻守交替成功
		if ((m_Adjust == Shot_AdjustTable.eAdjust.PERFECT) || (m_Adjust == Shot_AdjustTable.eAdjust.GOOD))
		{
			m_bChangeAttack_Suc = true;
		}
		else
		{
			m_bChangeAttack_Suc = false;
		}
	}

	public IEnumerator	ShotSE_Exec(bool p_bAdChange, SwingManager p_SwingMan, SwingManager.stResShotData p_Res)
	{
		//	音量を取得
		EtcParam.Param	a_Param = def.instance.etcTable.nameList["SHOT_SE_VOLUME"];
		float			a_fVol	= (m_Receiver == eReceiver.PLAYER) ? a_Param.P1 : a_Param.P2;

		string			a_SEName	= p_Res.shot.SE_HIT;	//	ＳＥ名
		if(p_Res.shot.ID == def.instance.shotTable.foreShotData.ID)
		{
			switch (m_Adjust)
			{
				case	Shot_AdjustTable.eAdjust.POOR:
					{
						a_SEName += "_poor";
					}
					break;

				case Shot_AdjustTable.eAdjust.GOOD:
					{
						a_SEName += "_good";
					}
					break;

				case Shot_AdjustTable.eAdjust.PERFECT:
					{
						a_SEName += "_perfect";
					}
					break;
			}

			int a_nCount = SoundHelper.instance.foreShotCountList[m_Adjust];
			int a_nNo = UnityEngine.Random.Range(0, a_nCount);

			a_SEName += a_nNo.ToString();
		}

		SoundHelper.eNo a_SeNo		= (SoundHelper.eNo)Enum.Parse(typeof(SoundHelper.eNo), a_SEName);

		//	SE:ヒット音
		SoundHelper.instance.SePlay(a_SeNo, false, a_fVol);

		yield return new WaitForSeconds(0.3f);

		//	攻守交替時
		if(p_bAdChange)
		{
			if(p_SwingMan.state == SwingManager.eState.Offence)
			{
				if (p_Res.shot.SWINGTYPE_JPN == "ダストショット")
				{
					a_SeNo = SoundHelper.eNo.se_voice_oh;	
				}
				else
				if (p_Res.shot.SWINGTYPE_JPN == "フックショット")
				{
					a_SeNo = SoundHelper.eNo.se_bastreturn;
					SoundHelper.instance.SePlay(SoundHelper.eNo.se_fu);

				}
			}
			else
			{
				if (p_Res.shot.SWINGTYPE_JPN == "フックショット")
				{
					a_SeNo = SoundHelper.eNo.se_voice_oh;
				}
				else
				if (p_Res.shot.SWINGTYPE_JPN == "ダストショット")
				{
					a_SeNo = SoundHelper.eNo.se_bastreturn;
					SoundHelper.instance.SePlay(SoundHelper.eNo.se_fu);
				}
			}

			//	攻守交替時に再生するＳＥ
			SoundHelper.instance.SePlay(a_SeNo);
		}

		//	トレーニング時のＳＥ
		if(scn_training.c_bTraining)
		{
			//	成功音
			if(scn_training.isConsistentShot(p_Res.shot.ID))
			{
				SoundHelper.instance.SePlay(SoundHelper.eNo.se_bastreturn);
			}
			//	失敗音
			else
			{
				SoundHelper.instance.SePlay(SoundHelper.eNo.se_voice_oh);
			}
		}
	}

	/// <summary>
	/// 加速値を取得する
	/// </summary>
	/// <param name="p_Adjust"></param>
	/// <param name="p_ShotData"></param>
	/// <returns></returns>
	public int	AccSpeed_Get(Shot_AdjustTable.eAdjust p_Adjust,  SwingManager.stResShotData p_ShotData)
	{
		int a_nAccSpd = 0;
		//	判定結果に応じて加速度を取得する
		switch (m_Adjust)
		{
			case Shot_AdjustTable.eAdjust.POOR:
				{
					a_nAccSpd = p_ShotData.shot.ACC_SPEED_POOR;
				}
				break;

			case Shot_AdjustTable.eAdjust.GOOD:
				{
					a_nAccSpd = p_ShotData.shot.ACC_SPEED_GOOD;
				}
				break;

			case Shot_AdjustTable.eAdjust.PERFECT:
				{
					a_nAccSpd = p_ShotData.shot.ACC_SPEED_PERFECT;
				}
				break;
		}

		return a_nAccSpd;
	}

	/// <summary>
	///	レシーバーに対するボールの目標スケール
	/// </summary>
	Dictionary<eReceiver, Vector3>	m_ToBallScaleList = new Dictionary<eReceiver, Vector3>()
		{
			{eReceiver.NONE,	Vector3.one},
			{eReceiver.PLAYER,	new Vector3(0.1f, 0.1f)},
			{eReceiver.CPU,		new Vector3(3f, 3f)}
		}
	;

	/// <summary>
	/// アニメーション処理
	/// </summary>
	public IEnumerator	Animation_Exec(SwingManager p_SwingMan, ShotData.Param p_Shot)
	{
		//	目標サイズ
		Vector2 a_ToSize = m_ToBallScaleList[m_Receiver];
		//	到達時間算出（「m_fArrivalTime」と「m_fGraceTime」を設定）
		ArrivalTime_Culc();

		//	既に「iTween」がアタッチされている時は削除
		if (gameObject.GetComponentsInChildren<iTween>() != null)
		{
			iTween[]	a_Tweens = gameObject.GetComponentsInChildren<iTween>();
			foreach(iTween t in a_Tweens)
			{
				Destroy(t);
			}
		}
		
		//	アニメーション再生速度補正取得
		EtcParam.Param	a_Param		= def.instance.etcTable.nameList["BALLANIMATION_SPEED_REV"];
		//	アニメーションの速度に対する補正
		float			a_fDiv		= (m_Receiver == eReceiver.CPU) ? a_Param.P1 : a_Param.P2;
		//	実際のアニメーション時間
		float			a_fAniTime	= m_fArrivalTime / a_fDiv;

		//	ＣＰＵが打ち返した時はボールが表示されるまでにウェイトをかける
		//	4/7	理由としては、スピードが速くなってくるとプレイヤーが次の動作に入る前に振り始め判定を取ってしまうため
		if(m_Receiver == eReceiver.PLAYER)
		{
			float	a_fViewTime = a_fAniTime / 2;	//	到達時間の半分
			m_fArrivalTime += a_fViewTime;	//	表示補正時間を到達時間に加算しておく
			yield return new WaitForSeconds(a_fViewTime);
		}

		//	ボールを飛ばす
		iTween.ScaleTo(ball, iTween.Hash("x", a_ToSize.x, "y", a_ToSize.y, "time", a_fAniTime, "easetype", iTween.EaseType.linear));

		float	a_fTarget	= 2f;
		float	a_fFrom		= (m_Receiver == eReceiver.CPU) ? 1 : 0;
		float	a_fTo		= (m_Receiver == eReceiver.CPU) ? 0 : 1;
		if (m_Receiver == eReceiver.CPU)
		{
			//	ボールのスケールが２を超えたらアルファのアニメーション
			while (ball.transform.localScale.x < a_fTarget && ball.transform.localScale.y < a_fTarget)	yield return 0;
			a_fAniTime = m_fArrivalTime / a_fDiv;
		}
		else
		{
			//	サーブ時にボールの座標がズレる対策
			ball.transform.localPosition = Vector3.zero;

			float	a_fDef = m_fArrivalTime / Mathf.FloorToInt((3f - 0.1f) * 10);
			a_fAniTime = Mathf.FloorToInt((3 - a_fTarget) * 10) * a_fDef;
		}

		//	アルファのアニメーション
		StartCoroutine(GameUtils.AlphaAnim(ballTex.color, a_fAniTime, a_fFrom, a_fTo, (p_Color) =>
			{
				ballTex.color = p_Color;
			}
		));
	}

	/// <summary>
	/// サーブ時のアニメーション
	/// </summary>
	/// <param name="p_SwingMan"></param>
	/// <param name="p_Shot"></param>
	/// <returns></returns>
	public IEnumerator	ServeAnimation_Exec(SwingManager p_SwingMan, ShotData.Param p_Shot)
	{
		m_bServeAnim = true;	//	サーブ時のアニメーション開始

		//	ボールの位置を初期位置にする
		ball.transform.localPosition = Vector3.zero;

		//	ガイド表示
		guideTex.gameObject.SetActive(true);

		//	到達時間算出（「m_fArrivalTime」と「m_fGraceTime」を設定）
		ArrivalTime_Culc();

		//	既に「iTween」がアタッチされている時は削除
		if (gameObject.GetComponent<iTween>()) Destroy(gameObject.GetComponent<iTween>());

		//	アニメーション時間
		float	a_fAniTime = 1f;

		//	ボールを浮かして元の位置に戻す
		iTween.ScaleTo(ball, iTween.Hash("x", 1.75f, "y", 1.75f, "time", a_fAniTime, "easetype", iTween.EaseType.easeOutSine));
		iTween.ScaleTo(ball, iTween.Hash("x", 0.2f, "y", 0.2f, "time", a_fAniTime, "delay", a_fAniTime, "easetype", iTween.EaseType.easeInSine));

		yield return new WaitForSeconds(a_fAniTime * 2);

		//	サーブに成功したら終了
		if (m_bServed)	yield break;

		m_bServeAnim = false;	//	サーブ時のアニメーション終了

		//	ガイド非表示
		guideTex.gameObject.SetActive(false);

		yield return 0;
	}

	/// <summary>
	/// 到達時間を算出する
	/// </summary>
	/// <returns></returns>
	public void	ArrivalTime_Culc()
	{
		//	１秒当たり進む距離（距離（m） / 分 / 秒）
		float	a_fDistanceToSecond = (float)(m_nSpeed * 1000f) / 60f / 60f;
		
		//	到達時間補正取得
		EtcParam.Param	a_Param		= def.instance.etcTable.nameList["SHOT_ARRIVAL_TIME_REV"];
		float			a_fTimeRev	= (m_Receiver == eReceiver.CPU) ? a_Param.P1 : a_Param.P2;
		a_fTimeRev = (100 + a_fTimeRev) / 100f;

		//	到達時間設定
		m_fArrivalTime	= def.c_nDistance / (a_fDistanceToSecond / a_fTimeRev);

		//	猶予時間設定
		m_fGraceTime = def.c_nPlayerGracetime;
	}

	/// <summary>
	/// 到達時間を算出する（速度を指定して返す）
	/// </summary>
	/// <param name="p_nSpeed"></param>
	/// <returns></returns>
	public float	ArrivalTime_Get(int p_nSpeed)
	{
		//	１秒当たり進む距離（距離（m） / 分 / 秒）
		float	a_fDistanceToSecond = (float)(p_nSpeed * 1000f) / 60f / 60f;
		
		//	到達時間補正取得
		EtcParam.Param	a_Param		= def.instance.etcTable.nameList["SHOT_ARRIVAL_TIME_REV"];
		float			a_fTimeRev	= (a_Param.P1 + a_Param.P2) / 2;
		a_fTimeRev = (100 + a_fTimeRev) / 100f;

		//	到達時間設定
		float	a_fArrivalTime = def.c_nDistance / (a_fDistanceToSecond / a_fTimeRev);

		return	a_fArrivalTime;
	}

	/// <summary>
	/// レシーバーを変える
	/// </summary>
	public void	Receiver_Change()
	{
		m_Receiver = (m_Receiver == eReceiver.PLAYER) ? eReceiver.CPU : eReceiver.PLAYER;
	}

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		//	ステートを調べる
		State_Check();
		//	到達時間を更新する
		ArrivalTime_Update();
	}

	/// <summary>
	/// 到着時間を更新する
	/// </summary>
	public void		ArrivalTime_Update()
	{
		//	ボールが到達する時間を更新
		if (m_fArrivalTime > 0)
		{
			m_fArrivalTime -= Time.deltaTime;

			if (m_fArrivalTime < 0) m_fArrivalTime = 0;
		}
		else
		//	ボールが到達したら猶予時間を更新
		if (m_fArrivalTime == 0)
		{
			m_fGraceTime -= Time.deltaTime;

			if (m_fGraceTime < 0) m_fGraceTime = 0;
		}
	}
}
