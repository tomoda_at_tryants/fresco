﻿using UnityEngine;
using System.Collections;

public class ResultItem : MonoBehaviour {

	public UISprite	titleSp;
	public UILabel	numLabel;

	bool	m_bSkip;
	/// <summary>
	/// スキップするか
	/// </summary>
	public bool	skip
	{
		get { return m_bSkip; }
		set { m_bSkip = value; }
	}

	// Use this for initialization
	void Start () {
		titleSp.gameObject.SetActive(false);
		numLabel.gameObject.SetActive(false);
		m_bSkip = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void	Animation(int p_nNum)
	{
		titleSp.gameObject.SetActive(true);
		numLabel.gameObject.SetActive(true);
		numLabel.text = p_nNum.ToString();
	}

	public IEnumerator	Anim_Exec(int p_nNum)
	{
		titleSp.gameObject.SetActive(true);

		float	a_fTime = 0f;
		float	a_fWait = 0.5f;
		while(a_fTime < a_fWait)
		{
			a_fTime += Time.deltaTime;
			if (m_bSkip)	break;

			yield return 0;
		}
		a_fTime = 0f;

		numLabel.gameObject.SetActive(true);
		numLabel.text = p_nNum.ToString();

		if(!m_bSkip)
		{
			numLabel.transform.localScale = new Vector3(5, 5);

			GameObject a_Target = numLabel.gameObject;
			float	a_fAniTime = 0.5f;
			iTween.ScaleTo(a_Target, iTween.Hash("x", 1, "y", 1, "time", a_fAniTime, "easetype", iTween.EaseType.linear));

			//	アルファアニメーション
			StartCoroutine(GameUtils.AlphaAnim(numLabel.color, a_fAniTime - 0.1f, 0, 1, (p_Color) =>
				{
					numLabel.color = p_Color;
				}
			));

			a_fWait = a_fAniTime;
			while (a_fTime < a_fWait)
			{
				a_fTime += Time.deltaTime;
				if (m_bSkip)
				{
					Destroy(a_Target.GetComponent<iTween>());
					a_Target.transform.localScale = Vector3.one;

					break;
				}

				yield return 0;
			}
		}
	}
}
