﻿using UnityEngine;
using System.Collections;

public class TouchSensor : MonoBehaviour {

	public BoxCollider		boxCollider;
	public UIButtonMessage	pressEvent;
	public UIButtonMessage	releaseEvent;
	public UISprite			sprite;

	bool		m_bTouching;
	/// <summary>
	/// タッチされている状態
	/// </summary>
	public bool	touching
	{
		get { return m_bTouching; } 
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="p_EventTarget"></param>
	/// <param name="p_PressEvent"></param>
	/// <param name="p_ReleaseEvent"></param>
	public void	Init(GameObject p_EventTarget, string p_PressEvent, string p_ReleaseEvent)
	{
		pressEvent.target			= p_EventTarget;
		pressEvent.functionName		= p_PressEvent;

		releaseEvent.target			= p_EventTarget;
		releaseEvent.functionName	= p_ReleaseEvent;
	}

	/// <summary>
	/// スプライトの表示設定（trueなら表示1）
	/// </summary>
	public void	SetView(bool p_bActive)
	{
		sprite.gameObject.SetActive(p_bActive);
	}

	/// <summary>
	/// タッチされた時の処理
	/// </summary>
	public void	Press()
	{
		SetView(true);
		m_bTouching = true;
	}

	/// <summary>
	/// タッチが解除された時の処理
	/// </summary>
	public void	Release()
	{
		SetView(false);
		m_bTouching = false;
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
