﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲーム中の評価等を管理する
/// </summary>
public class EvaluationManager : MonoBehaviour {

	/// <summary>
	/// 評価のタイプ
	/// </summary>
	public enum eType
	{
		ENERGY,
		ATTACK,
		RALLY,
		BALANCE,
		TECHNIQUE,

		MAX
	}

	int m_nHitCount;
	/// <summary>
	/// １ゲーム内での打球数
	/// </summary>
	public int allHitCount
	{
		get { return m_nHitCount; }
	}

	int m_nPlayerHitCount;
	/// <summary>
	/// プレイヤーの打球数
	/// </summary>
	public int	playerHitCount
	{
		get { return m_nPlayerHitCount; }
	}

	/// <summary>
	/// 評価に必要な情報
	/// </summary>
	[System.Serializable]
	public struct stScore
	{
		//	計算用

		public int	energyScore;
		public int	attackScore;
		public int	rallyScore;
		public int	balanceScore;
		public int	techniqueScore;

		/// <summary>
		/// 合計点
		/// </summary>
		public int	totalScore
		{
			get 
			{
				return energyScore + attackScore + rallyScore + balanceScore + techniqueScore; 
			}
		}
	}

	/// <summary>
	/// 判定別カウンター
	/// </summary>
	public Dictionary<Shot_AdjustTable.eAdjust, int> shotAdjustCounter = new Dictionary<Shot_AdjustTable.eAdjust, int>()
	{
		{Shot_AdjustTable.eAdjust.NONE,		0},
		{Shot_AdjustTable.eAdjust.POOR,		0},
		{Shot_AdjustTable.eAdjust.GOOD,		0},
		{Shot_AdjustTable.eAdjust.PERFECT,	0}
	};

	/// <summary>
	/// ショットの判定カウンターを増やす
	/// </summary>
	/// <param name="p_Adjust"></param>
	public void shotAdjustCounter_Add( Shot_AdjustTable.eAdjust p_Adjust)
	{
		if(p_Adjust != null || shotAdjustCounter.ContainsKey(p_Adjust))
		{
			int	a_nCount = shotAdjustCounter[p_Adjust];
			a_nCount++;

			shotAdjustCounter[p_Adjust] = a_nCount;
		}
	}

	static EvaluationManager	s_Instance;
	public static EvaluationManager	instance
	{
		get { return s_Instance; }
	}

	stScore m_ScoreData;
	/// <summary>
	/// 点数データ
	/// </summary>
	public stScore	scoreData
	{
		get { return m_ScoreData; }
	}

	/// <summary>
	///	計算用リスト
	/// </summary>
	Dictionary<eType, int>	m_CulcList = new Dictionary<eType, int>()
	{
		{eType.ENERGY,		0},
		{eType.ATTACK,		0},
		{eType.RALLY,		0},
		{eType.BALANCE,		0},
		{eType.TECHNIQUE,	0}
	};

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static EvaluationManager	Create()
	{
		GameObject a_Obj = new GameObject("EvaluationManager");
		s_Instance = a_Obj.AddComponent<EvaluationManager>();

		s_Instance.Init();
		return	s_Instance;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init()
	{
		m_ScoreData = new stScore();
	}

	/// <summary>
	/// 打った球の数を増やす
	/// </summary>
	public void	HitCount_Add(BallManager.eReceiver p_Receiver)
	{
		m_nHitCount += 1;
		//	プレイヤーの打球数を増やす
		m_nPlayerHitCount += (p_Receiver == BallManager.eReceiver.PLAYER) ? 1 : 0;
	}

	/// <summary>
	/// エネルギーの評価を調べる
	/// （「Perfect」の回数）
	/// </summary>
	/// <param name="p_Adjsut"></param>
	public void	EnergyCheck(Shot_AdjustTable.eAdjust p_Adjsut)
	{
		int	a_nCurrent = m_CulcList[eType.ENERGY];
		a_nCurrent += (p_Adjsut == Shot_AdjustTable.eAdjust.PERFECT) ? 1 : 0;
		m_CulcList[eType.ENERGY] = a_nCurrent;

		//	割合
		float	a_fRev		= 2f;
		float	a_fRate		= ((float)a_nCurrent / (float)m_nPlayerHitCount) * a_fRev;
		if (a_fRate > 1f)	a_fRate = 1f;

		//	エネルギーの最高点
		int		a_nMaxScore = MaxScore_Get(eType.ENERGY);

		m_ScoreData.energyScore = Mathf.RoundToInt(a_nMaxScore * a_fRate);
	}

	/// <summary>
	/// アタックの評価を調べる
	/// （加速がプラスなら加算）
	/// </summary>
	/// <param name="p_nSpeed"></param>
	public void	AttackCheck(int	p_nSpeed)
	{
		//	現在の計算用得点
		int a_nCurrent = m_CulcList[eType.ATTACK];
		//	加速しているなら加点
		a_nCurrent += (p_nSpeed >= 0) ? 1 : 0;

		//	リスト更新
		m_CulcList[eType.ATTACK] = a_nCurrent;
		float	a_fRev	= 1.1f;
		//	割合
		float	a_fRate = ((float)a_nCurrent / (float)m_nPlayerHitCount) * a_fRev;
		if (a_fRate > 1f)	a_fRate = 1f;

		//	アタックの最高点
		int a_nMaxScore = MaxScore_Get(eType.ATTACK);

		m_ScoreData.attackScore = Mathf.RoundToInt(a_nMaxScore * a_fRate);
	}

	public int	ballSpeed_Max;

	/// <summary>
	///	ラリーの評価を調べる
	///	（ラリーの最大回数）
	/// </summary>
	public void	RallyCheck(int p_nRallyCount)
	{
		//	現在の計算用得点
		int	a_nCurrent = m_CulcList[eType.RALLY];
		if(a_nCurrent < p_nRallyCount)
		{
			a_nCurrent += 1;
			m_CulcList[eType.RALLY] = a_nCurrent;
		}

		//	最高速度取得
		if (ballSpeed_Max == 0)	ballSpeed_Max = def.c_nBallSpeed_Max;

		//	最高速度の到達時間
		float	a_fArrivalTime	= BallManager.instance.ArrivalTime_Get(ballSpeed_Max);
		//	最高速度で打ち合った場合のラリー数
		float	a_fRev = 1.1f;	//	補正
		int		a_nTheoryCount	= Mathf.FloorToInt(def.c_nGameTime / (a_fArrivalTime * a_fRev));

		//	全て最高速度で打ち合った時のラリー数との割合
		float	a_fRate			= (float)a_nCurrent / (float)a_nTheoryCount;
		//	ラリーの最高点
		int		a_nMaxScore		= MaxScore_Get(eType.RALLY);

		//	最大点数制限
		int a_nScore = Mathf.RoundToInt(a_nMaxScore * a_fRate);
		if (a_nScore > a_nMaxScore)	a_nScore = a_nMaxScore;

		m_ScoreData.rallyScore = a_nScore;
	}

	Dictionary<Shot_AdjustTable.eAdjust, int> m_TechniquePoint = new Dictionary<Shot_AdjustTable.eAdjust, int>()
	{
		{Shot_AdjustTable.eAdjust.NONE,		  -20},		//	ミス
		{Shot_AdjustTable.eAdjust.POOR,			1},
		{Shot_AdjustTable.eAdjust.GOOD,			2},
		{Shot_AdjustTable.eAdjust.PERFECT,		3}
	};

	/// <summary>
	/// テクニックの評価を調べる
	/// （判定を点数化し、その平均を割り出す）
	/// </summary>
	public void	TechniqueCheck(Shot_AdjustTable.eAdjust p_Adjust)
	{
		//	現在の計算用得点
		int a_nCurrent = m_CulcList[eType.TECHNIQUE];
		//	今回の判定点を加算
		a_nCurrent += m_TechniquePoint[p_Adjust];

		if(a_nCurrent < 0)	a_nCurrent = 0;

		//	リスト更新
		m_CulcList[eType.TECHNIQUE] = a_nCurrent;

		//	全て「Perfect」だった場合の点数
		int		a_nAllPerfect	= m_TechniquePoint[Shot_AdjustTable.eAdjust.PERFECT] * m_nPlayerHitCount;
		float	a_fRev			= 1.2f;
		//	全「Perfect」だった場合との現在の点数の割合
		float			a_fRate = ((float)a_nCurrent / (float)a_nAllPerfect) * a_fRev;
		if (a_fRate > 1f)	a_fRate = 1;

		//	最高点取得
		int a_nMaxScore = MaxScore_Get(eType.TECHNIQUE);

		m_ScoreData.techniqueScore = Mathf.RoundToInt(a_nMaxScore * a_fRate);
	}

	/// <summary>
	/// 各評価の最高点を返す
	/// </summary>
	/// <param name="p_Type"></param>
	/// <returns></returns>
	public int	MaxScore_Get(eType p_Type)
	{
		string			a_Key	= string.Format("{0}_SCORE", p_Type.ToString());
		EtcParam.Param	a_Param = def.instance.etcTable.nameList[a_Key];

		int a_nMaxScore = (int)a_Param.P0;

		return a_nMaxScore;
	}

	/// <summary>
	/// 攻守交替
	/// </summary>
	int m_nChangeCount;

	Dictionary<Shot_AdjustTable.eAdjust, int> m_BalancePoint = new Dictionary<Shot_AdjustTable.eAdjust, int>()
	{
		{Shot_AdjustTable.eAdjust.NONE,		    0},		//	ミス
		{Shot_AdjustTable.eAdjust.POOR,			1},
		{Shot_AdjustTable.eAdjust.GOOD,			2},
		{Shot_AdjustTable.eAdjust.PERFECT,		3}
	};

	/// <summary>
	/// バランスの評価を調べる
	/// （攻守交替時の対応）
	/// </summary>
	public void	BalanceCheck(Shot_AdjustTable.eAdjust p_Adjust)
	{
		m_nChangeCount++;	//	攻守交替にチャレンジした回数を加算

		//	現在の合計点数
		int a_nCurrent = m_CulcList[eType.BALANCE];
		//	今回の判定点を加算
		a_nCurrent += m_BalancePoint[p_Adjust];

		//	点数更新
		m_CulcList[eType.BALANCE] = a_nCurrent;

		//	全て「Perfect」だった場合の点数
		int		a_nAllPerfect	= m_BalancePoint[Shot_AdjustTable.eAdjust.PERFECT] * m_nChangeCount;
		//	全「Perfect」だった場合との現在の点数の割合
		float	a_fRate			= (float)a_nCurrent / (float)a_nAllPerfect;
		//	最高点取得
		int		a_nMaxScore		= MaxScore_Get(eType.BALANCE);

		m_ScoreData.balanceScore = Mathf.RoundToInt(a_nMaxScore * a_fRate);
	}
}
