﻿using UnityEngine;
using System.Collections;

public class ResultManager : MonoBehaviour {

	public UITexture	background;

	public GameObject	freeButtonRoot;
	public GameObject	challengeButtonRoot;
	public GameObject	viewRoot;

	public UIButtonTween	retryButton;
	public UIButtonTween	quitButton;
	public UIButtonTween	resultButton;
	public UIButtonTween	skipButton;

	public ResultItem	energyItem;
	public ResultItem	attackItem;
	public ResultItem	rallyItem;
	public ResultItem	balanceItem;
	public ResultItem	techniqueItem;
	public ResultItem	totalItem;

	public UILabel		perfectCountLabel;
	public UILabel		goodCountLabel;
	public UILabel		poorCountLabel;
	public UILabel		missCountLabel;

	/// <summary>
	/// スキップするか
	/// </summary>
	bool	m_bSkip
	{
		get
		{
			if(energyItem.skip && attackItem.skip && rallyItem.skip && balanceItem.skip && techniqueItem)
			{
				return true;
			}
			return	false;
		}
	}

	/// <summary>
	/// スキップ可能か
	/// </summary>
	public bool	m_bCanSkip;

	public IEnumerator Play()
	{
		//	背景画像をロード
		string	a_FileName = "Bg/bg_result";
		background.mainTexture = ResourceManager.TextureLoad(a_FileName, false);

		m_bCanSkip = true;	//	スキップ可能

		//	ボタンは非表示にする
		freeButtonRoot.SetActive(false);
		challengeButtonRoot.SetActive(false);

//		EvaluationManager	a_Evaluation = EvaluationManager.instance;

		//	各項目のアニメーション
		float	a_fInterval = 0.5f;

//	各判定数
		perfectCountLabel.text = EvaluationManager.instance.shotAdjustCounter[Shot_AdjustTable.eAdjust.PERFECT].ToString();
		goodCountLabel.text = EvaluationManager.instance.shotAdjustCounter[Shot_AdjustTable.eAdjust.GOOD].ToString();
		poorCountLabel.text = EvaluationManager.instance.shotAdjustCounter[Shot_AdjustTable.eAdjust.POOR].ToString();
		missCountLabel.text = EvaluationManager.instance.shotAdjustCounter[Shot_AdjustTable.eAdjust.NONE].ToString();

//	エネルギー
		int a_nViewNum = EvaluationManager.instance.scoreData.energyScore;
		yield return	StartCoroutine(energyItem.Anim_Exec(a_nViewNum));
		yield return	StartCoroutine(Wait(a_fInterval));	//	スキップ判定用待機処理

//	アタック		
		a_nViewNum = EvaluationManager.instance.scoreData.attackScore;
		if(m_bSkip)
		{
			attackItem.Animation(a_nViewNum);
		}
		else
		{
			yield return StartCoroutine(attackItem.Anim_Exec(a_nViewNum));
			yield return StartCoroutine(Wait(a_fInterval));
		}
	
//	ラリー
		a_nViewNum = EvaluationManager.instance.scoreData.rallyScore;
		if (m_bSkip)
		{
			rallyItem.Animation(a_nViewNum);
		}
		else
		{
			yield return StartCoroutine(rallyItem.Anim_Exec(a_nViewNum));
			yield return StartCoroutine(Wait(a_fInterval));
		}

//	バランス
		a_nViewNum = EvaluationManager.instance.scoreData.balanceScore;
		if (m_bSkip)
		{
			balanceItem.Animation(a_nViewNum);
		}
		else
		{
			yield return StartCoroutine(balanceItem.Anim_Exec(a_nViewNum));
			yield return StartCoroutine(Wait(a_fInterval));
		}

//	テクニック
		a_nViewNum = EvaluationManager.instance.scoreData.techniqueScore;
		if (m_bSkip)
		{
			techniqueItem.Animation(a_nViewNum);
		}
		else
		{
			yield return StartCoroutine(techniqueItem.Anim_Exec(a_nViewNum));
			yield return StartCoroutine(Wait(a_fInterval));
		}

		m_bCanSkip = false;	//	スキップできない

//	合計
		yield return StartCoroutine(totalItem.Anim_Exec(EvaluationManager.instance.scoreData.totalScore));
		yield return new WaitForSeconds(1f);

		skipButton.gameObject.SetActive(false);

		//	ボタンの表示設定
		bool isFree = false;
		if (GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)
		{
			isFree = true;
		}

		freeButtonRoot.SetActive(isFree);
		challengeButtonRoot.SetActive(!isFree);

		yield return 0;
	}

	/// <summary>
	/// 待機処理
	/// </summary>
	/// <param name="p_fWait"></param>
	/// <returns></returns>
	public IEnumerator	Wait(float p_fWait)
	{
		float	a_fTime = 0f;
		while(a_fTime < p_fWait)
		{
			a_fTime += Time.deltaTime;
			if (m_bSkip)	break;

			yield return 0;
		}
	}

	/// <summary>
	/// 画面がタッチされた時の処理
	/// </summary>
	public void	ViewDidPush()
	{
		if (!m_bCanSkip)	return;

		energyItem.skip		= true;
		attackItem.skip		= true;
		rallyItem.skip		= true;
		balanceItem.skip	= true;
		techniqueItem.skip	= true;
	}
}
