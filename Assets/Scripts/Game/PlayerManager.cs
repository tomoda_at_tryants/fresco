﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	SwingManager m_SwingManager;
	/// <summary>
	/// スイングマネージャー
	/// </summary>
	public SwingManager swingMan
	{
		get { return m_SwingManager; }
		set { m_SwingManager = value; }
	}

	TouchSensorRoot	m_Sensor;
	/// <summary>
	/// タッチセンサー
	/// </summary>
	public TouchSensorRoot sensor
	{
		get { return m_Sensor; }
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static PlayerManager Create(System.Action p_ChangeAttackCallBack)
	{
		GameObject		a_Obj	= new GameObject("PlayerManager");
		PlayerManager	me		= a_Obj.AddComponent<PlayerManager>();

		me.Init(p_ChangeAttackCallBack);

		return me;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init(System.Action p_ChangeAttackCallBack)
	{
		//	スイングマネージャー
		m_SwingManager = SwingManager.Create(SwingManager.eManager.Player, SwingManager.eState.Deffence, p_ChangeAttackCallBack, BallManager.instance.Hit_Exec);

		//	センサーの初期化
		m_Sensor = TouchSensorRoot.Create(gameObject, "SensorPressDidPush", "SensorReleaseDidPush");
	}

	/// <summary>
	/// 振り始めデータを取得したか
	/// </summary>
	public bool isGetBeginSwingData
	{
		get
		{
			return (swingMan.swingData.beginAcc != Vector3.zero) ? true : false;
		}
	}

	/// <summary>
	/// 振り始めデータを取得する
	/// </summary>
	public void	BeginSwingData_Get()
	{
		//	ＣＰＵがロブを打ってきた時はボールが打てるタイミングになったら音を鳴らす
		//	5/27　ロブショットの時、スイングするタイミングが他に比べて取りづらいため
		if(BallManager.instance.isChangeAttack && m_SwingManager.state == SwingManager.eState.Deffence)
		{
			//	振り始めの開始時
			SoundHelper.instance.SePlay(SoundHelper.eNo.se_change2);
		}

		//	初期加速度取得
		swingMan.swingData.beginAcc = def.swingSpeed;
	}

	/// <summary>
	/// ショット判定
	/// </summary>
	public void ShotDecision()
	{
		ShotTable	a_ShotTable = def.instance.shotTable;
		string		a_ShotName	= SwingType_Check();	//	スイングの種類を調べる

		//	ショットの種類が判別できない時は終了
		if (string.IsNullOrEmpty(a_ShotName)) return;

		BallManager a_BallMan = BallManager.instance;

		bool	a_bDecision = false;
		//	サーブ時はアニメーションが既に実行されていれば判定
		if ((a_BallMan.isServeAnim && !a_BallMan.isServed) || a_BallMan.isServed)
		{
			a_bDecision			= true;
			a_BallMan.isServed	= true;
		}

		//	ショットの種類が確定している
		if (a_bDecision)
		{
			//	ショットの種類等を調べる
			SwingManager.stResShotData	a_Res = new SwingManager.stResShotData(a_ShotTable.ShotData_Get(a_ShotName), Shot_AdjustTable.eAdjust.NONE);

			//	共通処理
			StartCoroutine(swingMan.Common_Exec(a_Res));
		}
	}

	/// <summary>
	/// スイングの種類判定を行う
	/// </summary>
	/// <returns></returns>
	public string SwingType_Check()
	{
		ShotTable	a_ShotTable		= def.instance.shotTable;
		Vector3		a_BeginAcc		= swingMan.swingData.beginAcc;
		Vector3		a_CurrentAcc	= def.swingSpeed;
		string		a_SwingType		= "";

		foreach (ShotData.Param param in a_ShotTable.paramList)
		{
			//	●振り始めの加速度判定
			int		a_nCond_Y1	= param.COND_BEGIN_ACC_Y_1;
			int		a_nCond_Y2	= param.COND_BEGIN_ACC_Y_2;
			bool	a_bUnder	= (param.COND_UNDER == "○")	? true : false;
			bool	a_bOver		= (param.COND_OVER == "○")		? true : false;

			//	最高条件値より上回っている
			if (a_bUnder)
			{
				if (a_BeginAcc.y > a_nCond_Y1) continue;
			}

			//	最低条件値より下回っている
			if (a_bOver)
			{
				if (a_BeginAcc.y < a_nCond_Y2) continue;
			}

			string	a_Cond		= param.END_OVER_OR_UNDER_X;
			int		a_nValue	= param.END_ACC_X;
			//	Ｘ方向の加速度チェック
			if (a_Cond == "under")
			{
				if (a_CurrentAcc.x > a_nValue) continue;
			}
			else
			if (a_Cond == "over")
			{
				if (a_CurrentAcc.x < a_nValue) continue;
			}

			a_Cond		= param.END_OVER_OR_UNDER_Y;
			a_nValue	= param.END_ACC_Y;
			//	Ｙ方向の加速度チェック
			if (a_Cond == "under")
			{
				if (a_CurrentAcc.y > a_nValue) continue;
			}
			else
			if (a_Cond == "over")
			{
				if (a_CurrentAcc.y < a_nValue) continue;
			}

			a_Cond = param.END_OVER_OR_UNDER_Z;
			a_nValue = param.END_ACC_Z;
			//	Ｚ方向の加速度チェック
			if (a_Cond == "under")
			{
				if (a_CurrentAcc.z > a_nValue) continue;
			}
			else
			if (a_Cond == "over")
			{
				if (a_CurrentAcc.z < a_nValue) continue;
			}

			a_SwingType = param.SWINGTYPE_JPN;
			break;
		}

		return a_SwingType;
	}

	/// <summary>
	/// センサーオブジェクトの初期化
	/// </summary>
	/// <param name="p_EventTarget"></param>
	/// <param name="p_PressEventName"></param>
	/// <param name="p_ReleaseEventName"></param>
	void Sensor_Init(GameObject p_EventTarget = null, string p_PressEventName = "", string p_ReleaseEventName = "")
	{
		//	「TouchSensor」の生成
		if (m_Sensor == null && p_EventTarget != null)
		{
			GameObject a_Obj = ResourceManager.PrefabLoadAndInstantiate("Game/TouchSensorObj", Vector3.zero);
			m_Sensor = a_Obj.GetComponent<TouchSensorRoot>();

			GameUtils.AttachChild(PanelManager.instance.alignmanLow.alignCenter.gameObject, a_Obj);
			a_Obj.transform.localScale = Vector3.one;

			//	初期化（イベントの設定）
			m_Sensor.Init(p_EventTarget, p_PressEventName, p_ReleaseEventName);
		}
	}

	/// <summary>
	/// センサーがタッチされた時の処理
	/// </summary>
	public void SensorPressDidPush(GameObject p_Target)
	{
		BallManager	a_BallMan = BallManager.instance;

		//	サーブが終了しているなら終了
		if (a_BallMan.isServed)	return;

		//	初期加速度取得
		swingMan.swingData.beginAcc = def.swingSpeed;

		//	ＳＥ：タッチした時の音
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_grip_s);

		//	スプライトを表示する
		TouchSensor sensor = p_Target.GetComponent<TouchSensor>();
		sensor.Press();

		//	サーブ時のアニメーション
		if(!a_BallMan.isServeAnim)
		{
			StartCoroutine(a_BallMan.ServeAnimation_Exec(m_SwingManager, null));
			return;
		}
	}

	/// <summary>
	/// タッチが離された時の処理
	/// </summary>
	public void SensorReleaseDidPush(GameObject p_Target)
	{
		//	スプライトを非表示にする
		TouchSensor	sensor = p_Target.GetComponent<TouchSensor>();
		sensor.Release();
	}

	/// <summary>
	/// 自動スイング処理
	/// </summary>
	/// <param name="p_Res"></param>
	/// <returns></returns>
	public IEnumerator AutoSwing_Exec(SwingManager.stResShotData p_Res)
	{
		//	共通処理
		yield return StartCoroutine(swingMan.Common_Exec(p_Res));
	}
}
