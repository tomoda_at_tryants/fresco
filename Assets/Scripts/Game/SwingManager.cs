﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 端末を振った時等の判定を行う
/// </summary>
public class SwingManager : MonoBehaviour {

	/// <summary>
	/// 管理者
	/// </summary>
	public enum eManager : int
	{
		Computer,
		Player
	}

	/// <summary>
	/// 攻守の状態
	/// </summary>
	public enum eState : int
	{
		Offence,	//	攻撃側
		Deffence		//	防御側
	}

	/// <summary>
	/// 判定に必要なデータ
	/// </summary>
	public class SwingData
	{
		/// <summary>
		/// タッチ開始時の加速度
		/// </summary>
		public Vector3	beginAcc;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public SwingData()
		{
			beginAcc	= Vector3.zero;
		}

		/// <summary>
		/// 初期化
		/// </summary>
		public void	Init()
		{
			beginAcc	= Vector3.zero;
		}
	}

	/// <summary>
	/// ＣＰＵが打った時の結果用データ
	/// </summary>
	public struct stResShotData
	{
		public	ShotData.Param				shot;
		public	Shot_AdjustTable.eAdjust	adjust;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="p_Shot"></param>
		/// <param name="p_Adjust"></param>
		public stResShotData(ShotData.Param p_Shot, Shot_AdjustTable.eAdjust p_Adjust)
		{
			shot	= p_Shot;
			adjust	= p_Adjust;
		}
	}

	SwingData	m_SwingData;
	/// <summary>
	/// スイングデータ
	/// </summary>
	public SwingData	swingData
	{
		get { return m_SwingData; }
		set { m_SwingData = value; }
	}

	/// <summary>
	/// 管理者
	/// </summary>
	public eManager manager;

	/// <summary>
	/// 攻守の状態
	/// </summary>
	public eState	state;

	/// <summary>
	/// ボールが当たった時のコールバック
	/// </summary>
	public System.Action<SwingManager, stResShotData, System.Action, System.Action>	hitCallback;

	/// <summary>
	/// 攻守交替時のコールバック
	/// </summary>
	public System.Action	changeAttackCallback;

	UILabel	m_ResultLabel;

	/// <summary>
	/// 生成
	/// </summary>
	/// <param name="p_EventTarget">イベント関数が存在するオブジェクト名</param>
	/// <param name="p_PressEventName">タッチされた時に呼ばれるイベント名</param>
	/// <param name="p_ReleaseEventName">タッチが終了した時に呼ばれるイベント名</param>
	/// <returns></returns>
	public static SwingManager	Create(eManager p_Manager, eState p_State, System.Action p_ChangeAttackCallBack, System.Action<SwingManager, stResShotData, System.Action, System.Action> p_HitCallback)
	{
		GameObject		a_Obj		= new GameObject("SwingManager" + p_Manager.ToString());
		SwingManager	a_SwingMan	= a_Obj.AddComponent<SwingManager>();

		//	ボールが当たった時のコールバック設定
		if (p_HitCallback != null)	a_SwingMan.hitCallback = p_HitCallback;

		//	攻守交替時のコールバック設定
		if (p_ChangeAttackCallBack != null)	a_SwingMan.changeAttackCallback = p_ChangeAttackCallBack;

		//	管理者設定
		a_SwingMan.manager = p_Manager;

		//	状態設定
		a_SwingMan.state = p_State;

		//	初期化
		a_SwingMan.Init();

		return	a_SwingMan;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void Init()
	{
		if(m_SwingData == null)	m_SwingData = new SwingData();
		m_SwingData.Init();
	}

	/// <summary>
	/// プレイヤーとＣＰＵ共通で行うチェック
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Common_Exec(stResShotData p_Res)
	{
		bool a_bWait = true;
		//	ボールが当たった時のコールバックを呼び出す
		hitCallback(this, p_Res, changeAttackCallback, () =>
			{
				a_bWait = false;
			}
		);

		while (a_bWait) yield return 0;

		//	「ResultLabel」表示
		ResultLabelView(p_Res);

		//	初期化
		Init();
	}

	/// <summary>
	/// 「ResultLabel」を表示する
	/// </summary>
	public void	ResultLabelView(stResShotData p_Res)
	{
		//	今は非表示にしておく
		return;

		string		a_Name		= "baseLabel";
		GameObject	a_Parent	= PanelManager.instance.alignmanMid.alignCenter.gameObject;

		Transform	a_Label		= a_Parent.transform.FindChild(a_Name);
		if(a_Label != null)
		{
			Destroy(a_Label.gameObject);
		}

		//	結果表示
		if (m_ResultLabel == null)
		{
			m_ResultLabel = ResourceManager.PrefabLoadAndInstantiate("Base/baseLabel", Vector3.zero).GetComponent<UILabel>();
			m_ResultLabel.gameObject.name	= a_Name;
			m_ResultLabel.fontSize			= 36;
			m_ResultLabel.fontStyle			= FontStyle.Bold;
			m_ResultLabel.maxLineCount		= 4;	//	４行まで
			//	Widgetのサイズ設定
			GameUtils.WidgetSize_Set(m_ResultLabel.gameObject, new Vector3(640, 150));
			//	親設定
			GameUtils.AttachChild(a_Parent, m_ResultLabel.gameObject);
			//	座標設定
			CSTransform.SetPos_Y(m_ResultLabel.transform, -340);
		}

		m_ResultLabel.text = "";

		//	ラベル更新
		m_ResultLabel.text += string.Format("Begin　　x:{0},y:{1},z:{2}\n", Mathf.FloorToInt(m_SwingData.beginAcc.x), Mathf.FloorToInt(m_SwingData.beginAcc.y), Mathf.FloorToInt(m_SwingData.beginAcc.z));
		m_ResultLabel.text += string.Format("Current　x:{0},y:{1},z:{2}\n", Mathf.FloorToInt(def.swingSpeed.x), Mathf.FloorToInt(def.swingSpeed.y), Mathf.FloorToInt(def.swingSpeed.z));
		m_ResultLabel.text += p_Res.shot.SWINGTYPE_JPN + "\n";
		m_ResultLabel.text += string.Format("{0}	{1}", BallManager.instance.adjust.ToString(), BallManager.instance.ball.transform.localScale);
	}

	/// <summary>
	/// 「ResultLabel」を削除する
	/// </summary>
	public void	ResultLabelDestroy()
	{
		if (m_ResultLabel != null)	Destroy(m_ResultLabel.gameObject);
	}
}
