﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DecisionLabel : MonoBehaviour {

	[SerializeField]	//Inspector上に表示
	public UILabel shotLabel;

	[SerializeField]	//Inspector上に表示
	public UILabel	decisionLabel;

	public bool	isCreated;
	public bool	isPlay;

	static Dictionary<Shot_AdjustTable.eAdjust, Color> c_ColorList = new Dictionary<Shot_AdjustTable.eAdjust, Color>
	{
		//	POOR
		{Shot_AdjustTable.eAdjust.POOR, new Color(154, 0, 208)},

		//	GOOD
		{Shot_AdjustTable.eAdjust.GOOD, Color.blue},

		//	PERFECT
		{Shot_AdjustTable.eAdjust.PERFECT, Color.yellow}
	};

	/// <summary>
	/// 生成
	/// </summary>
	public static DecisionLabel Create(ShotData.Param p_ShotParam, Shot_AdjustTable.eAdjust p_Adjust)
	{
		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate("Game/DecisionLabel", Vector2.zero);
		a_Obj.name = "DecisionLabel";
		GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, a_Obj);

		CSTransform.SetPos_Y(a_Obj.transform, 100);

		DecisionLabel	me	= a_Obj.GetComponent<DecisionLabel>();
		me.isCreated		= true;
		me.isPlay			= false;

		string	a_Lan	= GameInfo.instance.language.ToString();

		//	ラベルを設定
		string	a_Key			= string.Format("{0}_{1}", p_Adjust.ToString(), a_Lan);
		string	a_Text			= Localization.Get(a_Key);
		me.decisionLabel.text	= a_Text;
		me.decisionLabel.color	= c_ColorList[p_Adjust];

		if(p_ShotParam != null)
		{
			string	a_ShotName	= def.instance.shotTable.ShotName_Get(p_ShotParam.ID);
			me.shotLabel.text	= a_ShotName;
		}

		return	me;
	}

	/// <summary>
	/// 生成とアニメーション再生を行う
	/// </summary>
	/// <param name="p_Adjust"></param>
	public static void CreateAndPlay(ShotData.Param p_ShotParam, Shot_AdjustTable.eAdjust p_Adjust)
	{
		DecisionLabel a_Label = Create(p_ShotParam, p_Adjust);
		a_Label.Play();
	}

	public void	Play()
	{
		StartCoroutine(Play_Exec());
	}

	/// <summary>
	/// アニメーションを再生する
	/// </summary>
	/// <returns></returns>
	public IEnumerator	Play_Exec()
	{
		isPlay = true;

		float	a_fAniTime	= 0.7f;
		float	a_fFromY	= transform.localPosition.y;
		iTween.MoveTo(gameObject, iTween.Hash("y", a_fFromY + 150f, "time", a_fAniTime, "islocal", true, "easetype", iTween.EaseType.easeOutCirc));
		yield return new WaitForSeconds(a_fAniTime);

		a_fAniTime = 0.3f;
		StartCoroutine(
			GameUtils.AlphaAnim(decisionLabel.color, a_fAniTime, 1f, 0f, (p_Res) =>
			{
				decisionLabel.color = p_Res;
			}
		));

		StartCoroutine(
			GameUtils.AlphaAnim(shotLabel.color, a_fAniTime, 1f, 0f, (p_Res) =>
			{
				shotLabel.color = p_Res;
			}
		));

		Destroy(gameObject, a_fAniTime);
	}

	// Update is called once per frame
	void Update () {
	}
}
