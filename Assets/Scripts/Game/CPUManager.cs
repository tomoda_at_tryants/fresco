﻿using UnityEngine;
using System.Collections;

public class CPUManager : MonoBehaviour {

	/// <summary>
	/// パラメータ
	/// </summary>
	public  PlayerData.Param	param;

	SwingManager m_SwingManager;
	/// <summary>
	/// スイングマネージャー
	/// </summary>
	public SwingManager	swingMan
	{
		get { return m_SwingManager; }
		set { m_SwingManager = value; }
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static CPUManager Create(int p_nID, System.Action p_ChangeAttackCallBack)
	{
		GameObject	a_Obj	= new GameObject("CPUManager");
		CPUManager	me		= a_Obj.AddComponent<CPUManager>();

		me.Init(p_nID, p_ChangeAttackCallBack);

		return	me;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init(int p_nID, System.Action p_ChangeAttackCallBack)
	{
		//	パラメータ設定
		param = def.instance.playerTable.paramList[p_nID];
		//	スイングマネージャー
		m_SwingManager = SwingManager.Create(SwingManager.eManager.Computer, SwingManager.eState.Offence, p_ChangeAttackCallBack, BallManager.instance.Hit_Exec);
	}

	/// <summary>
	/// ショットの判定を決める
	/// </summary>
	public SwingManager.stResShotData	ShotCheck()
	{
		SwingManager.stResShotData	a_Res = new SwingManager.stResShotData();

		ShotTable	a_Table		= def.instance.shotTable;
		BallManager	a_BallMan	= BallManager.instance;
		//	ショットの種類取得
		//	攻守交替無し
		if(!a_BallMan.isChangeAttack)
		{
			if(swingMan.state == SwingManager.eState.Offence)
			{
				a_Res.shot = a_Table.ShotData_Get("フォアハンド");
			}
			else
			if (swingMan.state == SwingManager.eState.Deffence)
			{
				a_Res.shot = a_Table.ShotData_Get("バックハンド");
			}
		}
		//	攻守交替あり
		else
		{
			//	防御に交代する時はロブ
			if(m_SwingManager.state == SwingManager.eState.Offence)
			{
				a_Res.shot = a_Table.ShotData_Get("ダストショット");
			}
			//	攻撃に交代する時はダスト
			else
			{
				a_Res.shot = a_Table.ShotData_Get("フックショット");
			}
		}

		//	判定取得
		a_Res.adjust = Adjust_Culc();

		return	a_Res;
	}

	/// <summary>
	/// スイング処理
	/// </summary>
	/// <param name="p_Res"></param>
	/// <returns></returns>
	public IEnumerator	Swing_Exec(SwingManager.stResShotData p_Res)
	{
		//	共通処理
		yield return StartCoroutine(swingMan.Common_Exec(p_Res));
		if(BallManager.instance.useType == BallManager.eUseType.Game)	def.instance.SpeedUp_Exec();	//	最高速アップ（スタミナによる）
	}

	/// <summary>
	///	リズムパラメータによる遅延時間を取得する
	/// </summary>
	/// <returns></returns>
	public float	ShotLag_Culc()
	{
		float	a_fLag = 0f;

		int a_nRhythm = (int)param.Rhythm;
		if (a_nRhythm == 10)	return 0;

		//	リズムパラメータ１当たりの最大遅延時間
		float	a_fOnePer = 0.05f;
		//	最大遅延時間
		float	a_fMax = a_fOnePer * (10 - a_nRhythm);

		//	遅延時間はランダムで決める
		a_fLag = UnityEngine.Random.Range(0, a_fMax);
		if (a_fLag < a_fOnePer) a_fLag = 0f;

		return	a_fLag;
	}

	/// <summary>
	/// テクニックパラメータによる判定を取得する
	/// </summary>
	/// <returns></returns>
	public Shot_AdjustTable.eAdjust	Adjust_Culc()
	{
		int a_nTechnique = (int)param.Technique;

		//	タイミング判定を決める
		int a_nRand = Random.Range(1, 10 + 1);
		if (a_nTechnique >= a_nRand)
		{
			return	Shot_AdjustTable.eAdjust.PERFECT;
		}
		else
		{
			a_nRand = Random.Range(0, 1 + 1);
			if (a_nRand == 0)
			{
				return	Shot_AdjustTable.eAdjust.GOOD;
			}
			else
			{
				return	Shot_AdjustTable.eAdjust.POOR;
			}
		}


	}
}
