﻿using UnityEngine;
using System.Collections;

public class TouchSensorRoot : MonoBehaviour{

	public TouchSensor	sensor;

	/// <summary>
	/// 有効設定（trueなら有効）
	/// </summary>
	/// <param name="p_bDisable"></param>
	public void SetEnable(bool p_bEnable)
	{
		sensor.boxCollider.enabled = p_bEnable;
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static TouchSensorRoot	Create(GameObject p_EventTarget = null, string p_PressEventName = "", string p_ReleaseEventName = "")
	{
		GameObject		a_Obj	= ResourceManager.PrefabLoadAndInstantiate("Game/TouchSensorObj", Vector3.zero);
		TouchSensorRoot	me		= a_Obj.GetComponent<TouchSensorRoot>();

		GameUtils.AttachChild(PanelManager.instance.alignmanLow.alignCenter.gameObject, a_Obj);
		a_Obj.transform.localScale = Vector3.one;

		//	初期化（イベントの設定）
		me.Init(p_EventTarget, p_PressEventName, p_ReleaseEventName);

		return	me;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="p_EventTarget"></param>
	/// <param name="p_PressEvent"></param>
	/// <param name="p_ReleaseEvent"></param>
	public void Init(GameObject p_EventTarget, string p_PressEvent, string p_ReleaseEvent)
	{
		sensor.Init(p_EventTarget, p_PressEvent, p_ReleaseEvent);
	}

	/// <summary>
	/// どっちか片方がタッチされている状態か
	/// </summary>
	public bool	isTouching
	{
		get
		{
			return	(sensor.touching) ? true : false;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
