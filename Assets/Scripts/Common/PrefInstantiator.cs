﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefInstantiater {

	/// <summary>
	/// プレハブは位置情報
	/// </summary>
	public struct Layout
	{
		public string				name;
		public string				prefName;
		public bool					fromAssetBundle;
		public int					depth;
		public PanelManager.eLayer	layer;
		public AlignManager.eAlign	align;
		public Vector3				pos;

		private bool				auto;

		/// <summary>
		/// コンストラクタ
		/// （自動的にシーンに配置する場合）
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_prefName"></param>
		/// <param name="_fromAssetBundle"></param>
		/// <param name="_layer"></param>
		/// <param name="_align"></param>
		/// <param name="_pos"></param>
		public Layout(string _name, string _prefName, bool _fromAssetBundle, int _depth, PanelManager.eLayer	_layer, AlignManager.eAlign	_align, Vector3 _pos)
		{
			name		= _name;
			prefName	= _prefName;
			fromAssetBundle	= _fromAssetBundle;
			depth		= _depth;
			layer		= _layer;
			align		= _align;
			pos			= _pos;

			auto		= true;
		}

		/// <summary>
		/// コンストラクタ
		/// （自動的にシーンに配置しない場合）
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_prefName"></param>
		/// <param name="_fromAssetBundle"></param>
		/// <param name="_depth"></param>
		public Layout(string _name, string _prefName, bool _fromAssetBundle, int _depth)
		{
			name		= _name;
			prefName	= _prefName;
			fromAssetBundle	= _fromAssetBundle;
			depth		= _depth;
			layer		= PanelManager.eLayer.Mid;
			align		= AlignManager.eAlign.Center;
			pos			= Vector3.zero;

			auto = false;
		}

	}


	Dictionary<string, GameObject> prefs;

	private PrefInstantiater()
	{
		prefs = new Dictionary<string,GameObject>();
	}

	public static PrefInstantiater Create()
	{
		PrefInstantiater a_This = new PrefInstantiater();
		return a_This;
	}


	/// <summary>
	/// プレハブのインスタンス化
	/// </summary>
	/// <param name="p_Pref"></param>
	public GameObject Build(Layout p_Pref)
	{
		Transform a_Parent = null;

		AlignManager	a_AM = PanelManager.instance.alignman(p_Pref.layer);
		UIWidget		a_WD = a_AM.align(p_Pref.align);
		a_Parent		= a_WD.transform;

		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate(p_Pref.prefName, p_Pref.pos, a_Parent);
		a_Obj.SetActive(false);

		NGUITools.AdjustDepth(a_Obj, p_Pref.depth);

		prefs.Add(p_Pref.name, a_Obj);

		return a_Obj;
	}

	/// <summary>
	/// プレハブのインスタンス化
	/// </summary>
	/// <param name="p_Pref"></param>
	public T Build<T>(Layout p_Pref)
	{
		T a_This = default(T);

		Transform a_Parent = null;

		AlignManager	a_AM = PanelManager.instance.alignman(p_Pref.layer);
		UIWidget		a_WD = a_AM.align(p_Pref.align);
		a_Parent		= a_WD.transform;

		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate(p_Pref.prefName, p_Pref.pos, a_Parent);

		if (a_Obj != null)
		{
			NGUITools.AdjustDepth(a_Obj, p_Pref.depth);

			prefs.Add(p_Pref.name, a_Obj);

			a_This = a_Obj.GetComponent<T>();
		}


		return a_This;
	}

	/// <summary>
	/// 複数のプレハブをインスタンス化
	/// </summary>
	/// <param name="p_Prefs"></param>
	public void Build(Layout[] p_Prefs)
	{
		foreach(Layout a_Pref in p_Prefs)
		{
			Build(a_Pref);
		}
	}

	/// <summary>
	/// プレハブのインスタンス化
	/// </summary>
	/// <param name="p_Pref"></param>
	public GameObject Build(string p_NameKey, Layout p_Pref, Transform p_Parent, Vector3 p_Pos)
	{
		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate(p_Pref.prefName, p_Pos, p_Parent);

		NGUITools.AdjustDepth(a_Obj, p_Pref.depth);

		if (string.IsNullOrEmpty(p_NameKey))
		{
			p_NameKey = p_Pref.name;
		}

		prefs.Add(p_NameKey, a_Obj);

		return a_Obj;
	}

	/// <summary>
	/// プレハブのインスタンス化
	/// </summary>
	/// <param name="p_Pref"></param>
	public T Build<T>(string p_NameKey, Layout p_Pref, Transform p_Parent, Vector3 p_Pos)
	{
		T a_This = default(T);

		GameObject	a_Obj = ResourceManager.PrefabLoadAndInstantiate(p_Pref.prefName, p_Pos, p_Parent);

		if (a_Obj != null)
		{
			NGUITools.AdjustDepth(a_Obj, p_Pref.depth);

			if (string.IsNullOrEmpty(p_NameKey))
			{
				p_NameKey = p_Pref.name;
			}

			prefs.Add(p_NameKey, a_Obj);

			a_This = a_Obj.GetComponent<T>();
		}


		return a_This;
	}

	/// <summary>
	/// インスタンスを取得する
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public GameObject	this[string p_Key]
	{
		get { if (prefs.ContainsKey(p_Key)) return prefs[p_Key]; else return null;}
		set { if (prefs.ContainsKey(p_Key)) prefs[p_Key] = value; }
	}

	/// <summary>
	/// インスタンスのコンポーネントを取得する
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public T Get<T>(string p_Key)
	{
		T a_This = default(T);

		GameObject a_Obj = this[p_Key];
		if (a_Obj != null)
		{
			a_This = a_Obj.GetComponent<T>();
			a_Obj.SetActive(true);
		}

		return a_This;
	}

	/// <summary>
	/// インスタンスを全て破棄する
	/// </summary>
	public void RemoveAll()
	{
		foreach(GameObject a_Obj in prefs.Values)
		{
			GameObject.Destroy(a_Obj);
		}
		prefs.Clear();
		prefs = new Dictionary<string,GameObject>();
	}

}
