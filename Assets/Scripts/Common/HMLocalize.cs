﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode()]	//	※コレが付くとUnityを再生しなくてもスクリプトが実行される
public class HMLocalize : MonoBehaviour {

	/// <summary>
	/// 現在の設定されている言語
	/// </summary>
	GameInfo.eLanguage		m_CurrentLanguage;

	/// <summary>
	/// trueなら自動でスプライトと名前を設定する
	/// </summary>
	public	bool			autoSpriteSet;

	[Space(5)]	//	Inspector上でのスペース

	public	UISprite		targetSprite;
	public	bool			spriteInvalid;
	public	stCommonData[]	spriteNameList	= new stCommonData[(int)GameInfo.eLanguage.MAX];

	[Space(15)]

	public	UILabel			targetLabel;
	public	bool			labelInvalid;
	public	stCommonData[]	labelTextList	= new stCommonData[(int)GameInfo.eLanguage.MAX];

	[System.Serializable]
	public struct stCommonData
	{
		[SerializeField]
		GameInfo.eLanguage	language;

		[SerializeField]
		string				text;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="p_Language"></param>
		/// <param name="p_LabelText"></param>
		public stCommonData(GameInfo.eLanguage p_Language, string p_SetText)
		{
			language	= p_Language;
			text		= p_SetText;
		}

		/// <summary>
		/// 言語を設定する
		/// </summary>
		/// <param name="p_Lan"></param>
		public void	LanguageSet(GameInfo.eLanguage p_Lan)
		{
			language = p_Lan;
		}

		/// <summary>
		/// テキストを取得する
		/// </summary>
		/// <param name="p_Lan"></param>
		public string	TextGet(GameInfo.eLanguage p_Lan)
		{
			return	(language == p_Lan) ? text : "";
		}

		/// <summary>
		/// テキストを設定する
		/// </summary>
		/// <param name="p_Lan"></param>
		public void TextSet(string p_Text)
		{
			text = p_Text;
		}
	}

	/// <summary>
	/// テキストを取得する
	/// </summary>
	/// <param name="p_Lan"></param>
	/// <returns></returns>
	public string SpriteNameGet(GameInfo.eLanguage p_Lan)
	{
		if (p_Lan == GameInfo.eLanguage.NONE || p_Lan == GameInfo.eLanguage.MAX) return "";

		string	a_Text = "";
		for(int i = 0 ; i < spriteNameList.Length ; i++)
		{
			stCommonData	a_Data = spriteNameList[i];
			a_Text = a_Data.TextGet(p_Lan);

			if (!string.IsNullOrEmpty(a_Text))	break;
		}

		return	a_Text;
	}

	/// <summary>
	/// テキストを取得する
	/// </summary>
	/// <param name="p_Lan"></param>
	/// <returns></returns>
	public string LabelTextGet(GameInfo.eLanguage p_Lan)
	{
		if (p_Lan == GameInfo.eLanguage.NONE || p_Lan == GameInfo.eLanguage.MAX) return "";
		stCommonData	a_Data = labelTextList[(int)p_Lan];

		return	a_Data.TextGet(p_Lan);
	}

	// Use this for initialization
	void Start () 
	{
		m_CurrentLanguage = GameInfo.eLanguage.NONE;
		//	表示を更新する（最初の１度だけ強制更新）
		ViewUpdate(true);	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//	表示を更新する
		ViewUpdate();

		if(autoSpriteSet)
		{
			UISprite	a_Sprite = transform.GetComponent<UISprite>();
			if (a_Sprite == null)	return;

			targetSprite = a_Sprite;	//	ターゲットとなるスプライトを設定

			//	スプライト名取得
			string	a_SpName = targetSprite.spriteName;
			if (string.IsNullOrEmpty(a_SpName))	return;

			//	先頭から最後の一文字を抜いた文字列を取得する
			int	a_nLength = a_SpName.Length;
			a_SpName = a_SpName.Substring(0, (a_nLength - 1));

			//	日本語
			spriteNameList[0].LanguageSet(GameInfo.eLanguage.JPN);
			spriteNameList[0].TextSet(a_SpName + "j");

			//	英語
			spriteNameList[1].LanguageSet(GameInfo.eLanguage.ENG);
			spriteNameList[1].TextSet(a_SpName + "e");

			//	ポルトガル語
			spriteNameList[2].LanguageSet(GameInfo.eLanguage.PRT);
			spriteNameList[2].TextSet(a_SpName + "p");
		}
	}

	/// <summary>
	/// 表示を更新する
	/// </summary>
	public void	ViewUpdate(bool p_bForceSet = false)
	{
		if (GameInfo.instance == null)	return;
		//	現在設定されている言語を取得
		GameInfo.eLanguage a_Lan = GameInfo.instance.language;

		//	強制更新の時は無視
		if(!p_bForceSet)
		{
			bool	a_bUpdate = false;
			//	言語が変更されたらインデックスを更新
			if (m_CurrentLanguage != a_Lan)
			{
				m_CurrentLanguage = a_Lan;
				a_bUpdate = true;
			}

			//	言語の切り替えが無い場合は終了
			if (!a_bUpdate) return;
		}
		else
		{
			m_CurrentLanguage = a_Lan;
		}

		//	スプライト名
		if (targetSprite != null && !spriteInvalid)
		{
			string	a_SpriteName = SpriteNameGet(m_CurrentLanguage);
			targetSprite.spriteName = a_SpriteName;
		}

		//	ラベル
		if (targetLabel != null && !labelInvalid)
		{
			string	a_Text = LabelTextGet(m_CurrentLanguage);
			targetLabel.text = a_Text;
		}
	}
}
