﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonAnimation : MonoBehaviour {

	/// <summary>
	/// アニメーションのパワー
	/// </summary>
	public enum ePower
	{
		WEAK,
		NORMAL,
		STRONG
	}

	public ePower	power;

	/// <summary>
	/// パワー毎のアニメーションスケールリスト
	/// </summary>
	Dictionary<ePower, Vector2> m_AnimScaleList = new Dictionary<ePower, Vector2>()
	{
		{ePower.WEAK,	new Vector2(0.2f, 0.2f)},
		{ePower.NORMAL, new Vector2(0.5f, 0.5f)},
		{ePower.STRONG, new Vector2(  1f,   1f)}
	};

	// Use this for initialization
	void Start () 
	{
	}

	float	m_fBusy;

	public bool	isBusy
	{
		get { return (m_fBusy > 0f); }
	}
	
	// Update is called once per frame
	void Update () {
		if(isBusy)
		{
			m_fBusy -= Time.deltaTime;
			if (m_fBusy < 0)	m_fBusy = 0;
		}
	}

	public void	TouchAnimation()
	{
		if(isBusy)	return;

		UIButtonTween a_Tween = GetComponent<UIButtonTween>();
		m_fBusy = a_Tween.delay + 0.1f;

		if(gameObject.GetComponent<iTween>())
		{
			Destroy(gameObject.GetComponent<iTween>());
			transform.localScale = Vector3.one;
		}

		Vector2	a_Scale = m_AnimScaleList[power];
		iTween.PunchScale(gameObject, iTween.Hash("x", a_Scale.x, "y", a_Scale.y, "time", 0.5f, "easetype", iTween.EaseType.easeInOutQuad));
	}
}
