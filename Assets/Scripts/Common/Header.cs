﻿using UnityEngine;
using System.Collections;

public class Header : MonoBehaviour {

	static Header c_Instance;
	public static Header instance
	{
		get
		{
			return	c_Instance;
		}
	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static void	Create()
	{
		if (c_Instance == null)
		{
			GameObject a_Obj = ResourceManager.PrefabLoadAndInstantiate("Game/Header", Vector3.zero);
			c_Instance = a_Obj.GetComponent<Header>();
			GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignTop.gameObject, a_Obj);
		}

		c_Instance.Init();
	}

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",					"Menu/MenuBg",				false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("PausePref",			"Game/PausePref",			false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero)
	};
	PrefInstantiater m_Instantiator;

	public void	Init()
	{
		m_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		m_Instantiator.Build(c_Prefs);

		//	各ラベルの初期化
		RallyTimes_Update(0);
		Speed_Update(0);

		//	試合時間設定
		m_fCurrentTime	= def.c_nGameTime;
		timeLabel.text	= m_fCurrentTime.ToString();
		m_bTimeStop		= true;
	}

	public UILabel	speedLabel;
	public UILabel	timeLabel;
	public UILabel	rallyCountLabel;

	public UIButtonTween	pauseButton;

	public UIWidget		widget;

	PauseManager	m_PauseMan;

	/// <summary>
	/// 現在の時間
	/// </summary>
	float	m_fCurrentTime;
	public float	currentTime
	{
		get { return m_fCurrentTime; }
	}

	bool	m_bTimeStop;
	
	/// <summary>
	/// 時計を動かす
	/// </summary>
	public void	GameStart()
	{
		m_bTimeStop = false;
	}

	/// <summary>
	/// 時計を停止する
	/// </summary>
	public void	GamePause()
	{
		m_bTimeStop = true;
	}

	/// <summary>
	/// 試合終了か
	/// </summary>
	/// <returns></returns>
	public bool	isTimeUp
	{
		get
		{
			return (m_fCurrentTime > 0) ? false : true;
		}
	}

	/// <summary>
	/// アクティブセット
	/// </summary>
	/// <param name="p_bActive"></param>
	/// <returns></returns>
	public void	ActiveSet(bool p_bActive)
	{
		gameObject.SetActive(p_bActive);
		ButtonEnable(p_bActive);
	}

	/// <summary>
	/// ゲーム中はこの表示設定関数を使用する
	/// </summary>
	public void	GameActiveSet(bool p_bActive)
	{
		float	a_fAlpha = p_bActive ? 1 : 0;

		Color	a_SetCol = widget.color;
		a_SetCol.a = a_fAlpha;
		widget.color = a_SetCol;
	}

	int m_nSpeed;

	/// <summary>
	///	表示速度を更新する
	/// </summary>
	/// <param name="p_nSpeed"></param>
	/// <param name="p_bLabelSet"></param>
	public void	Speed_Update(int p_nSpeed, bool p_bLabelSet = true)
	{
		m_nSpeed = p_nSpeed;
		if (p_bLabelSet)	speedLabel.text = m_nSpeed.ToString();
	}

	int		m_nRallyTimes;

	/// <summary>
	/// 表示ラリー回数を更新する
	/// </summary>
	/// <param name="p_nRallyTimes"></param>
	/// <param name="p_bLabelSet"></param>
	public void	RallyTimes_Update(int p_nRallyTimes, bool p_bLabelSet = true)
	{
		StartCoroutine(RallyTimes_Update_Exec(p_nRallyTimes, p_bLabelSet));
	}

	IEnumerator	RallyTimes_Update_Exec(int p_nRallyTimes, bool p_bLabelSet = true)
	{
		//	ラリー回数を増やす
		m_nRallyTimes = p_nRallyTimes;

		Transform	a_Parent = rallyCountLabel.transform.parent;

		//	２コンボ未満は終了
		if (m_nRallyTimes < 3)
		{
			a_Parent.gameObject.SetActive(false);
			rallyCountLabel.text = "";
			yield break;
		}

		a_Parent.gameObject.SetActive(true);

		//	ラベルをセット
		if (p_bLabelSet) rallyCountLabel.text = m_nRallyTimes.ToString();

		iTween.ScaleTo(rallyCountLabel.gameObject, iTween.Hash("x", 1.5f, "y", 1.5f, "time", 0.1f, "easetype", iTween.EaseType.linear));
		iTween.ScaleTo(rallyCountLabel.gameObject, iTween.Hash("x", 1f, "y", 1f, "time", 0.1f, "delay", 0.15f, "easetype", iTween.EaseType.linear));
	}

	/// <summary>
	/// ゲームを停止する処理を行う
	/// </summary>
	public void	PauseButtonDidPush()
	{
		if (Time.timeScale == 0) return;

		Time.timeScale = 0;	//	停止

		//	タッチエフェクトを削除する
		TouchEffect.instance.AllDestroy();

		//	時計を止める
		Header.instance.GamePause();
		//	ポーズ画面に必要なオブジェクトを生成する
		if (m_PauseMan == null)
		{
			m_PauseMan = m_Instantiator.Get<PauseManager>("PausePref");

			//	イベント設定
			m_PauseMan.resumeButton.onClickEventSet(this, "ResumeButtonDidPush", "", 0);
			m_PauseMan.quitButton.onClickEventSet(this, "QuitButtonDidPush", "", 0);
		}
		//	既に生成されている場合は表示
		else
		{
			m_PauseMan.gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// 再開ボタンが押された時に行う処理
	/// </summary>
	public void ResumeButtonDidPush(UIButtonTween p_Sender)
	{
		Time.timeScale = 1;	//	再開

		if (m_PauseMan != null)
		{
			m_PauseMan.gameObject.SetActive(false);
		}

		//	既にサーブを打っている時は時計を動かす
		if (BallManager.instance.isServed)
		{
			Header.instance.GameStart();
		}
	}

	/// <summary>
	/// やめるボタンが押された時に行う処理
	/// </summary>
	public void QuitButtonDidPush(UIButtonTween p_Sender)
	{
		def.instance.QuitScene(false);
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(TimeUpdate());
	}
	
	// Update is called once per frame
	void Update () {
	}

	/// <summary>
	/// ボタン類のコライダーを設定する
	/// </summary>
	/// <param name="p_bEnable"></param>
	public void	ButtonEnable(bool p_bEnable)
	{
		pauseButton.GetComponent<BoxCollider>().enabled = p_bEnable;
	}

	/// <summary>
	/// 残り試合時間を更新する
	/// </summary>
	public IEnumerator	TimeUpdate()
	{
		float	a_fTime = 0f;
		//	時間切れになっていない
		while(!isTimeUp)
		{
			//	時計が止められていない
			if (m_bTimeStop)
			{
				yield return 0;
				continue;
			}

			//	時間更新
			a_fTime += Time.deltaTime;
			if(a_fTime >= 1f)
			{
				m_fCurrentTime -= 1;
				timeLabel.text = m_fCurrentTime.ToString();	//	ラベル更新

				a_fTime = 0;
			}

			yield return 0;
		}
	}
}
