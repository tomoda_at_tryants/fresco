﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerTable : PlayerData {

	public List<PlayerData.Param>	paramList;

	/// <summary>
	/// コンピュータかどうか
	/// </summary>
	public bool	isCOM(PlayerData.Param p_Param)
	{
		if(p_Param.WorldRank == 99)
		{
			return true;
		}

		return false;
	}

	/// <summary>
	/// テーブルを生成する
	/// </summary>
	/// <returns></returns>
	public static PlayerTable Create()
	{
		PlayerTable	a_Table		= ScriptableObject.CreateInstance<PlayerTable>();
		a_Table.paramList		= new List<Param>();
	
		string	a_AssetsName = "PlayerData";

		PlayerData	a_Data = Resources.Load(def.c_XmlPath + a_AssetsName) as PlayerData;
		foreach (Param param in a_Data.param)
		{
			if(param != null)
			{
				a_Table.paramList.Add(param);
			}
		}

		return	a_Table;
	}

	/// <summary>
	/// 名前を取得する
	/// </summary>
	/// <returns></returns>
	public string	Name_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
	{
		if (paramList == null || paramList.Count <= p_nID) return	"Error";

		PlayerData.Param	a_Param		= paramList[p_nID];
		GameInfo.eLanguage	a_Language	= (p_Lan != GameInfo.eLanguage.NONE) ? p_Lan : GameInfo.instance.language;
		switch(a_Language)
		{
			case GameInfo.eLanguage.JPN:
				{
					return a_Param.Name_JPN;
				}

			case GameInfo.eLanguage.ENG:
				{
					return a_Param.Name_ENG;
				}

			case GameInfo.eLanguage.PRT:
				{
					return a_Param.Name_PRT;
				}
		}

		return "";
	}

    /// <summary>
    /// 利き腕を取得する
    /// </summary>
    /// <param name="p_nID"></param>
    /// <param name="p_Lan"></param>
    /// <returns></returns>
    public string   Hand_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
    {
        if (paramList == null || paramList.Count <= p_nID)  return "Error";

        PlayerData.Param    a_Param     = paramList[p_nID];
        GameInfo.eLanguage  a_Language  = (p_Lan != GameInfo.eLanguage.NONE) ? p_Lan : GameInfo.instance.language;
        switch (a_Language)
        {
            case GameInfo.eLanguage.JPN:
                {
                    return a_Param.Hand_JPN;
                }

            case GameInfo.eLanguage.ENG:
                {
                    return a_Param.Hand_ENG;
                }

            case GameInfo.eLanguage.PRT:
                {
                    return a_Param.Hand_PRT;
                }
        }

        return "";
    }

    /// <summary>
    /// 得意ショットを取得する
    /// </summary>
    /// <param name="p_nID"></param>
    /// <param name="p_Lan"></param>
    /// <returns></returns>
    public string BestShot_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
    {
        if (paramList == null || paramList.Count <= p_nID) return "Error";

        PlayerData.Param    a_Param     = paramList[p_nID];
        GameInfo.eLanguage  a_Language  = (p_Lan != GameInfo.eLanguage.NONE) ? p_Lan : GameInfo.instance.language;
        switch (a_Language)
        {
            case GameInfo.eLanguage.JPN:
                {
                    return a_Param.BestShot_JPN;
                }

            case GameInfo.eLanguage.ENG:
                {
                    return a_Param.BestShot_ENG;
                }

            case GameInfo.eLanguage.PRT:
                {
                    return a_Param.BestShot_PRT;
                }
        }

        return "";
    }

    /// <summary>
    /// プロフィールを取得する
    /// </summary>
    /// <param name="p_nID"></param>
    /// <param name="p_Lan"></param>
    /// <returns></returns>
    public string Profile_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
	{
		if (paramList == null || paramList.Count <= p_nID)	return "Error";

		PlayerData.Param	a_Param		= paramList[p_nID];
		GameInfo.eLanguage	a_Language	= (p_Lan != GameInfo.eLanguage.NONE) ? p_Lan : GameInfo.instance.language;

		switch (a_Language)
		{
			case GameInfo.eLanguage.JPN:
				{
					return a_Param.Profile_JPN;
				}

			case GameInfo.eLanguage.ENG:
				{
					return a_Param.Profile_ENG;
				}

			case GameInfo.eLanguage.PRT:
				{
					return a_Param.Profile_PRT;
				}
		}

		return "";
	}

	/// <summary>
	/// 受賞情報を取得する
	/// </summary>
	/// <param name="p_nID"></param>
	/// <param name="p_Lan"></param>
	/// <returns></returns>
	public string Award_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
	{
		if (paramList == null || paramList.Count <= p_nID)	return "Error";

		PlayerData.Param	a_Param		= paramList[p_nID];
		GameInfo.eLanguage	a_Language	= (p_Lan != GameInfo.eLanguage.NONE) ? p_Lan : GameInfo.instance.language;

		switch (a_Language)
		{
			case GameInfo.eLanguage.JPN:
				{
					return a_Param.Award_JPN;
				}

			case GameInfo.eLanguage.ENG:
				{
					return a_Param.Award_ENG;
				}

			case GameInfo.eLanguage.PRT:
				{
					return a_Param.Award_PRT;
				}
		}

		return "";
	}

	/// <summary>
	/// ＩＤからパラメータを返す
	/// </summary>
	/// <param name="p_nID"></param>
	/// <returns></returns>
	public Param	Param_Get(int p_nID)
	{
		foreach(Param a_Param in paramList)
		{
			if(a_Param.ID == p_nID)
			{
				return a_Param;
			}
		}

		return null;
	}

	/// <summary>
	/// 「ＣＯＭ」のＩＤを取得する
	/// </summary>
	/// <returns></returns>
	public int	comID
	{
		get { return	paramList.Count - 2; }
	}

	/// <summary>
	/// アイコンのテクスチャを生成する
	/// </summary>
	/// <param name="p_FileName"></param>
	/// <returns></returns>
	public static UITexture	IconCreate(string p_FileName)
	{
		//	テクスチャをロード
		UITexture	a_Tex = ResourceManager.TextureLoadAndInstantiate("PlayerIcon/" + p_FileName);

		return	a_Tex;
	}

	/// <summary>
	/// 選手のテクスチャを生成する
	/// </summary>
	/// <returns></returns>
	public static UITexture	TexCreate(string p_FileName)
	{
		//	テクスチャをロード
		UITexture	a_Tex = ResourceManager.TextureLoadAndInstantiate("PlayerTex/" + p_FileName);

		return a_Tex;
	}
}
