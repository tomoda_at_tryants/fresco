﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtkChangeTable : AtkChangeData {

	/// <summary>
	/// パラメータリスト
	/// </summary>
	public List<Param>	paramList;

	/// <summary>
	/// キー値によるリスト
	/// </summary>
	public Dictionary<float, Param> keyList;

	/// <summary>
	/// 生成する
	/// </summary>
	/// <returns></returns>
	public static AtkChangeTable	Create()
	{
		AtkChangeTable a_Table	= ScriptableObject.CreateInstance<AtkChangeTable>(); ;
		a_Table.paramList		= new List<AtkChangeData.Param>();
		a_Table.keyList			= new Dictionary<float, Param>();

		string			a_AssetsName	= "AtkChangeData";
		AtkChangeData	a_Data			= Resources.Load(def.c_XmlPath + a_AssetsName) as AtkChangeData;
		foreach (AtkChangeData.Param param in a_Data.param)
		{
			if (param.Key >= 1f)
			{
				a_Table.paramList.Add(param);

				if(a_Table.keyList.Count == 0 || !a_Table.keyList.ContainsKey(param.Key))
				{
					a_Table.keyList.Add(param.Key, param);
				}
			}
		}

		return	a_Table;
	}

	/// <summary>
	/// 攻守交替を要求するまでに必要なラリー回数を取得する
	/// </summary>
	/// <returns></returns>
	public int ChallengeTimes_Get(float p_fAggressive)
	{
		AtkChangeData.Param a_Param = def.instance.atkChangeTable.keyList[p_fAggressive];
		int a_nTimes = UnityEngine.Random.Range(a_Param.NeedShotTimes_Min, a_Param.NeedShotTimes_Max + 1);

		return a_nTimes;
	}
}
