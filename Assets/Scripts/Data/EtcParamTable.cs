﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EtcParamTable : EtcParam {

	/// <summary>
	/// パラメータリスト
	/// </summary>
	public List<EtcParam.Param>					paramList;

	/// <summary>
	/// 名前からパラメータ参照するリスト
	/// </summary>
	public Dictionary<string,EtcParam.Param>	nameList;

	/// <summary>
	/// テーブルを生成する
	/// </summary>
	/// <returns></returns>
	public static EtcParamTable Create()
	{
		EtcParamTable	a_Table = ScriptableObject.CreateInstance<EtcParamTable>(); ;
		a_Table.paramList		= new List<EtcParamTable.Param>();
		a_Table.nameList		= new Dictionary<string,EtcParam.Param>();

		string	a_AssetsName = "EtcParam";

		EtcParam	a_Data = Resources.Load(def.c_XmlPath + a_AssetsName) as EtcParam;
		foreach (EtcParam.Param param in a_Data.param)
		{
			if(!string.IsNullOrEmpty(param.NAME))
			{
				a_Table.paramList.Add(param);
				//	名前から参照するリストに追加
				a_Table.nameList.Add(param.NAME, param);
			}
		}

		return	a_Table;
	}
}
