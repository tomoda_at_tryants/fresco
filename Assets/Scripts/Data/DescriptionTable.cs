﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DescriptionTable : DescriptionData {

	/// <summary>
	/// パラメータリスト
	/// </summary>
	public List<DescriptionTable.Param>					paramList;

	/// <summary>
	/// 言語からパラメータ参照するリスト
	/// </summary>
	public Dictionary<string, DescriptionTable.Param>	nameList;

	/// <summary>
	/// テーブルを生成する
	/// </summary>
	/// <returns></returns>
	public static DescriptionTable  Create()
	{
        DescriptionTable    a_Table = ScriptableObject.CreateInstance<DescriptionTable>(); ;
		a_Table.paramList		= new List<DescriptionTable.Param>();
		a_Table.nameList		= new Dictionary<string, DescriptionTable.Param>();

		string	a_AssetsName = "DescriptionData";

        DescriptionData a_Data = Resources.Load(def.c_XmlPath + a_AssetsName) as DescriptionData;
		foreach (DescriptionTable.Param param in a_Data.param)
		{
			if(!string.IsNullOrEmpty(param.Language))
			{
				a_Table.paramList.Add(param);
				//	名前から参照するリストに追加
				a_Table.nameList.Add(param.Language, param);
			}
		}

		return	a_Table;
	}
}
