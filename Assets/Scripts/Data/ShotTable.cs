﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShotTable : ShotData {

	public List<ShotData.Param>	paramList;

	/// <summary>
	/// テーブルを生成する
	/// </summary>
	/// <returns></returns>
	public static ShotTable	Create()
	{
		ShotTable	a_Table = ScriptableObject.CreateInstance<ShotTable>();
		a_Table.paramList	= new List<ShotData.Param>();

		string	a_AssetsName = "ShotData";

		ShotData	a_Data = Resources.Load(def.c_XmlPath + a_AssetsName) as ShotData;
		foreach (ShotData.Param param in a_Data.param)
		{
			if (!string.IsNullOrEmpty(param.SWINGTYPE_JPN))
			{
				a_Table.paramList.Add(param);
			}
		}

		return	a_Table;
	}

	/// <summary>
	/// フォアショットデータ
	/// </summary>
	public Param	foreShotData{ get { return paramList[0]; } }

	/// <summary>
	/// バックショットデータ
	/// </summary>
	public Param	backShotData{ get { return paramList[1]; } }

	/// <summary>
	/// フックショットデータ
	/// </summary>
	public Param	fookShotData{ get { return paramList[2]; } }

	/// <summary>
	/// ダストショットデータ
	/// </summary>
	public Param	dustShotData{ get { return paramList[3]; } } 

	/// <summary>
	/// 種類と名前からパラメータを取得する
	/// </summary>
	/// <param name="p_SwingType"></param>
	/// <param name="p_ShotName"></param>
	/// <returns></returns>
	public ShotData.Param	ShotData_Get(string p_SwingType)
	{
		foreach(ShotData.Param a_Param in paramList)
		{
			if(a_Param.SWINGTYPE_JPN == p_SwingType)
			{
				return a_Param;
			}
		}
		return	null;
	}

	/// <summary>
	/// 名前を取得する
	/// </summary>
	/// <returns></returns>
	public string ShotName_Get(int p_nID, GameInfo.eLanguage p_Lan = GameInfo.eLanguage.NONE)
	{
		if (paramList == null || paramList.Count <= p_nID) return "Error";

		ShotData.Param      a_Param     = paramList[p_nID];
        GameInfo.eLanguage  a_Language  = (p_Lan == GameInfo.eLanguage.NONE) ? GameInfo.instance.language : p_Lan;
		switch (a_Language)
		{
			case GameInfo.eLanguage.JPN:
				{
					return a_Param.SWINGTYPE_JPN;
				}

			case GameInfo.eLanguage.ENG:
				{
					return a_Param.SWINGTYPE_ENG;
				}

			case GameInfo.eLanguage.PRT:
				{
					return a_Param.SWINGTYPE_PRT;
				}
		}

		return "";
	}

	/// <summary>
	/// 名前を取得する
	/// </summary>
	/// <returns></returns>
	public string Comment_Get(int p_nID)
	{
		if (paramList == null || paramList.Count <= p_nID) return "Error";

		ShotData.Param a_Param = paramList[p_nID];
		GameInfo.eLanguage a_Language = GameInfo.instance.language;
		switch (a_Language)
		{
			case GameInfo.eLanguage.JPN:
				{
					return a_Param.COMMENT_JPN;
				}

			case GameInfo.eLanguage.ENG:
				{
					return a_Param.COMMENT_ENG;
				}

			case GameInfo.eLanguage.PRT:
				{
					return a_Param.COMMENT_PRT;
				}
		}

		return "";
	}

    /// <summary>
	/// 名前を取得する
	/// </summary>
	/// <returns></returns>
	public string Howto_Get(int p_nID)
    {
        if (paramList == null || paramList.Count <= p_nID) return "Error";

        ShotData.Param      a_Param     = paramList[p_nID];
        GameInfo.eLanguage  a_Language  = GameInfo.instance.language;
        switch (a_Language)
        {
            case GameInfo.eLanguage.JPN:
                {
                    return a_Param.Howto_JPN;
                }

            case GameInfo.eLanguage.ENG:
                {
//                    return a_Param.COMMENT_ENG;
                }
                break;

            case GameInfo.eLanguage.PRT:
                {
 //                   return a_Param.COMMENT_PRT;
                }
                break;
        }

        return "";
    }
}
