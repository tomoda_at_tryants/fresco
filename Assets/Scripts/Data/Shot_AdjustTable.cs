﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shot_AdjustTable : Shot_Adjust {

	public enum eAdjust
	{
		NONE,

		POOR,		//	△
		GOOD,		//	○
		PERFECT		//	◎
	}

	/// <summary>
	/// 判定取得リスト
	/// </summary>
	Dictionary<string, eAdjust> adjustList = new Dictionary<string, eAdjust>()
		{
			{"",	eAdjust.NONE},
			{"△",	eAdjust.POOR},
			{"○",	eAdjust.GOOD},
			{"◎",	eAdjust.PERFECT}
		};

	public List<Shot_Adjust.Param>		paramList;

	/// <summary>
	/// テーブルを生成する
	/// </summary>
	/// <returns></returns>
	public static Shot_AdjustTable Create()
	{
		Shot_AdjustTable	a_Table = ScriptableObject.CreateInstance<Shot_AdjustTable>();
		a_Table.paramList			= new List<Shot_Adjust.Param>();

		string	a_AssetsName = "Shot_Adjust";

		Shot_Adjust a_Data = Resources.Load(def.c_XmlPath + a_AssetsName) as Shot_Adjust;
		foreach (Shot_Adjust.Param param in a_Data.param)
		{
			if (!string.IsNullOrEmpty(param.ADJUST))
			{
				a_Table.paramList.Add(param);
			}
		}

		return	a_Table;
	}

	/// <summary>
	/// 判定
	/// </summary>
	/// <returns></returns>
	public eAdjust	Adjust_Get(float p_BallSize)
	{
		foreach(Shot_Adjust.Param param in paramList)
		{
			if(param.MIN <= p_BallSize && param.MAX > p_BallSize)
			{
				return	adjustList[param.ADJUST];
			}
		}

		return	eAdjust.NONE;
	}


}
