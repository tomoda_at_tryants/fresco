﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class scn_swingcheck : GameSceneBase {

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",				"Menu/MenuBg",					false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("SensitivityBar",	"SwingCheck/SensitivityBar",	false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Top,		new Vector3(0,	  -260)),
		new PrefInstantiater.Layout("ExitButton",		"Common/ExitButton",			false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Top,		new Vector3(262,   -60)),
		new PrefInstantiater.Layout("SensorCheckLabel",	"SwingCheck/SensorCheckLabel",	false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Top,		new Vector3(0,     -60)),
	};
	PrefInstantiater m_Instantiator;

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
	{
		None,

		State_Init,
		State_Ready,
		State_Serving,
		State_Game,

		Max
	}

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	eState m_State;
	eState m_NextState;
	string m_Coroutine;

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
		if (p_bContinus)
		{
			State_Check();
		}
	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
			{
				StopCoroutine(m_Coroutine);
				m_Coroutine = null;
			}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

			DebugManager.Log("Call " + m_Coroutine + "()");

			return true;
		}

		return false;
	}

//	メンバ変数
	UISlider		m_SensitivityBar;

	// Use this for initialization
	void Start()
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState	a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}

		m_State		= eState.None;
		m_NextState = eState.State_Init;

		//	「State_Init」を呼び出す
		State_Next(eState.State_Init, true);
	}

	public IEnumerator	State_Init()
	{
		m_bInitialized = false;

		//	ＢＧＭを止める
		SoundHelper.instance.BgmStop();

		//	共通の初期化
		bool	a_bWait = true;
		def.CommonInit(() =>
			{
				a_bWait = false;
			}
		);
		//	初期化が終了するまで待機
		while (a_bWait)	yield return 0;

        //	バナー広告非表示
        def.AdManager.Banner_SetActive(false);

        //	背景の初期化
        BG_Init();

		//	ボール生成
		BallManager.Create(BallManager.eUseType.Check);

		//	ＣＰＵの選手データ設定
		int a_nID = def.instance.playerTable.comID;
		m_CPUManager	= CPUManager.Create(a_nID, ChangeAttack);

		//	プレイヤーデータ生成
		m_PlayerManager = PlayerManager.Create(ChangeAttack);

		m_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		m_Instantiator.Build(c_Prefs);

		//	タイトル表示
		GameObject	a_Title = m_Instantiator.Get<GameObject>("SensorCheckLabel");

		//	センサー調整バー生成
		m_SensitivityBar	= m_Instantiator.Get<UISlider>("SensitivityBar");

		//	イベント設定
		EventDelegate	a_Delegate = EventDelegate.Add(m_SensitivityBar.onChange, OnValueChange_Exec);
		m_SensitivityBar.onChange.Add(a_Delegate);

		//	スライダーの値設定
		float	a_fSensitivityRev = GameInfo.instance.sensitivityRev;
		m_SensitivityBar.value = (a_fSensitivityRev - 1) / 2;

		//	戻るボタンのイベント設定
		UIButtonTween a_ExitButton = m_Instantiator.Get<UIButtonTween>("ExitButton");
		a_ExitButton.onClickEventSet(this, "ExitButtonDidPush", "", 0);

		//	初期化が完了した時の処理
		def.CommonInit_Completed();

		//	初期化を完了にする
		m_bInitialized = true;

		//	「State_Ready」を呼び出す
		State_Next(eState.State_Ready, true);
	}

	public IEnumerator	State_Ready()
	{
		//	ボールの準備が出来るまで待機
		while (!m_BallManager.isReady) yield return 0;

		yield return new WaitForSeconds(0.3f);

		//	「State_Serving」を呼び出す
		State_Next(eState.State_Serving, true);
	}

	/// <summary>
	/// サーブが成功するまで呼ばれる
	/// </summary>
	/// <returns></returns>
	public IEnumerator State_Serving()
	{
		//	結果用データ
		SwingManager.stResShotData a_Res = new SwingManager.stResShotData();

		//	サーブを打つ前はこのwhileをループする
		while (true)
		{
			//	自動
			if (c_bAutoPlay)
			{
				m_BallManager.isServed = true;
				a_Res = new SwingManager.stResShotData(def.instance.shotTable.fookShotData, Shot_AdjustTable.eAdjust.PERFECT);
				yield return StartCoroutine(m_PlayerManager.AutoSwing_Exec(a_Res));

				break;
			}
			else
			{
				//	サーブショット判定
				m_PlayerManager.ShotDecision();
				//	サーブを打ったらループを抜ける
				if (m_BallManager.isServed) break;
			}

			//	サーブアニメーション中は説明画像は表示しない
			if (m_BallManager.isServeAnim)
			{
				//	タッチを無効にする
				m_PlayerManager.sensor.SetEnable(false);
			}
			else
			{
				//	タッチを有効にする
				m_PlayerManager.sensor.SetEnable(true);
			}

			yield return 0;
		}

		//	ゲーム処理に移動
		State_Next(eState.State_Game, true);
	}

	static bool	c_bAutoPlay = false;

	public IEnumerator State_Game()
	{
		//	結果用データ
		SwingManager.stResShotData a_Res = new SwingManager.stResShotData();

		//	ラリー中はループ
		while (true)
		{
			//	ボールを打てるタイミングになるまで待機
			while (!m_BallManager.isShotTiming)
			{
				yield return 0;
			}

			//	ＣＰＵ
			if (m_BallManager.isReceiveCPU)
			{
				//	遅延時間取得
				float	a_fLag = m_CPUManager.ShotLag_Culc();
				if (a_fLag > 0f)	yield return new WaitForSeconds(a_fLag);

				//	ショットデータ取得
				a_Res = m_CPUManager.ShotCheck();
				//	到達したら打ち返す
				yield return	StartCoroutine(m_CPUManager.Swing_Exec(a_Res));

				continue;
			}
			else
			//	プレイヤー
			if (m_BallManager.isReceivePlayer)
			{
				//	振り始めデータを取得していない
				if (!m_PlayerManager.isGetBeginSwingData)
				{
					m_PlayerManager.BeginSwingData_Get();
				}
				else
				{
					//	手動プレイの時はショット判定を行う
					if (!c_bAutoPlay)	m_PlayerManager.ShotDecision();
				}

				//	自動
				if (c_bAutoPlay)
				{
					Shot_AdjustTable.eAdjust	a_Adjust = Shot_AdjustTable.eAdjust.NONE;

					//	ボールが「Perfect」判定になるまで待機
					while (true)
					{
						a_Adjust = def.instance.adjustTable.Adjust_Get(m_BallManager.ball.transform.localScale.x);
						if (a_Adjust == Shot_AdjustTable.eAdjust.PERFECT) break;

						yield return 0;
					}

					//	攻守交替時等によってショットの種類を変える
					ShotData.Param a_Param = def.instance.shotTable.foreShotData;
					if (BallManager.instance.isChangeAttack)
					{
						if (m_PlayerManager.swingMan.state == SwingManager.eState.Deffence)
						{
							//	ダストショット
							a_Param = def.instance.shotTable.fookShotData;
						}
						else
						if (m_PlayerManager.swingMan.state == SwingManager.eState.Offence)
						{
							//	ロブショット
							a_Param = def.instance.shotTable.dustShotData;
						}
					}
					else
					{
						//	フォアハンド
						a_Param = def.instance.shotTable.foreShotData;
					}

					a_Adjust	= (Shot_AdjustTable.eAdjust)UnityEngine.Random.Range(1, 3 + 1);
					a_Res		= new SwingManager.stResShotData(a_Param, a_Adjust);

					//	到達したら打ち返す
					yield return	StartCoroutine(m_PlayerManager.AutoSwing_Exec(a_Res));

					continue;
				}
				//	手動
				else
				{
					//	猶予期間が過ぎたらミス
					if (m_BallManager.isMiss)
					{
						//	時計を止める
						Header.instance.GamePause();
						State_Next(eState.State_Ready, true);
					}
				}
			}

			yield return 0;
		}
	}

	/// <summary>
	/// 感度補正を更新する
	/// </summary>
	/// <param name="p_fScaleValue"></param>
	public void OnValueChange_Exec()
	{
		GameInfo.instance.sensitivityRev = m_SensitivityBar.value;
		//	効果音
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_input);
	}

	/// <summary>
	/// 戻るボタンが押された時の処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	ExitButtonDidPush(UIButtonTween p_Sender)
	{
		StartCoroutine(def.instance.SceneMove(def.Scene.EDITOR, true));
	}
}
