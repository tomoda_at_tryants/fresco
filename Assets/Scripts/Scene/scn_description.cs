﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class scn_description : MonoBehaviour
{

    /// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
    {
        None,

        State_Init,

        Max
    }

    /// <summary>
    ///	ステート名
    /// </summary>
    static Dictionary<eState, string> c_StateList;

    eState m_State;
    eState m_NextState;
    string m_Coroutine;

    /// <summary>
    /// 配置するプレハブの情報
    /// </summary>
    static PrefInstantiater.Layout[] c_Prefs =
    {
//	    new PrefInstantiater.Layout("Bg",					"Menu/MenuBg",				        false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0,      0, 0)),
        new PrefInstantiater.Layout("DescriptionPanel",      "Description/DescriptionPanel",    false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(0,    320, 0)),
        new PrefInstantiater.Layout("ExitButton",           "Common/ExitButton",                false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(260,  510, 0))
    };
    PrefInstantiater    m_Instantiator;

    /// <summary>
    /// 次に呼び出すステートを指定する
    /// </summary>
    /// <param name="p_NextState"></param>
    /// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
    void State_Next(eState p_NextState, bool p_bContinus = false)
    {
        m_NextState = p_NextState;
        if (p_bContinus)
        {
            State_Check();
        }
    }

    /// <summary>
    /// ステートに変化が無いかチェックし、変化があったらステートを切り替える
    /// </summary>
    /// <returns></returns>
    bool State_Check()
    {
        if (m_NextState != m_State)
        {
            m_State = m_NextState;
            if (!string.IsNullOrEmpty(m_Coroutine))
            {
                StopCoroutine(m_Coroutine);
                m_Coroutine = null;
            }

            m_Coroutine = c_StateList[m_State];
            StartCoroutine(m_Coroutine);

            DebugManager.Log("Call " + m_Coroutine + "()");

            return true;
        }

        return false;
    }

    // Use this for initialization
    void Start()
    {
        //	ステートのリストを追加
        c_StateList = new Dictionary<eState, string>();
        for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
        {
            eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
            c_StateList.Add(a_State, a_State.ToString());
        }

        m_State = eState.None;
        m_NextState = eState.State_Init;

        //	「State_Boot」を呼び出す
        State_Next(eState.State_Init, true);
    }

    public IEnumerator State_Init()
    {
        //	共通の初期化
        bool a_bWait = true;
        def.CommonInit(() =>
            {
                a_bWait = false;
            }
        );
        //	初期化が終了するまで待機
        while (a_bWait) yield return 0;

        //	使用するプレハブをインスタンス化
        m_Instantiator = PrefInstantiater.Create();
        m_Instantiator.Build(c_Prefs);

        //  背景の読み込み
        def.BGCreate("Bg/" + "bg_1", "Bg_description", PanelManager.instance.alignmanLow.alignCenter.gameObject);

        //  戻るボタン
        ButtonCreate("ExitButton", "ExitButtonDidPush");

        UIPanel    a_View = m_Instantiator.Get<UIPanel>("DescriptionPanel");
        CSTransform.SetPos_Y(a_View.transform, -a_View.clipOffset.y);

        yield return new WaitForSeconds(0.3f);

        //	初期化が完了した時の処理
        def.CommonInit_Completed();

        //	フェードイン
        PanelManager.instance.fade.In(0, Color.white);
    }

    /// <summary>
	/// ボタンを生成する
	/// </summary>
	/// <param name="p_ButtonName"></param>
	/// <param name="p_Event"></param>
	public void ButtonCreate(string p_ButtonName, string p_Event)
    {
        //	ボタン生成
        UIButtonTween a_Button = m_Instantiator.Get<UIButtonTween>(p_ButtonName);
        //	イベント設定
        a_Button.onClickEventSet(this, p_Event, "", 0);
    }

    public void ExitButtonDidPush(UIButtonTween p_Sender)
    {
        StartCoroutine(def.instance.SceneMove(def.Scene.HOWTO, true));
    }
}