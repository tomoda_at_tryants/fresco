﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class scn_training : GameSceneBase {

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",				"Menu/MenuBg",					false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("ExitButton",		"Common/ExitButton",			false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Top,		new Vector3( 262,  -60)),
        new PrefInstantiater.Layout("ShotHowtoLabel",   "Training/ShotHowtoLabel",      false,  1,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(-300,  420)),
        new PrefInstantiater.Layout("ShotExplainLabel", "Training/ShotExplainLabel",    false,  1,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(-260, -360)),
        new PrefInstantiater.Layout("ShotImage",        "Training/ShotImage",           false,  1,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(-160, -200)),
    };

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
	{
		None,

		State_Init,
		State_Ready,
		State_Serving,
		State_Game,

		Max
	}

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	eState	m_State;
	eState	m_NextState;
	string	m_Coroutine;

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
		if (p_bContinus)
		{
			State_Check();
		}
	}

	/// <summary>
	/// ショットＩＤ
	/// </summary>
	public static int	c_nShotID;

	/// <summary>
	/// トレーニング中か
	/// </summary>
	public static bool	c_bTraining;

	/// <summary>
	/// 現在トレーニング中のショットと実際に振られたショットが一致するか
	/// </summary>
	/// <returns></returns>
	public static bool	isConsistentShot(int p_nShotID)
	{
		return	(c_nShotID == p_nShotID) ? true : false;
	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
			{
				StopCoroutine(m_Coroutine);
				m_Coroutine = null;
			}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

			DebugManager.Log("Call " + m_Coroutine + "()");

			return true;
		}

		return false;
	}

	// Use this for initialization
	void Start()
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}

		m_State		= eState.None;
		m_NextState = eState.State_Init;

		//	「State_Init」を呼び出す
		State_Next(eState.State_Init, true);
	}

	public IEnumerator State_Init()
	{
		m_bInitialized	= false;                                                                        
		c_bTraining		= true;

        //  ゲームシーンの共通初期化
        yield return StartCoroutine(GameScene_Init(BallManager.eUseType.Check, def.instance.playerTable.comID, c_Prefs));

		//	戻るボタンのイベント設定
		UIButtonTween a_ExitButton = m_Instantiator.Get<UIButtonTween>("ExitButton");
		a_ExitButton.onClickEventSet(this, "ExitButtonDidPush", "", 0);

		//	初期化を完了にする
		m_bInitialized = true;

		//	「State_Ready」を呼び出す
		State_Next(eState.State_Ready, true);
	}

	public IEnumerator State_Ready()
	{
		//	ボールの準備が出来るまで待機
		while (!m_BallManager.isReady) yield return 0;

		//	「State_Serving」を呼び出す
		State_Next(eState.State_Serving, true);
	}

	/// <summary>
	/// サーブが成功するまで呼ばれる
	/// </summary>
	/// <returns></returns>
	public IEnumerator State_Serving()
	{
        //	ショット説明用ラベル表示
        UILabel     a_ShotHowto     = m_Instantiator.Get<UILabel>("ShotHowtoLabel");
        //	ショット説明用ラベル表示
        UILabel		a_ShotExplain	= m_Instantiator.Get<UILabel>("ShotExplainLabel");
        //  ショットイメージ画像
        UISprite    a_ShotImage     = m_Instantiator.Get<UISprite>("ShotImage");
		//	ショット名
		string		a_ShotName		= def.instance.shotTable.ShotName_Get(c_nShotID);
		//	ショット説明文
		string		a_Text			= def.instance.shotTable.Howto_Get(c_nShotID);

        //	画面上に表示する文言設定
        a_ShotHowto.text = a_Text;

        //	画面下に表示する文言設定
        a_Text = def.instance.shotTable.Comment_Get(c_nShotID);
        a_ShotExplain.text = string.Format("{0}\n{1}", a_ShotName, a_Text);

        //  ショット名
        a_ShotName = def.instance.shotTable.ShotName_Get(c_nShotID, GameInfo.eLanguage.ENG);
        a_ShotImage.spriteName = string.Format("img_{0}", a_ShotName);

		//	結果用データ
		SwingManager.stResShotData	a_Res = new SwingManager.stResShotData();

		//	サーブを打つ前はこのwhileをループする
		while (true)
		{
			//	サーブショット判定
			m_PlayerManager.ShotDecision();
			//	サーブを打ったらループを抜ける
			if (m_BallManager.isServed) break;

			//	サーブアニメーション中は説明画像は表示しない
			if (m_BallManager.isServeAnim)
			{
                //	説明用画像を非表示にする
                a_ShotHowto.enabled     = false;
                a_ShotExplain.enabled   = false;
                a_ShotImage.enabled     = false;

				//	タッチを無効にする
				m_PlayerManager.sensor.SetEnable(false);
			}
			else
			{
                //	説明用画像を表示する
                a_ShotHowto.enabled     = true;
                a_ShotExplain.enabled   = true;
                a_ShotImage.enabled     = true;

				//	タッチを有効にする
				m_PlayerManager.sensor.SetEnable(true);
			}

			yield return 0;
		}

        a_ShotHowto.enabled     = false;
        a_ShotExplain.enabled	= false;
        a_ShotImage.enabled     = false;

        //	ＣＰＵ
        if (m_BallManager.isReceiveCPU)
		{
			yield return new WaitForSeconds(2f);

			m_BallManager.Restart();
			m_PlayerManager.sensor.SetEnable(true);
			State_Next(eState.State_Ready, true);
		}
	}

	/// <summary>
	/// 戻るボタンが押された時の処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void ExitButtonDidPush(UIButtonTween p_Sender)
	{
		c_bTraining = false;
		StartCoroutine(def.instance.SceneMove(def.Scene.HOWTO, true));
	}

	// Update is called once per frame
	void Update () {
	
	}
}
