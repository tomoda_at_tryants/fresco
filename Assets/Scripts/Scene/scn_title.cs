﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;

public class scn_title : MonoBehaviour {

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
	{
		None,

		State_Boot,
		State_Init,
		State_Title,

		Max
	}

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	eState	m_State;
	eState	m_NextState;
	string	m_Coroutine;

	/// <summary>
	/// 起動したか
	/// </summary>
	static bool	c_bBoot;

    /// <summary>
    /// 注意ボタンをタッチしたか
    /// </summary>
    bool m_bNotice;

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
		if (p_bContinus)
		{
			State_Check();
		}
	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
			{
				StopCoroutine(m_Coroutine);
				m_Coroutine = null;
			}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

			DebugManager.Log("Call " + m_Coroutine + "()");

			return true;
		}

		return false;
	}

	List<UIButtonTween> m_ButtonList;

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",				"Menu/MenuBg",				false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("BootLogo",			"Title/BootLogo",			false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		Vector3.zero),
		new PrefInstantiater.Layout("FreeButton",		"Title/FreeButton",			false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3( 245,  120, 0)),
		new PrefInstantiater.Layout("TournamentButton",	"Title/TournamentButton",	false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3( 245,  -50, 0)),
		new PrefInstantiater.Layout("EditorButton",		"Title/EditorButton",		false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3( 245, -220, 0)),
		new PrefInstantiater.Layout("HowToButton",		"Title/HowToButton",		false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3( 245, -390, 0)),
		new PrefInstantiater.Layout("LinkButton",		"Title/LinkButton",			false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(-245, -390, 0)),
		new PrefInstantiater.Layout("ModeSelect",		"Title/ModeSelect",			false,	0,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
        new PrefInstantiater.Layout("NoticeButton",     "Title/NoticeButton",       false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero),
        new PrefInstantiater.Layout("Link_jfbaButton",  "Title/Link_jfbaButton",    false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(  80, -410, 0)),
        new PrefInstantiater.Layout("LanguageSelect",   "Common/LanguageSelect",    false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Center,     Vector3.zero),
//		new PrefInstantiater.Layout("PlayerListButton",	"Title/PlayerListButton",	false,	0,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(225, -250, 0)),
	};
	PrefInstantiater m_Instantiator;

	// Use this for initialization
	void Start () 
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}

		m_State		= eState.None;
		m_NextState = eState.State_Boot;

		//	「State_Boot」を呼び出す
		State_Next(eState.State_Boot, true);
	}

	/// <summary>
	/// ロゴ表示等のタイトル画面が表示される前のステート
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Boot()
	{
		//	共通の初期化
		bool	a_bWait = true;
		def.CommonInit(() =>
			{
				a_bWait = false;
			}
		);
		//	初期化が終了するまで待機
		while (a_bWait) yield return 0;

		//	使用するプレハブをインスタンス化
		m_Instantiator = PrefInstantiater.Create();
		m_Instantiator.Build(c_Prefs);

		//	起動回数を増やす
		GameInfo.instance.BootCount_Add();

        //  初回起動時の言語設定
        //	初回起動時は言語設定を行う
        if (GameInfo.instance.isFirstBoot)
		{
            //  デフォルトの感度
            GameInfo.instance.sensitivityRev = 0.5f;

#if false
            LanguageSelect a_View = m_Instantiator.Get<LanguageSelect>("LanguageSelect");
			//	戻るボタンを非表示にする
			a_View.BackButton.gameObject.SetActive(false);
			//	表示開始時のアニメーション
			yield return StartCoroutine(a_View.StartAnim());

			//	言語設定ダイアログが開いている間はループ
			while (a_View.isOpen)	yield return 0;

			yield return new WaitForSeconds(0.3f);
#endif
        }

        if (!c_bBoot)
		{
			c_bBoot = true;

            

            GameInfo.instance.Language_Set(GameInfo.eLanguage.JPN);
            //  初回起動時の言語設定
#if false
            //	起動時に一度だけ言語設定
            def.Language_Set();
#endif

			//	起動時のロゴ表示
			Animator	a_BootLogo = m_Instantiator.Get<Animator>("BootLogo");
			float		a_fAniTime = GameUtils.AnimationTime_Get(a_BootLogo);

			yield return new WaitForSeconds(a_fAniTime);

            Destroy(a_BootLogo.gameObject);

            //	注意文画像の読み込み
            UIButtonTween   a_Notice    = m_Instantiator.Get<UIButtonTween>("NoticeButton");
            UITexture       a_NoticeTex = a_Notice.GetComponent<UITexture>();
            a_NoticeTex.mainTexture     = ResourceManager.TextureLoad("Bg/" + "bg_notice", false);
            Vector2         a_TexSize   = new Vector2(def.c_ScreenSize.x, def.c_ScreenSize.y + 2);
            GameUtils.WidgetSize_Set(a_Notice.gameObject, a_TexSize);
            GameUtils.AttachChild(PanelManager.instance.alignmanMid.alignCenter.gameObject, a_Notice.gameObject);

            a_bWait = true;
            //	イベント設定
            a_Notice.onClickSubEventSet(() => { a_bWait = false; a_Notice.forceDisable(); });

            //  注意文をタッチするまで待機
            while(a_bWait)  yield return 0;

            yield return new WaitForSeconds(0.5f);

            //	フェードアウト
            float   a_fFadeTime = 0.5f;
			PanelManager.instance.fade.Out(a_fFadeTime, Color.white, true);

			//	フェードが終わるまで待機
			yield return new WaitForSeconds(a_fFadeTime + 0.3f);

            Destroy(a_Notice.gameObject);
        }

		//	初期化が完了した時の処理
		def.CommonInit_Completed();

        //	バナー広告関連
        if (def.AdManager.bannerView == null)
		{
			def.AdManager.RequestBanner();
		}
		else
		{
            def.AdManager.Banner_SetActive(true);
        }

//        PlayerInfo.instance.userRank = def.c_nPlayerRankMin;

		State_Next(eState.State_Init, true);
	}

    public void NoticeButtonDidPush(UIButtonTween p_Sender)
    {
        m_bNotice = true;
    }

	/// <summary>
	/// ボタンを生成する
	/// </summary>
	/// <param name="p_ButtonName"></param>
	/// <param name="p_Event"></param>
	public void	ButtonCreate(string p_ButtonName, string p_Event)
	{
        if (p_ButtonName == "NoticeButton") return;

		//	ボタン生成
		UIButtonTween	a_Button = m_Instantiator.Get<UIButtonTween>(p_ButtonName);
		//	イベント設定
		a_Button.onClickEventSet(this, p_Event, "", 0);

		m_ButtonList.Add(a_Button);
	}

	/// <summary>
	/// 初期化を行う
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Init()
	{
        m_ButtonList = new List<UIButtonTween>();

		//	ボタン生成
		foreach(PrefInstantiater.Layout a_Layout in c_Prefs)
		{
			string	a_PrefName = a_Layout.name;
			if(a_PrefName.Contains("Button"))
			{
				string	a_Event = a_PrefName + "DidPush";
				ButtonCreate(a_PrefName, a_Event);
			}
		}

		//	背景の読み込み
        def.BGCreate("Bg/" + "bg_title", "Bg_game", PanelManager.instance.alignmanLow.alignCenter.gameObject);

		yield return new WaitForSeconds(0.3f);

		//	フェードイン
		PanelManager.instance.fade.In(0, Color.white);

		//	ＢＧＭ
		SoundHelper.instance.BgmPlay(SoundHelper.eNo.bgm_title);
		//	音量設定
		SoundHelper.instance.BgmVolumeSet(def.c_fMaxVolume);

		yield return 0;
	}

	/// <summary>
	/// リンク先にジャンプを行う
	/// </summary>
	/// <param name="p_Sender"></param>
	public	void	LinkButtonDidPush(UIButtonTween p_Sender)
	{
		//	プラットフォームを取得
		RuntimePlatform a_Platform = Application.platform;

		//	Android
		if (a_Platform == RuntimePlatform.Android)
		{
			Application.OpenURL("https://play.google.com/store/apps/developer?id=Happymeal+Inc.");
		}
		else
		//	iOS
		if (a_Platform == RuntimePlatform.IPhonePlayer)
		{
			Application.OpenURL("http://itunes.apple.com/WebObjects/MZSearch.woa/wa/search?entity=software&media=software&submit=seeAllLockups&term=tiddlygame");
		}
	}

    /// <summary>
	/// リンク先にジャンプを行う
	/// </summary>
	/// <param name="p_Sender"></param>
	public void Link_jfbaButtonDidPush(UIButtonTween p_Sender)
    {
        //	プラットフォームを取得
        RuntimePlatform a_Platform = Application.platform;

        Application.OpenURL("http://www.frescoball.org/");
    }

	/// <summary>
	/// 選手一覧を閉じた時に呼ばれるコールバック
	/// </summary>
	public void	PlayerList_Close()
	{
		foreach (UIButtonTween a_Button in m_ButtonList)
		{
			a_Button.GetComponent<BoxCollider>().enabled = true;
		}

		//	音量設定
		SoundHelper.instance.BgmVolumeSet(def.c_fMaxVolume);
	}

	/// <summary>
	/// トーナメントモードを開始する
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	TournamentButtonDidPush(UIButtonTween p_Sender)
	{
		GameInfo.instance.GameMode_Set(GameInfo.eGameMode.TOURNAMENT);	//	モード設定
		StartCoroutine(def.instance.SceneMove(def.Scene.SELECT, true));
	}

	/// <summary>
	/// フリーモードを開始する
	/// </summary>
	/// <param name="p_Sender"></param>
	public void FreeButtonDidPush(UIButtonTween p_Sender)
	{
		GameInfo.instance.GameMode_Set(GameInfo.eGameMode.FREE);	//	モード設定
		StartCoroutine(def.instance.SceneMove(def.Scene.GAME, true));
	}

	/// <summary>
	/// ラケットエディターに遷移する
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	EditorButtonDidPush(UIButtonTween p_Sender)
	{
		StartCoroutine(def.instance.SceneMove(def.Scene.EDITOR, true));
	}

	/// <summary>
	/// フレスコボールについての説明シーンに遷移する
	/// </summary>
	/// <param name="p_Sender"></param>
	public void HowToButtonDidPush(UIButtonTween p_Sender)
	{
		StartCoroutine(def.instance.SceneMove(def.Scene.HOWTO, true));
	}
}
