﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scn_select : MonoBehaviour {

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",					"Menu/MenuBg",					false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("SelectProfileList",	"Select/SelectProfileList",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(   0,	 20)),
		new PrefInstantiater.Layout("ExitButton",			"Select/ExitButton",			false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Top,		new Vector3( 272,	-52)),
		new PrefInstantiater.Layout("SelectScene_Title",	"Select/SelectScene_Title",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Top,		new Vector3(-310,	-65)),
		new PrefInstantiater.Layout("ChallengeButton",		"Select/ChallengeButton",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Bottom,		new Vector3(   0,   160)),
	};
	PrefInstantiater m_Instantiator;

	/// <summary>
	/// 初期化フラグ
	/// </summary>
	bool m_bInitialize;

	SelectProfileList	m_ProfileList;
	SelectProfileList	m_PartnerList;

	// Use this for initialization
	IEnumerator Start () {
		m_bInitialize = false;

		//	GUI用の解像度を更新
		GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / def.c_ScreenSize.x, Screen.height / def.c_ScreenSize.y), Vector2.zero);

		//	音量設定（タイトル画面の半分）
		SoundHelper.instance.BgmVolumeSet(def.c_fMaxVolume / 3f);

		//	共通の初期化
		bool a_bWait = true;
		def.CommonInit(() =>
			{
				a_bWait = false;
			}
		);
		//	初期化が終了するまで待機
		while (a_bWait) yield return 0;

        //	背景の読み込み
        def.BGCreate("Bg/" + "bg_challenge", "bg_challenge", PanelManager.instance.alignmanLow.alignCenter.gameObject);

        //	プレイヤーのランクが１位の時は２位からスタートする
        int a_nPlayerRank = PlayerInfo.instance.userRank;
		if (a_nPlayerRank == 1)	PlayerInfo.instance.userRank = 2;

		m_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		m_Instantiator.Build(c_Prefs);

		//	背景リスト生成
		m_ProfileList = m_Instantiator.Get<SelectProfileList>("SelectProfileList");
		m_ProfileList.Init(SelectProfileList.eViewType.Game, false, ChangePartnerOpen, null);

		//	ボタンの配置
		//	戻るボタン
		UIButtonTween	a_BackButton = m_Instantiator.Get<UIButtonTween>("ExitButton");
		//	イベント設定
		a_BackButton.onClickEventSet(this, "BackToTitleButtonDidPush", "", 0);

		//	挑戦ボタン
		UIButtonTween	a_ChallengeButton = m_Instantiator.Get<UIButtonTween>("ChallengeButton");
		//	イベント設定
		a_ChallengeButton.onClickEventSet(this, "ChallengeButtonDidPUsh", "", 0);

		//	ボタンの文言設定
		GameInfo.eGameMode	a_Mode = GameInfo.instance.gameMode;
		GameInfo.eLanguage	a_Lang = GameInfo.instance.language;
		string  a_Key	= string.Format("GAMESTART_{0}_{1}", a_Mode, a_Lang);
		string  a_Text	= Localization.Get(a_Key);

		//	初期化が完了した時の処理
		def.CommonInit_Completed();

		//	結果
		GameInfo.stBattleResult a_Res = GameInfo.instance.resultData;
		//	結果表示
		if (a_Res.enableResult && def.instance.pastScene == defSystem.Scene.GAME)
		{
			StartCoroutine(Result_Exec());
		}
	}

	/// <summary>
	/// パートナーの変更を行うダイアログを開く時に行う処理
	/// </summary>
	public void	ChangePartnerOpen()
	{
		//	プロフィールリストは非表示にする
		m_ProfileList.gameObject.SetActive(false);

        //  挑戦ボタンを非表示にする
        UIButtonTween   a_Button =  m_Instantiator.Get<UIButtonTween>("ChallengeButton");
        a_Button.gameObject.SetActive(false);

        //	パートナー変更用ダイアログ生成
        m_PartnerList = ResourceManager.PrefabLoadAndInstantiate("Select/SelectProfileList", Vector3.zero).GetComponent<SelectProfileList>();
		GameUtils.AttachChild(PanelManager.instance.alignmanMid.alignCenter.gameObject, m_PartnerList.gameObject);
		CSTransform.SetPos_Y(m_PartnerList.transform, 20);
		//	初期化
		m_PartnerList.Init(SelectProfileList.eViewType.Partner, true, null, ChangePartnerSet);
		//	閉じる時に呼ばれるコールバックの設定
		m_PartnerList.CloseCallBack_Set(ChangePartnerClose);
	}

	/// <summary>
	/// パートナーを設定した時の処理
	/// </summary>
	public void	ChangePartnerSet(SelectProfileDraw p_ProfileDraw)
	{
        int a_nIndex = 1;

		//	パートナー選手を記録
		GameInfo.instance.PartnerProfile_Set(p_ProfileDraw.info);
		//	情報更新
		m_ProfileList.userDialog.profileDrawList[a_nIndex].Init(a_nIndex, p_ProfileDraw.info);
		m_ProfileList.userDialog.profileList[1] = p_ProfileDraw.info;

		//	結果表示用に選手とプレイヤー情報を登録
		GameInfo.instance.resultData.UserProfile_Set(m_ProfileList.userDialog.profileList);

		//	パートナー関連の更新
		m_PartnerList.PartnerUpdate();
	}

	/// <summary>
	/// パートナーの変更を行うダイアログを閉じる時に行う処理
	/// </summary>
	public void	ChangePartnerClose()
	{
		//	プロフィールリストを表示する
		m_ProfileList.gameObject.SetActive(true);

        //  挑戦ボタンを表示する
        UIButtonTween a_Button = m_Instantiator.Get<UIButtonTween>("ChallengeButton");
        a_Button.gameObject.SetActive(true);
    }

	/// <summary>
	/// 「挑戦」ボタンが押された時の処理
	/// </summary>
	public void	ChallengeButtonDidPUsh(UIButtonTween p_Sender)
	{
		SelectProfileDialog	a_PlayerDialog = m_ProfileList.PlayerDialog_Get(PlayerInfo.instance.userRank - 2);
		StartCoroutine(GameStart_Exec(a_PlayerDialog));
	}

	/// <summary>
	/// ゲームスタート時の処理（トーナメントモードで「挑戦」を押した時の処理）
	/// </summary>
	/// <returns></returns>
	public IEnumerator	GameStart_Exec(SelectProfileDialog p_PlayerDialog)
	{
		//	ユーザーダイアログ
		SelectProfileDialog	a_UserDialog = m_ProfileList.userDialog;

		//	結果表示用に選手とプレイヤー情報を登録
		GameInfo.instance.resultData.Profile_Set(p_PlayerDialog.profileList, a_UserDialog.profileList);

		//	対戦開始時のアニメーション再生
		yield return StartCoroutine(VSAnim_Exec("intro", p_PlayerDialog, a_UserDialog, false, false));

		StartCoroutine(def.instance.SceneMove(def.Scene.GAME, true));

		yield return 0;
	}

    /// <summary>
    /// トーナメントで勝利した時の処理
    /// </summary>
    /// <returns></returns>
    public IEnumerator Result_Exec()
    {
        int a_nPlayerDialogID = PlayerInfo.instance.userRank - 2;

        //	ユーザーダイアログ
        SelectProfileDialog a_UserDialog = m_ProfileList.userDialog;
        //	選手ダイアログ
        SelectProfileDialog a_PlayerDialog = m_ProfileList.PlayerDialog_Get(a_nPlayerDialogID);

        //	結果
        GameInfo.stBattleResult a_Res = GameInfo.instance.resultData;

        //	タッチ制御
        TouchDisable.instance.SetDisable(true);

        //  インタースティシャル広告のリクエスト
        //  ※リクエストから表示されるまでが長すぎるので先にリクエストしておく
        bool    a_bFailed = false;
        bool    a_bClosed = false;
#if !UNITY_EDITOR
        def.AdManager.RequestInterstitial();

        //  広告のロードに失敗した時の処理
        def.AdManager.interstitial.AdFailedToLoad +=
            ((sender, args) =>
                {
                    a_bFailed = true;
                }
        );
        //  広告を閉じた時の処理
        def.AdManager.interstitial.AdClosed +=
            ((sender, args) =>
                {
                    a_bClosed = true;
                }
        );
#endif

        //	対戦結果のアニメーション再生
        yield return StartCoroutine(VSAnim_Exec("result", a_PlayerDialog, a_UserDialog, true, a_Res.isUserWin));

#if !UNITY_EDITOR
        if(!a_bFailed)
        {
            //	インタースティシャル広告が表示されるまで待機
            yield return StartCoroutine(def.AdManager.ShowInterstitial_Exec());

            //	インタースティシャル広告が閉じられるまで待機
            while (!a_bClosed)
            {
                yield return 0;
            }

            yield return new WaitForSeconds(1f);
        }
#endif

        //	ユーザーの勝利
        if (a_Res.isUserWin)
		{
			//	ユーザーのランクを１つ上げる
			PlayerInfo.instance.userRank--;

			//	ランクを入れ替えるアニメーション
			{
				iTween.MoveTo(a_UserDialog.gameObject, iTween.Hash("x", -400, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));
				iTween.MoveTo(a_PlayerDialog.gameObject, iTween.Hash("x", 400, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));

				yield return new WaitForSeconds(1f);

				float	a_fMoveY	= m_ProfileList.dialogSize.y;

				float	a_fCurrentY = a_UserDialog.transform.localPosition.y;
				iTween.MoveTo(a_UserDialog.gameObject, iTween.Hash("y", a_fCurrentY + a_fMoveY, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));

				a_fCurrentY = a_PlayerDialog.transform.localPosition.y;
				iTween.MoveTo(a_PlayerDialog.gameObject, iTween.Hash("y", a_fCurrentY - a_fMoveY, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));

				yield return new WaitForSeconds(1f);

				iTween.MoveTo(a_UserDialog.gameObject, iTween.Hash("x", 0, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));
				iTween.MoveTo(a_PlayerDialog.gameObject, iTween.Hash("x", 0, "time", 1, "islocal", true, "easeytype", iTween.EaseType.linear));

				yield return new WaitForSeconds(1f);
			}

			//	表示しているランクを更新
			a_UserDialog.rankLabel.text = PlayerInfo.instance.userRank.ToString();

			//	選手のランクを１つ下げる
			int a_nPlayerRank = a_PlayerDialog.profileList[0].worldRank;
			a_nPlayerRank++;
			a_PlayerDialog.rankLabel.text = a_nPlayerRank.ToString();

			//	ユーザーが勝った時に行うアニメーション
			yield return StartCoroutine(a_PlayerDialog.Lose_Exec());

			yield return new WaitForSeconds(1f);

			//	１位になったらアニメーションは行わない
			if(PlayerInfo.instance.userRank != 1)
			{
				//	次の対戦相手のフェードを非表示にする
				a_PlayerDialog = m_ProfileList.PlayerDialog_Get(a_nPlayerDialogID - 1);
				if (a_PlayerDialog != null)
				{
					a_PlayerDialog.fade.SetActive(false);
				}
			}
		}

		//	ランクが１位になった時はタイトルに遷移
		if(PlayerInfo.instance.userRank == 1)
		{
			StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));
		}
		else
		{
			//	タッチ制御解除
			TouchDisable.instance.SetDisable(false);
		}
	}

	/// <summary>
	/// 対戦する時のアニメーションを実行する
	/// </summary>
	/// <returns></returns>
	public IEnumerator	VSAnim_Exec(string p_Trigger, SelectProfileDialog p_PlayerDialog, SelectProfileDialog p_UserDialog, bool p_bResult, bool p_bWin)
	{
		//	演出はトーナメントモードのみ行う
		if (GameInfo.instance.gameMode == GameInfo.eGameMode.TOURNAMENT)
		{
			//	イントロ用のアニメーションオブジェクト生成
			VSIntro	a_Intro = ResourceManager.PrefabLoadAndInstantiate("Select/VSIntro", Vector3.zero).GetComponent<VSIntro>();
			//	親子設定
			GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, a_Intro.gameObject);

			//	アイテムをセット
			a_Intro.playerItem.Set(p_PlayerDialog.profileList);
			a_Intro.userItem.Set(p_UserDialog.profileList);

			//	得点をセット
			if(p_bResult)
			{
				GameInfo.stBattleResult	a_Result = GameInfo.instance.resultData;
				a_Intro.playerItem.PointSet(a_Result.playerScore);
				a_Intro.userItem.PointSet(a_Result.userScore);
			}

			//	アニメーションを実行（終了するまで待機）
			yield return StartCoroutine(a_Intro.Animation_Exec(p_Trigger));

			if(p_bResult)
			{
				string			a_ResultStr	= (p_bWin) ? "BattleWin" : "BattleLose";
				GameObject		a_Win		= ResourceManager.PrefabLoadAndInstantiate("Select/" + a_ResultStr, Vector3.zero);
				GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, a_Win.gameObject);
				AnimationEvent	a_Event = a_Win.GetComponent<AnimationEvent>();
				//	アニメーションが再生されるまで待機
				while (!a_Event.isAnimation)
				{
					yield return 0;
				}

				//	アニメーションが終了するまで待機
				yield return GameUtils.AnimEndWait(a_Win);

				Destroy(a_Intro.gameObject);
			}
		}
	}

	/// <summary>
	/// 「戻る」ボタンを押した時の処理
	/// </summary>
	public void	BackToTitleButtonDidPush(UIButtonTween p_Sender)
	{
		//	タイトルに遷移する
		StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
