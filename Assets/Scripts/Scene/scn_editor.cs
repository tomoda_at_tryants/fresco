﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scn_editor : MonoBehaviour {

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",					"Menu/MenuBg",					false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("ExitButton",			"Common/ExitButton",			false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Top,		new Vector3(262, -60)),
		new PrefInstantiater.Layout("BGDrawDialog",			"Editor/BGDrawDialog",			false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		Vector3.zero),
		new PrefInstantiater.Layout("CustomizeScene_Title",	"Editor/CustomizeScene_Title",	false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Top,		new Vector3(  0, -60)),
		new PrefInstantiater.Layout("RacketEditorButton",	"Editor/RacketEditorButton",	false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(  0, 280)),
		new PrefInstantiater.Layout("SensitivityButton",	"Editor/SensitivityButton",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(  0,  70)),
//		new PrefInstantiater.Layout("LanguageButton",		"Editor/LanguageButton",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(  0,-350)),
		new PrefInstantiater.Layout("SensitivitySelect",	"Editor/SensitivitySelect",		false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
		new PrefInstantiater.Layout("LanguageSelect",		"Common/LanguageSelect",		false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
	};
	PrefInstantiater	m_Instantiator;

	/// <summary>
	/// 背景を表示するリスト
	/// </summary>
	BGDrawDialog	m_BGDrawDialog;

	/// <summary>
	/// 初期化フラグ
	/// </summary>
	bool	m_bInitialize;

	List<UIButtonTween> m_ButtonList;

	// Use this for initialization
	IEnumerator	Start () {
		m_bInitialize = false;

		//	GUI用の解像度を更新
		GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / def.c_ScreenSize.x, Screen.height / def.c_ScreenSize.y), Vector2.zero);

		//	音量設定（タイトル画面の半分）
		SoundHelper.instance.BgmVolumeSet(def.c_fMaxVolume / 3f);

		//	共通の初期化
		bool a_bWait = true;
		def.CommonInit(() =>
			{
				a_bWait = false;
			}
		);
		//	初期化が終了するまで待機
		while (a_bWait) yield return 0;

		m_Instantiator = PrefInstantiater.Create();
		//	使用するプレハブをインスタンス化
		m_Instantiator.Build(c_Prefs);	

		//	背景の読み込み
        def.BGCreate("Bg/" + "bg_editor", "Bg_editor", PanelManager.instance.alignmanLow.alignCenter.gameObject);

        m_ButtonList = new List<UIButtonTween>();

		//	ボタン生成
		foreach (PrefInstantiater.Layout a_Layout in c_Prefs)
		{
			string a_PrefName = a_Layout.name;
			if (a_PrefName.Contains("Button"))
			{
				string	a_Event = a_PrefName + "DidPush";
				ButtonCreate(a_PrefName, a_Event);
			}
		}

		//	コンフィグラベル生成
		m_Instantiator.Get<UILabel>("CustomizeScene_Title");

		//	初期化が完了した時の処理
		def.CommonInit_Completed();

		//	初期化を完了にする
		m_bInitialize = true;
	}

	/// <summary>
	/// ボタンを生成する
	/// </summary>
	/// <param name="p_ButtonName"></param>
	/// <param name="p_Event"></param>
	public void ButtonCreate(string p_ButtonName, string p_Event)
	{
		//	ボタン生成
		UIButtonTween	a_Button = m_Instantiator.Get<UIButtonTween>(p_ButtonName);
		//	イベント設定
		a_Button.onClickEventSet(this, p_Event, "", 0);

		m_ButtonList.Add(a_Button);
	}


	void Update()
	{
		if (!m_bInitialize)	return;
	}

	public void	RacketEditorButtonDidPush(UIButtonTween p_Sender)
	{
		//	背景リスト生成
		m_BGDrawDialog = m_Instantiator.Get<BGDrawDialog>("BGDrawDialog");
		//	開始時のアニメーション
		StartCoroutine(m_BGDrawDialog.StartAnim());

		foreach(UIButtonTween a_Button in m_ButtonList)
		{
			a_Button.GetComponent<BoxCollider>().enabled = false;
		}

		//	閉じた時に呼ばれるコールバック設定
		m_BGDrawDialog.CloseCallBackSet(Racketeditor_Close);
	}

	public void	LanguageButtonDidPush(UIButtonTween p_Sender)
	{
		LanguageSelect	a_View = m_Instantiator.Get<LanguageSelect>("LanguageSelect");
		//	表示開始時のアニメーション
		StartCoroutine(a_View.StartAnim());
	}

	/// <summary>
	///	ラケットエディタダイアログを閉じた時に呼ばれるコールバック
	/// </summary>
	public void	Racketeditor_Close()
	{
		foreach (UIButtonTween a_Button in m_ButtonList)
		{
			a_Button.GetComponent<BoxCollider>().enabled = true;
		}
	}

	SelectProfileList	m_PartnerList;

	/// <summary>
	/// 感度設定ボタンが押された時の処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	SensitivityButtonDidPush(UIButtonTween p_Sender)
	{
//		SensitivitySelect	a_View = m_Instantiator.Get<SensitivitySelect>("SensitivitySelect");
		//	表示開始時のアニメーション
//		StartCoroutine(a_View.StartAnim());

		//	スイングチェックシーンに遷移する
		StartCoroutine(def.instance.SceneMove(def.Scene.SWINGCHECK, true));
	}

	/// <summary>
	/// タイトルに戻るボタン
	/// </summary>
	public void	ExitButtonDidPush(UIButtonTween p_Sender)
	{
		//	タイトルに遷移する
		StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));
	}
}
