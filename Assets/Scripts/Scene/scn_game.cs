﻿//	#define	GRAPH_ON			//	クォータニオンのグラフを表示する
//	#define AUTO_ROTATION		//	自動回転をオンにする

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class scn_game : GameSceneBase {

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
	{
		None,

		State_Init,
		State_Ready,
		State_Serving,
		State_Game,
		State_Result,

		Max
	}

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState,string>	c_StateList;

	eState	m_State;
	eState	m_NextState;
	string	m_Coroutine;
	
	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
		if (p_bContinus)
		{
			State_Check();
		}
	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
			{
				StopCoroutine(m_Coroutine);
				m_Coroutine = null;
			}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

			DebugManager.Log("Call " + m_Coroutine + "()");

			return true;
		}

		return false;
	}

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs = 
	{
//		new PrefInstantiater.Layout("Bg",					"Menu/MenuBg",				false,	-1,	PanelManager.eLayer.Low,	AlignManager.eAlign.Center,		new Vector3(0, 0, 0)),
		new PrefInstantiater.Layout("PausePref",			"Game/PausePref",			false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
		new PrefInstantiater.Layout("ResultDraw",			"Game/ResultDraw",			false,	1,	PanelManager.eLayer.High,	AlignManager.eAlign.Center,		Vector3.zero),
		new PrefInstantiater.Layout("SelectPartnerButton",	"Game/SelectPartnerButton",	false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(-260, 380)),
		new PrefInstantiater.Layout("ServeExplain",			"Common/ServeExplain",		false,	1,	PanelManager.eLayer.Mid,	AlignManager.eAlign.Center,		new Vector3(0, 220)),
	};

	public	GameObject	canvas;

	public	GameObject	sensorCheckButton;
	public	GameObject	sensorCheckRoot;

	public	GameObject	sensorObjPref;

	GameObject			m_PosObjRoot;
	GameObject[]		m_PosObj;

	ResultManager		m_ResultMan;

	UIButtonTween		m_SelectPartnerButton;

	SelectProfileList	m_PartnerList;

	/// <summary>
	/// デバッグ時に表示するルート
	/// </summary>
	[SerializeField]
	GameObject	m_DebugRoot;

	public static bool	c_bAutoPlay;
	/// <summary>
	/// 一時的なフラグ
	/// </summary>
	bool	m_bAuto_Buf;

	bool	m_bDebugSet;

    /// <summary>
    /// 広告のロードに失敗した
    /// </summary>
    bool    m_bAdLoadFailed;

	// Use this for initialization
	void Start () 
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState	a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}
		
		m_State		= eState.None;
		m_NextState = eState.State_Init;

		//	「State_Init」を呼び出す
		State_Next(eState.State_Init, true);
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Init()
	{
		m_bInitialized = false;

		//	GUI用の解像度を更新
		GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / def.c_ScreenSize.x, Screen.height / def.c_ScreenSize.y), Vector2.zero);

#if !DEBUG_VERSION
		Destroy(canvas);
#endif

        //  インタースティシャル広告のリクエスト
        //  ※リクエストから表示されるまでが長すぎるので先にリクエストしておく
#if !UNITY_EDITOR
        if(GameInfo.instance.isFreeMode)
        {
            def.AdManager.RequestInterstitial();
            //  広告のロードに失敗した時の処理
            def.AdManager.interstitial.AdFailedToLoad +=
                ((sender, args) =>
                {
                    m_bAdLoadFailed = true;
                }
            );
        }
#else
        m_bAdLoadFailed = true;
#endif

        //	選択されているプレイヤーＩＤ取得
        int a_nID = GameInfo.instance.partnerProfile.id;
        if (a_nID < 0)
        {
            a_nID = 0;
            DebugManager.Log("selectPlayerID is not found = " + a_nID);
        }

        //  ゲームシーンの共通初期化
        yield return StartCoroutine(GameScene_Init(BallManager.eUseType.Game, a_nID, c_Prefs));

		//	ヘッダー
		Header.Create();

		//	フリーモードのみパートナー選択ボタンを表示
		if(GameInfo.instance.isFreeMode)
		{
			//	パートナー選択ボタン
			m_SelectPartnerButton = m_Instantiator.Get<UIButtonTween>("SelectPartnerButton");
			//	イベント設定
			m_SelectPartnerButton.onClickEventSet(this, "SelectPartnerButtonDidPush", "", 0);
		}

		//	評価管理
		EvaluationManager.Create();

        //	ＢＧＭ
        SoundHelper.instance.BgmPlay(SoundHelper.eNo.bgm_sea);

        //	初期化を完了にする
        m_bInitialized = true;

        //	「State_Ready」を呼び出す
        State_Next(eState.State_Ready, true);
	}

	/// <summary>
	/// パートナー選択ボタンを押した時に呼ばれる
	/// </summary>
	/// <param name="p_Sender"></param>
	public void	SelectPartnerButtonDidPush(UIButtonTween p_Sender)
	{
		//	スイング用センサーを無効にする
		m_PlayerManager.sensor.SetEnable(false);

		//	ヘッダーのボタン無効
		Header.instance.ButtonEnable(false);

		//	パートナー変更用ダイアログ生成
		m_PartnerList = ResourceManager.PrefabLoadAndInstantiate("Select/SelectProfileList", Vector3.zero).GetComponent<SelectProfileList>();
		GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, m_PartnerList.gameObject);
		CSTransform.SetPos_Y(m_PartnerList.transform, 20);

		//	初期化
		m_PartnerList.Init(SelectProfileList.eViewType.Partner, true, null, ChangePartnerSet);
		//	閉じる時に呼ばれるコールバックの設定
		m_PartnerList.CloseCallBack_Set(ChangePartnerClose);

		//	ヘッダーは非表示にする
		Header.instance.GameActiveSet(false);
		//	パートナー選択ボタンを非表示にする
		m_SelectPartnerButton.gameObject.SetActive(false);
	}

	/// <summary>
	/// パートナーを選択した時に呼ばれる
	/// </summary>
	/// <param name="p_ProfileDraw"></param>
	public void	ChangePartnerSet(SelectProfileDraw p_ProfileDraw)
	{
		//	選択されているプレイヤーＩＤ取得
		int a_nID = p_ProfileDraw.info.id;
		if (a_nID < 0)
		{
			a_nID = 0;
			DebugManager.Log("selectPlayerID is not found = " + a_nID);
		}

		//	パートナー選手を記録
		GameInfo.stProfile	a_Profile	= p_ProfileDraw.info;
		GameInfo.instance.PartnerProfile_Set(a_Profile);
		PlayerData.Param	a_Param		= def.instance.playerTable.Param_Get(a_Profile.id);
		m_CPUManager.param = a_Param;

		//	攻守交替に必要なスイング回数設定
		m_BallManager.changeChallengeCount = def.instance.atkChangeTable.ChallengeTimes_Get(a_Param.Aggressive);

		//	パートナー関連の更新
		m_PartnerList.PartnerUpdate();
	}

	/// <summary>
	/// パートナー選択ダイアログを閉じた時に呼ばれる
	/// </summary>
	public void	ChangePartnerClose()
	{
		//	スイング用センサーを有効にする
		m_PlayerManager.sensor.SetEnable(true);

		//	ヘッダーは表示する
		Header.instance.GameActiveSet(true);
		//	ボタンを有効にする
		Header.instance.ButtonEnable(true);
		//	パートナー選択ボタンを表示する
		m_SelectPartnerButton.gameObject.SetActive(true);
	}

	/// <summary>
	/// ゲーム準備画面
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Ready()
	{
		//	ボールの準備が出来るまで待機
		while (!m_BallManager.isReady)	yield return 0;

		yield return new WaitForSeconds(0.3f);

		//	「State_Serving」を呼び出す
		State_Next(eState.State_Serving, true);
	}

	/// <summary>
	/// サーブが成功するまで呼ばれる
	/// </summary>
	/// <returns></returns>
	public IEnumerator State_Serving()
	{
		//	サーブ説明用画像表示
		UISprite	a_ServeExplain = m_Instantiator.Get<UISprite>("ServeExplain");

		//	結果用データ
		SwingManager.stResShotData	a_Res = new SwingManager.stResShotData();

		//	センサーチェック
//		SensorCheck();

		//	サーブを打つ前はこのwhileをループする
		while (true)
		{
			//	自動
			if (c_bAutoPlay)
			{
				m_BallManager.isServed = true;
				a_Res = new SwingManager.stResShotData(def.instance.shotTable.fookShotData, Shot_AdjustTable.eAdjust.PERFECT);
				yield return StartCoroutine(m_PlayerManager.AutoSwing_Exec(a_Res));

				a_ServeExplain.enabled = false;
				if(GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)	m_SelectPartnerButton.forceDisable(true);

				break;
			}
			else
			{
				//	サーブショット判定
				m_PlayerManager.ShotDecision();
				//	サーブを打ったらループを抜ける
				if (m_BallManager.isServed) break;
			}

			//	サーブアニメーション中は説明画像は表示しない
			if (m_BallManager.isServeAnim)
			{
				//	説明用画像を非表示にする
				a_ServeExplain.enabled = false;
				//	フリーモード時はパートナー選択ボタンを非表示にする
				if(GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)	m_SelectPartnerButton.forceDisable(true);

				//	タッチを無効にする
				m_PlayerManager.sensor.SetEnable(false);
			}
			else
			{
				//	説明用画像を表示する
				a_ServeExplain.enabled = true;
				//	フリーモード時はパートナー選択ボタンを表示する
				if (GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)	m_SelectPartnerButton.forceEnable();

				//	タッチを有効にする
				m_PlayerManager.sensor.SetEnable(true);
			}

			yield return 0;
		}

		//	サーブを打ち終わった後に呼ばれる
		Served_Exec();

		while (m_bDebug) yield return 0;

		//	ゲーム処理に移動
		State_Next(eState.State_Game, true);
	}

	/// <summary>
	/// ゲーム処理
	/// </summary>
	/// <returns></returns>
	public IEnumerator	State_Game()
	{
		//	結果用データ
		SwingManager.stResShotData	a_Res = new SwingManager.stResShotData();

		//	ラリー中はループ
		while(true)
		{
			//	ボールを打てるタイミングになるまで待機
			while (!m_BallManager.isShotTiming)
			{
				yield return 0;
			}

			//	試合時間終了
			if (Header.instance.isTimeUp)
			{
				State_Next(eState.State_Result, true);
			}

			//	ＣＰＵ
			if (m_BallManager.isReceiveCPU)
			{
				//	遅延時間取得
				float	a_fLag = m_CPUManager.ShotLag_Culc();
				if (a_fLag > 0f) yield return new WaitForSeconds(a_fLag);

				//	ショットデータ取得
				a_Res = m_CPUManager.ShotCheck();
				//	到達したら打ち返す
				yield return StartCoroutine(m_CPUManager.Swing_Exec(a_Res));

				continue;
			}
			else
			//	プレイヤー
			if (m_BallManager.isReceivePlayer)
			{
				//	振り始めデータを取得していない
				if (!m_PlayerManager.isGetBeginSwingData)
				{
					m_PlayerManager.BeginSwingData_Get();
				}
				else
				{
					//	手動プレイの時はショット判定を行う
					if(!c_bAutoPlay)	m_PlayerManager.ShotDecision();
				}

				//	自動
				if (c_bAutoPlay)
				{
					Shot_AdjustTable.eAdjust	a_Adjust = Shot_AdjustTable.eAdjust.NONE;

					//	ボールが「Perfect」判定になるまで待機
					while (true)
					{
						a_Adjust = def.instance.adjustTable.Adjust_Get(m_BallManager.ball.transform.localScale.x);
						if (a_Adjust == Shot_AdjustTable.eAdjust.PERFECT)	break;

						yield return 0;
					}

					//	攻守交替時等によってショットの種類を変える
					ShotData.Param	a_Param = def.instance.shotTable.foreShotData;
					if (BallManager.instance.isChangeAttack)
					{
						if (m_PlayerManager.swingMan.state == SwingManager.eState.Deffence)
						{
							//	ダストショット
							a_Param = def.instance.shotTable.fookShotData;
						}
						else
						if (m_PlayerManager.swingMan.state == SwingManager.eState.Offence)
						{
							//	ロブショット
							a_Param = def.instance.shotTable.dustShotData;
						}
					}
					else
					{
						//	フォアハンド
						a_Param = def.instance.shotTable.foreShotData;
					}

					a_Adjust	= (Shot_AdjustTable.eAdjust)UnityEngine.Random.Range(1, 3 + 1);
					a_Res		= new SwingManager.stResShotData(a_Param, a_Adjust);

					//	到達したら打ち返す
					yield return	StartCoroutine(m_PlayerManager.AutoSwing_Exec(a_Res));

					continue;
				}
				//	手動
				else
				{
					//	猶予期間が過ぎたらミス
					if (m_BallManager.isMiss)
					{
						//	時計を止める
						Header.instance.GamePause();
						State_Next(eState.State_Ready, true);
					}
				}
			}

			yield return 0;
		}
	}

	/// <summary>
	/// サーブを打ち終わった後に呼ばれる処理
	/// </summary>
	public void	Served_Exec()
	{
		//	時計を動かす
		Header.instance.GameStart();
		if(GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)	m_SelectPartnerButton.gameObject.SetActive(false);
	}

	/// <summary>
	/// 結果画面
	/// </summary>
	/// <returns></returns>
	public IEnumerator State_Result()
	{
		//	ＳＥ：試合終了
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_whistle_end);

        //  ポーズボタンを不能にする
        Header.instance.pauseButton.forceDisable();

		yield return new WaitForSeconds(3);

		//	ヘッダー非表示
		Header.instance.ActiveSet(false);

		yield return 0;

		//	ＳＥ：歓声
		SoundHelper.instance.SePlay(SoundHelper.eNo.se_gameend);

		def.c_nSpeedUpCount = 0;

		//	トーナメントモード時は結果を登録する
		if(GameInfo.instance.gameMode == GameInfo.eGameMode.TOURNAMENT)
		{
			//	プレイヤーの点数を出す
			int a_nPlayerScore = 0;
			List<GameInfo.stProfile>	a_PlayerProfileList = GameInfo.instance.resultData.playerProfileList;
			foreach(GameInfo.stProfile a_Profile in a_PlayerProfileList)
			{
				a_nPlayerScore += def.instance.playerTable.paramList[a_Profile.id].Level;
			}
			a_nPlayerScore /= 2;	//	プレイヤー２人のレベルの平均
			//	点数を出す（平均点±５点）
			a_nPlayerScore = UnityEngine.Random.Range(a_nPlayerScore - 5, a_nPlayerScore + 5);

			//	ユーザーの点数
			int a_nUserScore	= EvaluationManager.instance.scoreData.totalScore;

			GameInfo.instance.resultData.ScoreSet(a_nPlayerScore, a_nUserScore);
		}

		//	ポーズ画面に必要なオブジェクトを生成する
		if (m_ResultMan == null)
		{
			m_ResultMan = m_Instantiator.Get<ResultManager>("ResultDraw");

			//	イベント設定
			m_ResultMan.retryButton.onClickEventSet(this, "RetryButtonDidPush", "", 0);
			m_ResultMan.quitButton.onClickEventSet(this, "QuitButtonDidPush", "", 0);
			m_ResultMan.resultButton.onClickEventSet(this, "QuitButtonDidPush", "", 0);

			yield return 0;

			yield return StartCoroutine(m_ResultMan.Play());
		}
		//	既に生成されている場合は表示
		else
		{
			m_ResultMan.gameObject.SetActive(true);
		}

		yield return 0;
	}

	/// <summary>
	/// リトライボタンを押した時の処理
	/// </summary>
	public void RetryButtonDidPush(UIButtonTween p_Sender)
	{
		Retry_Exec();
	}

	/// <summary>
	/// リトライ処理
	/// </summary>
	/// <returns></returns>
	void	Retry_Exec()
	{
		//	タイトルに遷移
		StartCoroutine(def.instance.SceneMove(def.Scene.GAME, true));
	}

	/// <summary>
	/// やめるボタンが押された時に行う処理
	/// </summary>
	public void QuitButtonDidPush(UIButtonTween p_Sender)
	{
		def.instance.QuitScene(!m_bAdLoadFailed && GameInfo.instance.isFreeMode);   //  インタースティシャル表示
	}

	void Update () 
	{
		if (!m_bInitialized)	return;

		//	ステートを調べる
		State_Check();
	}

	public void	DebugViewButtonDidPush()
	{
		bool	a_bActive = m_DebugRoot.activeSelf;
		m_DebugRoot.SetActive(a_bActive ^ true);
	}

	public void	AutoButtonDidPush(Text p_Text)
	{
		m_bAuto_Buf ^= true;
		p_Text.text = (m_bAuto_Buf ? "自動" : "手動");
	}

	public void	DebugSetButtonDidPush()
	{
		c_bAutoPlay = m_bAuto_Buf;

		m_bDebugSet = true;
	}

	bool	m_bDebug;
	public void	onSensorCheckButtonDidPush()
	{
		//	現在表示されているボタンは非表示にする
		sensorCheckButton.SetActive(false);
		sensorCheckRoot.SetActive(true);
	}

	public void DebugStart(out TextMesh[] p_Meshes, int p_nArray)
	{
		m_PosObjRoot	= new GameObject("PosObjRoot");
		GameUtils.AttachChild(PanelManager.instance.alignmanMid.alignCenter.gameObject, m_PosObjRoot);

		m_PosObj	= new GameObject[p_nArray];
		p_Meshes	= new TextMesh[p_nArray];

		for (int i = 0; i < p_nArray ; i++)
		{
			GameObject	a_Obj	= new GameObject(string.Format("pos_{0}", i));
			TextMesh	a_Mesh	= new TextMesh();

			a_Obj.transform.parent		= PanelManager.instance.alignmanMid.alignCenter.transform;
			a_Obj.transform.localScale	= Vector3.one;

			a_Obj.AddComponent<MeshRenderer>();
			a_Mesh = a_Obj.AddComponent<TextMesh>();

			a_Mesh.color			= Color.black;
			a_Mesh.characterSize	= 16;
			a_Mesh.fontSize			= 26;
			a_Mesh.anchor			= TextAnchor.LowerLeft;
			a_Mesh.transform.localPosition = new Vector3(-310, -420 + (-50 * i));

			p_Meshes[i] = a_Mesh;
			m_PosObj[i] = a_Obj;

			m_PosObj[i].transform.parent = m_PosObjRoot.transform;
		}
	}

	public void DebugEnd()
	{
		m_bDebug = false;

		Destroy(m_PosObjRoot);

		sensorCheckRoot.SetActive(false);
		sensorCheckButton.SetActive(true);

		//	「ResultLabel」削除
		m_PlayerManager.swingMan.ResultLabelDestroy();
		m_CPUManager.swingMan.ResultLabelDestroy();

		//	ボールを表示する
		m_BallManager.gameObject.SetActive(true);
		//	ヘッダーを表示する
		Header.instance.ActiveSet(true);
		//	背景を表示する
		if(m_Bg != null)	m_Bg.gameObject.SetActive(true);
	}	

	/// <summary>
	/// 加速度センサーのチェック
	/// </summary>
	public void	SensorCheck()
	{
		//	既にデバッグ中なら終了
		if (m_bDebug)	return;

		m_bDebug = true;

		//	ボールを非表示にする
		m_BallManager.gameObject.SetActive(false);
		//	ヘッダーを非表示にする
		Header.instance.ActiveSet(false);
		//	背景を非表示にする
		if(m_Bg != null)	m_Bg.gameObject.SetActive(false);

		TextMesh[] pos_Meshes;
		//	「x,y,z」
		DebugStart(out pos_Meshes, 3);

		//	加速度センサーテスト
		StartCoroutine(Sensor_Update(pos_Meshes));
	}

	public IEnumerator Sensor_Update(TextMesh[] p_Meshes)
	{
		Vector3		a_MinAcc	= Vector3.zero;
		Vector3		a_MaxAcc	= Vector3.zero;
		Vector3		a_ForView	= Vector3.zero;	//	表示用
		GameObject	a_Obj		= GameObject.Instantiate(sensorObjPref);
		Vector2		a_Size		= a_Obj.transform.GetChild(0).GetComponent<UISprite>().localSize;

		GameUtils.AttachChild(PanelManager.instance.alignmanMid.alignCenter.gameObject, a_Obj);

		while (m_bDebug)
		{
			Vector3	a_Acc	= def.swingSpeed;

			a_Obj.transform.localPosition = new Vector3(a_Acc.x * (def.c_ScreenSize.x / 1000), a_Acc.y * (def.c_ScreenSize.y / 1000));
			a_ForView	= a_Obj.transform.localPosition;

			//	左右の移動範囲制限
			float	a_fRight	= (def.c_ScreenSize.x / 2) - (a_Size.x / 2);
			float	a_fLeft		= -a_fRight;
			a_ForView.x = Mathf.Clamp(a_ForView.x, a_fLeft, a_fRight);

			//	上下の移動範囲制限
			float	a_fTop		= (def.c_ScreenSize.y / 2) - (a_Size.y / 2);
			float	a_fBottom	= -a_fTop;
			a_ForView.y = Mathf.Clamp(a_ForView.y, a_fBottom, a_fTop);

			if (p_Meshes[0] != null)
			{
				if (a_MinAcc.x > a_Acc.x)	a_MinAcc.x = a_Acc.x;
				if (a_MaxAcc.x < a_Acc.x)	a_MaxAcc.x = a_Acc.x;

				p_Meshes[0].text = string.Format("x: min:{0},max:{1}", Mathf.FloorToInt(a_MinAcc.x), Mathf.FloorToInt(a_MaxAcc.x));
			}

			if (p_Meshes[1] != null)
			{
				if (a_MinAcc.y > a_Acc.y)	a_MinAcc.y = a_Acc.y;
				if (a_MaxAcc.y < a_Acc.y)	a_MaxAcc.y = a_Acc.y;

				p_Meshes[1].text = string.Format("y: min:{0},max:{1}", Mathf.FloorToInt(a_MinAcc.y), Mathf.FloorToInt(a_MaxAcc.y));
			}

			if (p_Meshes[2] != null)
			{
				if (a_MinAcc.z > a_Acc.z)	a_MinAcc.z = a_Acc.z;
				if (a_MaxAcc.z < a_Acc.z)	a_MaxAcc.z = a_Acc.z;

				p_Meshes[2].text = string.Format("z: min:{0},max:{1}", Mathf.FloorToInt(a_MinAcc.z), Mathf.FloorToInt(a_MaxAcc.z));
			}

			//	座標を更新
			a_Obj.transform.localPosition = a_ForView;

			yield return 0;
		}
		Destroy(a_Obj);
	}
}
