using UnityEngine;
using System.Collections;

/// <summary>
/// 自動的に、UIRoot(3D)のサイズに合うように、カメラのＺ値を設定する
/// →Ｚ＝０で横方向がUIRootのContentWidthになるようにする
/// </summary>
[ExecuteInEditMode]
public class AutoScreenCamera : MonoBehaviour {

	UIRoot	m_UIRoot;
	Camera	m_Camera;

	void Awake()
	{
	}

	void Start()
	{
		m_UIRoot = gameObject.GetComponentInParent<UIRoot>();
		m_Camera = gameObject.GetComponent<Camera>();


		if ((m_UIRoot == null) || (m_Camera == null))
		{
			Destroy(this);
			return;
		}


		if (!Application.isEditor)
		{
			if (!m_UIRoot.fitHeight && !m_UIRoot.fitWidth)
			{
				Destroy(this);
				return;
			}
		}


		CalcZ();	
	}

	// Update is called once per frame
	void Update () {
		if (Application.isEditor)
		{
			CalcZ();
		}
	}
	
	/// <summary>
	/// カメラのＺを設定する
	/// </summary>
	void CalcZ()
	{
		float a_fAspect = (float)Screen.height / (float)Screen.width;

		float a_ScreenSize;
		if (m_UIRoot.fitWidth)
		{
			a_ScreenSize = m_UIRoot.manualWidth * a_fAspect;
		}
		else
		{
			a_ScreenSize = m_UIRoot.manualHeight;
		}

		float a_Fov = m_Camera.fieldOfView;

		float a_Z = -(a_ScreenSize * 0.5f) / Mathf.Tan(a_Fov * 0.5f * Mathf.Deg2Rad);

		Vector3 a_Pos = transform.localPosition;
		a_Pos.z = a_Z;
		transform.localPosition = a_Pos;
	}
	
}
