using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{

	static SoundManager s_Instance;
	public static SoundManager instance
	{
		get
		{
			if (s_Instance == null)
			{
				Create();
			}
			return s_Instance;
		}
	}

	Dictionary<int, AudioClip> m_Clips;// = new Dictionary<int, AudioClip>();
	AudioSource[] m_Tracks = null;

	public static int c_TrackNum = 4;	// BGM 1 + SE 3
	public static int c_DefaultPriority = 128;
	public static float c_DefaultPitch = 1.0f;
	public static float c_DefaultVolume = 1.0f;

	float m_Volume = c_DefaultVolume;

	public Dictionary<int, AudioClip> clips
	{
		set { m_Clips = value; }
		get { return m_Clips; }
	}

	public static SoundManager Create()
	{
		if (s_Instance != null)
		{
			return s_Instance;
		}

		GameObject obj = new GameObject("SoundManager");
		s_Instance = obj.AddComponent<SoundManager>();

		return s_Instance;

	}

	void Awake()
	{
		if ((s_Instance != null) && (s_Instance != this))
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		s_Instance = this;

	}

	// Use this for initialization
	void Start()
	{
		// Get SoundSource
		//		m_Tracks = GetComponents<AudioSource>();

		m_Tracks = new AudioSource[c_TrackNum];
		for (int i = 0; i < c_TrackNum; i++)
		{
			m_Tracks[i] = gameObject.AddComponent<AudioSource>();
		}
	}

	// Update is called once per frame
	void Update()
	{

	}

	public AudioSource SeTrackGet(int p_nTrackNo, int p_nPriorityOffset, bool p_bMultiplex, AudioClip p_Clip)
	{
		int a_nPriority = c_DefaultPriority + p_nPriorityOffset;

		//		int a_Priority = 128 + m_Tracks.Length - p_nTrackNo;

		// 多重鳴音可能な場合は同じサウンドクリップが再生されていないか調べる。
		if (!p_bMultiplex)
		{
			for (int i = 1; i < m_Tracks.Length; i++)
			{
				if (m_Tracks[i].clip == p_Clip)
				{
					m_Tracks[i].priority = a_nPriority;
					return m_Tracks[i];
				}
			}
		}

		// 指定トラックが空いているか？
		for (int i = 1; i < m_Tracks.Length; i++)
		{
			if (!m_Tracks[i].isPlaying)
			{
				m_Tracks[i].priority = a_nPriority;
				return m_Tracks[i];
			}
		}

		// 指定トラックが空いていないときは、優先が高ければ今鳴っている音を止めてから鳴らす。
		for (int i = 1; i < m_Tracks.Length; i++)
		{
			if (m_Tracks[i].priority > a_nPriority)
			{
				m_Tracks[i].Stop();
				m_Tracks[i].priority = a_nPriority;
				return m_Tracks[i];
			}
		}

		// 同じ優先のトラックがあれば止めてから鳴らす
		for (int i = 1; i < m_Tracks.Length; i++)
		{
			if (m_Tracks[i].priority == a_nPriority)
			{
				m_Tracks[i].Stop();
				m_Tracks[i].priority = a_nPriority;
				return m_Tracks[i];
			}
		}

		// それでもなければあきらめる
		//		m_Tracks[1].Stop();
		//		m_Tracks[1].priority = a_nPriority;
		//		return m_Tracks[1];

		return null;

	}

	public AudioSource BgmTrackGet()
	{
		return	(m_Tracks != null) ? m_Tracks[0] : null;
	}

	public void StopAll()
	{
		BgmStop();
		SeStop();
	}

	public void SePlay(int p_No, int p_nTrackNo)
	{
		//		Debug.Log("PlaySe[" + m_Tracks[p_nTrackNo] + "] = " + p_No);

		SePlay(p_No, p_nTrackNo, c_DefaultPriority, c_DefaultPitch, false);
	}

	public void SePlay(int p_No, int p_nTrackNo, int p_Priority, float p_Pitch)
	{
		SePlay(p_No, p_nTrackNo, p_Priority, p_Pitch, false);
	}

	public void SePlay(int p_No, int p_nTrackNo, int p_Priority, float p_Pitch, bool p_bMultiplex, bool p_bLoop = false, float p_fVolume = 1f)
	{
//		Debug.Log("PlaySe[" + m_Tracks[p_nTrackNo] + "] = " + p_No);

		AudioSource track = SeTrackGet(p_nTrackNo, p_Priority, p_bMultiplex, (AudioClip)m_Clips[p_No]);

		track.clip = (AudioClip)m_Clips[p_No];

		if (track.clip == null)
		{
			Debug.LogError("SePlay Error. AudioCLip is null");
			return;
		}

		track.volume = p_fVolume;
		track.loop = p_bLoop;
		track.pitch = p_Pitch;
		track.Play();
		//		Debug.Log("PlaySe end");
	}

	public void SeStop()
	{
		for (int i = 1; i < m_Tracks.Length; i++)
		{
			if (m_Tracks[i] != null)
			{
				m_Tracks[i].Stop();
			}
		}
	}

	public void BgmPlay(int p_No)
	{
		BgmPlay(p_No, true);
	}

	public void	BgmVolumeSet(float p_Vol)
	{
		AudioSource	track = BgmTrackGet();
		if (track != null)	track.volume = p_Vol;
	}

	public void BgmPlay(int p_No, bool p_Loop, float p_fVolume = 1f)
	{
		AudioSource track = BgmTrackGet();

		if (track.clip != (AudioClip)m_Clips[p_No])
		{
			track.clip = (AudioClip)m_Clips[p_No];

			if (track.clip == null)
			{
				Debug.LogError("SePlay Error. AudioCLip is null");
				return;
			}

		}

		track.volume	= p_fVolume;
		track.priority	= c_DefaultPriority;
		track.loop		= p_Loop;

		if (!track.isPlaying)
		{
			track.Play();
		}
	}

	public void BgmStop(float p_FadeTime = 0)
	{
		AudioSource track = BgmTrackGet();
		if (track != null)
		{
			if (p_FadeTime == 0)
			{
				track.Stop();
			}
			else
			{
				StartCoroutine(Fade_Exec(0, p_FadeTime, 0, true));
			}
		}
	}

	public IEnumerator Fade_Exec(int p_nTrackNo, float p_FadeTime, float p_Volume, bool stopAtFinish)
	{

		AudioSource track = m_Tracks[p_nTrackNo];

		float vol0 = track.volume;

		float t = 0;

		while (t < p_FadeTime)
		{
			yield return new WaitForFixedUpdate();

			t += Time.fixedDeltaTime;
			if (t >= p_FadeTime)
			{
				t = p_FadeTime;
			}

			float vol = vol0 + ((p_Volume - vol0) * (t / p_FadeTime));

			if (t >= p_FadeTime)
			{
				t = p_FadeTime;
			}

			track.volume = vol;

		}

		if (stopAtFinish)
		{
			track.Stop();
		}

	}

}
