﻿using UnityEngine;
using System.Collections;

public class SetQuaternion : MonoBehaviour {

	public bool		isSet;

	public float	X;
	public float	Y;
	public float	Z;
	public float	W;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isSet)	transform.rotation = new Quaternion(X, Y, Z, W);
	}

	public void	Set(Quaternion p_Quat)
	{
		X = p_Quat.x;
		Y = p_Quat.y;
		Z = p_Quat.z;
		W = p_Quat.w;
	}
}
