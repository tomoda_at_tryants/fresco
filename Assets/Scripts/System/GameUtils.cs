﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameUtils : MonoBehaviour {

	/// <summary>
	///	手ブレ補正（カーブを滑らかにする）
	/// </summary>
	/// <param name="p_Acceleration"></param>
	/// <returns></returns>
	public static Vector3	Acceleration_LowPass(Vector3 p_Acceleration)
	{
		float	a_fMulti				= 0.25f;			//	倍率
		float	a_fInterVal				= 60 * a_fMulti;	//	デフォルトは60
		float	a_fUpdateInterval		= 1f / a_fInterVal;
		float	a_fKernelWidthInSeconds	= 1f;
		float	a_fFilterFactor			= a_fUpdateInterval / a_fKernelWidthInSeconds;

		//	結果を取得
		Vector3 a_Res = Vector3.Lerp(p_Acceleration, def.swingSpeed, a_fFilterFactor);
		//	Ｚは０のまま
		a_Res.z = 0;

		return	a_Res;
	}

	/// <summary>
	///	現在再生されているアニメーションの再生時間を取得する
	/// </summary>
	/// <returns></returns>
	public static float CurrentAnimatorTime_Get(Animator p_Animator, int p_nLayer = 0)
	{
		float	a_fTime = 0f;
		if (p_Animator == null)	return a_fTime;

		AnimatorStateInfo	a_State = p_Animator.GetCurrentAnimatorStateInfo(p_nLayer);
		a_fTime = a_State.length;

		return a_fTime;
	}

	/// <summary>
	/// パーティクルの再生時間を取得する
	/// </summary>
	/// <returns></returns>
	public static float ParticleResumeTime_Get(GameObject p_Object)
	{
		float				a_fResumeTime	= 0f;
		ParticleSystem[]	a_Particles		= p_Object.GetComponentsInChildren<ParticleSystem>();
		if (a_Particles.Length > 0)
		{
			foreach (ParticleSystem p in a_Particles)
			{
				float	f = p.duration + p.startLifetime;	//	参照しているデータの再生時間取得
				//	一番長い時間を保存
				if (f > a_fResumeTime)	a_fResumeTime = f;
			}
		}

		return	a_fResumeTime;
	}

	/// <summary>
	/// アニメーションの再生時間を取得する
	/// </summary>
	/// <param name="p_Animator"></param>
	/// <param name="p_nLayerIndex"></param>
	/// <returns></returns>
	public static float	AnimationTime_Get(Animator p_Animator, int p_nLayerIndex = 0)
	{
		if (p_Animator == null) return 0;

		float	a_fTime = p_Animator.GetCurrentAnimatorStateInfo(p_nLayerIndex).length;
		return	a_fTime;
	}

	/// <summary>
	/// アニメーションが終了するまで待機する
	/// </summary>
	/// <param name="p_AnimObj"></param>
	/// <returns></returns>
	public static IEnumerator	AnimEndWait(GameObject p_AnimObj)
	{
		AnimationEvent a_Event = p_AnimObj.GetComponent<AnimationEvent>();
		if (a_Event == null)	yield break;

		while (a_Event.isAnimation) yield return 0;
	}

	/// <summary>
	/// 指定したアルファ値にセットする
	/// </summary>
	/// <param name="p_Color"></param>
	/// <param name="p_fAlpha"></param>
	public static Color	Alpha_Set(Color p_Color, float p_fAlpha)
	{
//		if (p_fAlpha < 0)	p_fAlpha = 0;
//		if (p_fAlpha > 1)	p_fAlpha = 1;

		return new Color(p_Color.r, p_Color.g, p_Color.b, p_fAlpha);
	}

	/// <summary>
	/// 数値が上昇するアニメーション
	/// </summary>
	/// <param name="p_nTo"></param>
	/// <param name="p_CallBack"></param>
	/// <returns></returns>
	public static IEnumerator LabelUpdate_Lerp(UILabel p_Label, int p_nFrom, int p_nTo, System.Action<UILabel> p_CallBack)
	{
		//	数値の桁数
		int a_nDamageRank = p_nTo.ToString().Length;
		if (p_nTo < 0) a_nDamageRank -= 1;	//	「－」分

		//	１桁の時はアニメーションはしないで終了
		if (a_nDamageRank <= 1)
		{
			p_Label.text = p_nTo.ToString();
			//	ラベルを更新する
			if (p_CallBack != null)	p_CallBack(p_Label);
			yield break;
		}

		float	a_fTimeToRank	= 0.08f;
		float	a_fAniTime		= a_nDamageRank * a_fTimeToRank;
		if (a_fAniTime > 0.4f)	a_fAniTime = 0.4f;
		float	a_fTime = 0f;

		while (a_fTime < a_fAniTime)
		{
			a_fTime += Time.deltaTime;
			int a_nNum = Mathf.FloorToInt(Mathf.Lerp(p_nFrom, p_nTo, a_fTime * (1f / a_fAniTime)));

			//	ラベル更新
			p_Label.text = a_nNum.ToString();

			//	ラベルを更新する
			if (p_CallBack != null)	p_CallBack(p_Label);

			yield return 0;
		}
		//	ラベル更新
		p_Label.text = p_nTo.ToString();
	}


	/// <summary>
	/// 成功したかを返す
	/// </summary>
	/// <param name="p_nSuccessRate">成功率</param>
	/// <returns></returns>
	public static bool Success_Check(int p_nSuccessRate)
	{
		//	０％以下は無条件で失敗
		if (p_nSuccessRate <= 0)	return false;
		//	１００％以上は無条件で成功
		if (p_nSuccessRate >= 100)	return true;

		//	０～１００
		int a_nRand = UnityEngine.Random.Range(1, 100 + 1);

		//	成功
		if (p_nSuccessRate >= a_nRand)	return true;

		//	失敗
		return false;
	}

	/// <summary>
	/// 振動させる
	/// </summary>
	public static void Vibrate()
	{
		Handheld.Vibrate();
	}

	/// <summary>
	/// 指定したプレハブの生成を行う
	/// </summary>
	/// <param name="pref"></param>
	/// <param name="parent"></param>
	/// <param name="pos"></param>
	/// <returns></returns>
	public static GameObject InstantiateAndAttach(GameObject pref, GameObject parent, Vector3 pos)
	{
		GameObject	a_Obj = (GameObject)GameObject.Instantiate(pref);
		a_Obj.transform.parent			= parent.transform;
		a_Obj.transform.localPosition	= pos;
		a_Obj.transform.localRotation	= Quaternion.identity;
		a_Obj.transform.localScale		= Vector3.one;

		return a_Obj;
	}

	/// <summary>
	/// 指定されたプレハブの生成を行う
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="pref"></param>
	/// <param name="parent"></param>
	/// <param name="pos"></param>
	/// <returns></returns>
	public static T InstantiateAndAttach<T>(GameObject pref, GameObject parent, Vector3 pos)
	{
		GameObject	a_Obj = (GameObject)GameObject.Instantiate(pref);
		a_Obj.transform.parent			= parent.transform;
		a_Obj.transform.localPosition	= pos;
		a_Obj.transform.localRotation	= Quaternion.identity;
		a_Obj.transform.localScale		= Vector3.one;

		T a_Res = a_Obj.GetComponent<T>();

		return a_Res;
	}

	/// <summary>
	///	子オブジェクトを全削除する
	/// </summary>
	/// <param name="parent"></param>
	/// <param name="forceDestroy"></param>
	public static void RemoveChildAll(GameObject parent, bool forceDestroy = false)
	{
		int	a_nCount = parent.transform.childCount;
		for (int i = a_nCount - 1; i >= 0; i--)
		{
			GameObject	child	= parent.transform.GetChild(i).gameObject;
			DontDestroy	a_DD	= child.GetComponent<DontDestroy>();
			if (forceDestroy || (a_DD == null))
			{
				GameObject.DestroyImmediate(child.gameObject);
			}
			child = null;
		}
	}

	/// <summary>
	/// 「UIWidget」のサイズを設定する
	/// </summary>
	public static void	WidgetSize_Set(GameObject p_Target, Vector2 p_Size)
	{
		if (p_Target == null)
		{
			Debug.LogError("WidgetSize_Set is null");
			return;
		}

		UIWidget	a_Widget = p_Target.GetComponent<UIWidget>();
		a_Widget.width	= Mathf.FloorToInt(p_Size.x);
		a_Widget.height = Mathf.FloorToInt(p_Size.y);
	}

	/// <summary>
	/// 「UIWidget」のサイズを取得する
	/// </summary>
	public static Vector2	WidgetSize_Get(GameObject p_Target)
	{
		if (p_Target == null)
		{
			Debug.LogError("WidgetSize_Set is null");
			return Vector2.zero;
		}

		UIWidget a_Widget	= p_Target.GetComponent<UIWidget>();
		if (a_Widget == null)	return Vector2.zero;
		Vector2	a_Size		= new Vector2(a_Widget.width, a_Widget.height);

		return	a_Size;
	}

	/// <summary>
	/// 「UIWidget」の「Depth」を設定する
	/// </summary>
	/// <param name="p_Target"></param>
	/// <param name="p_nDepth"></param>
	public static void WidgetDepth_Set(GameObject p_Target, int p_nDepth)
	{
		if (p_Target == null)
		{
			Debug.LogError("WidgetSize_Set is null");
			return;
		}

		UIWidget	a_Widget = p_Target.GetComponent<UIWidget>();
		a_Widget.depth = p_nDepth;
	}

	/// <summary>
	/// 指定したタグのオブジェクトを全て取得しソートして返す
	/// </summary>
	/// <param name="tag"></param>
	/// <returns></returns>
	public static GameObject[] FindGameObjectAndSortWithTag(string tag)
	{
		GameObject[]	foundObs = GameObject.FindGameObjectsWithTag(tag);
		Array.Sort(foundObs, CompareObNames);

		return foundObs;
	}

	private static int CompareObNames(GameObject x, GameObject y)
	{
		return x.name.CompareTo(y.name);
	}

	/// <summary>
	/// 任意の名前の子GameObjectを取得する
	/// </summary>
	/// <param name="p_Parent"></param>
	/// <param name="p_GameObjectHeadName"></param>
	/// <param name="p_nStartNo"></param>
	/// <param name="p_nNum"></param>
	/// <param name="p_nNoColumn"></param>
	/// <returns></returns>
	public static GameObject FindGameObjectByName(GameObject p_Parent, string p_GameObjectName)
	{
		Transform	a_Tr = p_Parent.transform.Find(p_GameObjectName);
		if (a_Tr == null) return null;

		return a_Tr.gameObject;

	}

	/// <summary>
	/// 任意の名前の子ゲームオブジェクトからGameObjectの中の任意コンポーネントのリストを取得する
	/// </summary>
	/// <typeparam name="Type"></typeparam>
	/// <param name="p_Parent"></param>
	/// <param name="p_GameObjectName"></param>
	/// <returns></returns>
	public static Type FindComponentByObjectName<Type>(GameObject p_Parent, string p_GameObjectName) where Type : MonoBehaviour
	{
		Transform	a_Tr = FindChildRecursive(p_Parent.transform, p_GameObjectName);

		if (a_Tr != null)
		{
			MonoBehaviour[]	a_MBList = a_Tr.gameObject.GetComponents<MonoBehaviour>();

			foreach (MonoBehaviour a_MB in a_MBList)
			{
				if (a_MB is Type)
				{
					return a_MB as Type;
				}
			}
		}
		return null;
	}

	/// <summary>
	/// 再帰的に任意の名前の子オブジェクトを検索する
	/// </summary>
	/// <param name="p_Parent"></param>
	/// <param name="p_GameObjectName"></param>
	/// <returns></returns>
	public static Transform	FindChildRecursive(Transform p_Parent, string p_GameObjectName)
	{
		if (p_Parent.name == p_GameObjectName)
		{
			return p_Parent;
		}

		for (int i = 0; i < p_Parent.childCount; i++)
		{
			Transform	a_Tr = FindChildRecursive(p_Parent.GetChild(i), p_GameObjectName);
			if (a_Tr != null)
			{
				return a_Tr;
			}
		}

		return null;
	}

	/// <summary>
	/// アルファ値のアニメーション
	/// </summary>
	/// <returns></returns>
	public static IEnumerator AlphaAnim(Color p_Color, float p_fAniTime, float p_fFrom, float p_fTo, System.Action<Color> p_CallBack)
	{
		//	アルファ値のアニメーション
		float a_fTime = 0f;
		while (a_fTime < p_fAniTime)
		{
			a_fTime += Time.deltaTime;
			float a_fAlpha = Mathf.Lerp(p_fFrom, p_fTo, a_fTime * (1f / p_fAniTime));
			p_Color = new Color(p_Color.r, p_Color.g, p_Color.b, a_fAlpha);

			p_CallBack(p_Color);

			yield return 0;
		}
	}

	/// <summary>
	/// アニメーションの速度を変更する
	/// </summary>
	/// <param name="p_TimeScale"></param>
	public static void AnimTimeScaleSet(GameObject p_Object, float p_TimeScale)
	{
		Animator[] a_Animators = p_Object.GetComponentsInChildren<Animator>();
		if (a_Animators != null)
		{
			foreach (Animator a_Animator in a_Animators)
			{
				a_Animator.speed = p_TimeScale;
			}
		}
	}

	/// <summary>
	/// アニメーションの再生タイムを変更する
	/// </summary>
	/// <param name="p_Time"></param>
	public static void AnimTimeSet(GameObject p_Object, float p_Time)
	{
		Animator[]	a_Animators = p_Object.GetComponentsInChildren<Animator>();
		if (a_Animators != null)
		{
			foreach (Animator a_Animator in a_Animators)
			{
				a_Animator.playbackTime = p_Time;
			}
		}
	}

	/// <summary>
	/// 列挙方を取得する
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="p_MemberStr"></param>
	/// <returns></returns>
	public static T EnumGet<T>(string p_MemberStr)
	{
		// typeof(MonsterExported.eAttr);
		// System.Enum.Parse(typeof(MonsterExported.eAttr), "Wood");

		T a_Res;
		try
		{
			a_Res = (T)System.Enum.Parse(typeof(T), p_MemberStr);
		}
		catch
		{
			DebugManager.Log("EnumGet Error " + p_MemberStr);
			p_MemberStr = Enum.GetNames(typeof(T))[0];
			a_Res = (T)System.Enum.Parse(typeof(T), p_MemberStr);
		}

		return a_Res;
	}

	/// <summary>
	///	子オブジェクトを設定する
	/// </summary>
	/// <param name="parent"></param>
	/// <param name="child"></param>
	public static void AttachChild(GameObject parent, GameObject child)
	{
		AttachChild(parent, child, Vector3.zero);
	}

	/// <summary>
	/// 子オブジェクトを設定する
	/// </summary>
	/// <param name="parent"></param>
	/// <param name="child"></param>
	/// <param name="p_LocalPos"></param>
	public static void AttachChild(GameObject parent, GameObject child, Vector3 p_LocalPos)
	{
		if (parent == null)
		{
			child.transform.parent = null;
			return;
		}

		child.transform.SetParent(parent.transform);
		child.transform.localPosition	= p_LocalPos;
		child.transform.localRotation	= Quaternion.identity;
		child.transform.localScale		= Vector3.one;
	}

	/// <summary>
	/// バイナリデータをロードしてテクスチャに変換する
	/// </summary>
	/// <param name="p_Tex"></param>
	/// <param name="p_BytePath"></param>
	/// <returns></returns>
	public static bool	FromByteToTexture(out UITexture p_Tex, int p_nID, string p_BytePath)
	{
		//	バイナリデータが存在するか
		bool	a_bExist = File.Exists(p_BytePath);

		GameObject	a_Obj	= new GameObject();
		p_Tex = a_Obj.AddComponent<UITexture>();

		//	存在しなければ終了
		if (!a_bExist)
		{
			return	false;
		}

		//	バイナリデータを読み込む
		byte[]		a_Byte		= File.ReadAllBytes(def.OriginalBGFilePath_Get(p_nID));
		Texture2D	a_LoadTex	= new Texture2D(0, 0);

		//	ロード
		a_LoadTex.LoadImage(a_Byte);
		p_Tex.mainTexture = a_LoadTex;

		return	true;
	}

	/// <summary>
	/// インジケーターの表示を設定する
	/// </summary>
	public static void	ActiveIndicator(bool p_bActive)
	{
		if (p_bActive)
		{
#if UNITY_IPHONE 
//			Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.WhiteLarge); 
#elif UNITY_ANDROID
			Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
#endif
			Handheld.StartActivityIndicator();
		}
		else
		{
			// インジケーター非表示
			Handheld.StopActivityIndicator(); 
		}
	}
}
