﻿
using UnityEngine;
using System.Collections;

/// <summary>
/// デバッグ時に使用する関数等が定義されている
/// </summary>
public class DebugManager : MonoBehaviour {

	static bool	c_bLog;

	/// <summary>
	///	生成
	/// </summary>
	/// <returns></returns>
	public static DebugManager	Create()
	{
		GameObject a_Obj = new GameObject();
		DebugManager	me = a_Obj.AddComponent<DebugManager>();

		me.name = "DebugManager";

		me.Init();

		return	me;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init()
	{
#if DEBUG_VERSION
		c_bLog = true;
#else
		c_bLog = false;
#endif
	}

	void Start()
	{
	}

	void Awake()
	{
	}
	
	public static void	Log(string p_Msg)
	{
		if(c_bLog)
		{
			Debug.Log(p_Msg);
		}
	}

	public static void	LogError(string p_Msg)
	{
		if(c_bLog)
		{
            Debug.LogError(p_Msg);
		}
	}
}
