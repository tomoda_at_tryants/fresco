﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ゲームシーン等に継承される汎用クラス
/// </summary>
public class GameSceneBase : MonoBehaviour {

	protected CPUManager	    m_CPUManager;
	protected PlayerManager	    m_PlayerManager;
	protected UITexture		    m_Bg;
    protected PrefInstantiater  m_Instantiator;

    /// <summary>
    /// ボール
    /// </summary>
    protected BallManager	m_BallManager
	{
		get { return BallManager.instance; }
	}

	//初期化完了フラグ
	protected bool			m_bInitialized;

	/// <summary>
	/// 背景の初期化
	/// </summary>
	public void BG_Init()
	{
		//	背景設定
		if (m_Bg == null)
		{
			GameInfo.stBGDData a_BGData = GameInfo.instance.gameBGInfo.BGData;
			//	デフォルト背景
			if (a_BGData.type == GameInfo.eBGType.Default)
			{
				GameObject a_Obj = new GameObject();
				m_Bg = a_Obj.AddComponent<UITexture>();

				//	テクスチャをロード
				m_Bg = ResourceManager.TextureLoadAndInstantiate(a_BGData.texName);
				m_Bg.gameObject.name = a_BGData.texName;

				Destroy(a_Obj);
			}
			else
				//	オリジナル背景
				if (a_BGData.type == GameInfo.eBGType.Original)
				{
					//	背景データを読み込む
					bool a_bSuc = GameUtils.FromByteToTexture(out m_Bg, a_BGData.id, def.OriginalBGFilePath_Get(a_BGData.id));
					if (a_bSuc) m_Bg.gameObject.name = "bg_original";
				}
		}

        //	親設定
        GameUtils.AttachChild(PanelManager.instance.alignmanLow.alignCenter.gameObject, m_Bg.gameObject);
        //	サイズ設定
        GameUtils.WidgetSize_Set(m_Bg.gameObject, def.c_BGSize);
    }

	/// <summary>
	/// 攻守交替時の処理
	/// </summary>
	public void ChangeAttack()
	{
		if (m_PlayerManager.swingMan.state == SwingManager.eState.Offence)
		{
			m_PlayerManager.swingMan.state = SwingManager.eState.Deffence;
			m_CPUManager.swingMan.state = SwingManager.eState.Offence;
		}
		else
		{
			m_PlayerManager.swingMan.state = SwingManager.eState.Offence;
			m_CPUManager.swingMan.state = SwingManager.eState.Deffence;
		}
	}

    public IEnumerator GameScene_Init(BallManager.eUseType p_BallType, int p_nCPID, PrefInstantiater.Layout[] p_Prefs)
    {
        //	ＢＧＭを止める
        SoundHelper.instance.BgmStop();

        //	共通の初期化
        bool a_bWait = true;
        def.CommonInit(() =>
        {
            a_bWait = false;
        }
        );
        //	初期化が終了するまで待機
        while (a_bWait) yield return 0;

        //	バナー広告非表示
        def.AdManager.Banner_SetActive(false);

        //	背景の初期化
        BG_Init();

        //	ボール生成
        BallManager.Create(p_BallType);

        //	ＣＰＵの選手データ設定
        m_CPUManager = CPUManager.Create(p_nCPID, ChangeAttack);

        //	プレイヤーデータ生成
        m_PlayerManager = PlayerManager.Create(ChangeAttack);

        //	使用するプレハブをインスタンス化
        m_Instantiator = PrefInstantiater.Create();
        m_Instantiator.Build(p_Prefs);

        //	初期化が完了した時の処理
        def.CommonInit_Completed();
    }
}
