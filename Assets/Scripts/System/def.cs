﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class def : defSystem {

	/// <summary>
	/// 端末の標準サイズ（iPhone5）
	/// </summary>
	readonly public static			Vector2 c_ScreenSize    = new Vector2(640, 1136);

    readonly public static          Vector2 c_BGSize        = new Vector2(640, 1138);

	/// <summary>
	/// プレイヤーとＣＰＵの距離（m）
	/// </summary>
	readonly public static int		c_nDistance = 8;

	/// <summary>
	/// 試合時間
	/// </summary>
	readonly public static int		c_nGameTime = 45;	//	デフォルトは４５秒

	/// <summary>
	/// デフォルトのボールスケール
	/// </summary>
	readonly public static Vector2	c_DefaultBallScale = new Vector2(0.3f, 0.3f);

	/// <summary>
	/// オリジナル背景数
	/// </summary>
	readonly public static int		c_nOriginalBGNum	= 2;

	/// <summary>
	/// デフォルトの背景数
	/// </summary>
	readonly public static int		c_nDefaultBGNum		= 6;

	/// <summary>
	/// 全背景数
	/// </summary>
	public static int	c_nAllBGNum
	{
		get
		{
			return c_nOriginalBGNum + c_nDefaultBGNum;
		}
	}

	/// <summary>
	/// 基準となる最大加速度値（Ｇ）
	/// </summary>
	readonly public static int		c_nRefAccMax		= 4;

	/// <summary>
	/// 基準となる最低加速度値（Ｇ）
	/// </summary>
	readonly public static int		c_nRefAccMin		= -4;

	/// <summary>
	/// 加速度に掛ける値
	/// </summary>
	readonly public static int		c_nMultiAccValue	= 250;	//	デフォルトは２５０

    /// <summary>
    /// オリジナル画像が存在しない時に表示される背景
    /// </summary>
    readonly public static string   c_BG_NoImageName = "Bg/bg_0";

	/// <summary>
	/// デフォルトのパートナー
	/// </summary>
	public static GameInfo.stProfile	c_DefaultPartner
	{
		get
		{
			PlayerTable			a_Table = instance.playerTable;
			PlayerData.Param	a_Param = a_Table.Param_Get(instance.playerTable.comID);
			string				a_Name	= instance.playerTable.Name_Get(a_Param.ID);
			GameInfo.stProfile	a_Data	= new GameInfo.stProfile(a_Param.ID, a_Param.WorldRank, a_Param.WorldRank.ToString(), a_Name, a_Param.FileName);

			return	a_Data;
		}
	}

	/// <summary>
	/// ボールの最高速度
	/// </summary>
	public static int	c_nBallSpeed_Max
	{
		get
		{
			GameInfo.stProfile	a_Partner	= GameInfo.instance.partnerProfile;
			PlayerData.Param	a_Param		= instance.playerTable.Param_Get(a_Partner.id);

			if (a_Param == null)
			{
				DebugManager.Log("def.cs CPUManager.c_Param is null.");
				return	50;
			}

			int	a_nSpeed = ((int)a_Param.Power * 10) + (c_nSpeedUpCount * 2);

			return	a_nSpeed;
		}
	}

	/// <summary>
	/// スピードアップ開始時間
	/// </summary>
	public static int	c_nSpeedUpStartTime
	{
		get
		{
			GameInfo.stProfile	a_Partner	= GameInfo.instance.partnerProfile;
			PlayerData.Param	a_Param		= instance.playerTable.Param_Get(a_Partner.id);

			if (a_Param == null)	return c_nGameTime;

			//	スピードアップタイム算出（スタミナ値が１増える毎にスピードアップタイムが２秒早くなる）
			int a_nTime = c_nGameTime - (((int)a_Param.Stamina - 1) * 3);

			return a_nTime;
		}
	}

	/// <summary>
	/// スピードアップを開始するか
	/// </summary>
	public static bool c_bSpeedUpStart
	{
		get
		{
			//	経過時間
			float   a_fElapsedTime = c_nGameTime - Header.instance.currentTime;
			//	経過時間がスピードアップ開始時間を越えていればtrue
			return (a_fElapsedTime >= c_nSpeedUpStartTime) ? true : false;
		}
	}

	/// <summary>
	/// スピードアップカウンタ
	/// </summary>
	public static int	c_nSpeedUpCount;

	/// <summary>
	/// 最高速アップ処理
	/// </summary>
	public void SpeedUp_Exec()
	{
		if (!c_bSpeedUpStart) return;
		c_nSpeedUpCount++;
	}

	/// <summary>
	/// ボールの最低速度（最高速の半分）
	/// </summary>
	public static int	c_nBallSpeed_Min
	{
		get
		{
			return	c_nBallSpeed_Max / 2;
		}
	}

	/// <summary>
	/// ボールが到達してから判定を行うまでのプレイヤーに与えられる猶予時間
	/// </summary>
	readonly public static float	c_nPlayerGracetime = 1.2f;

	/// <summary>
	/// ラケットエディタで撮影した画像ファイル名
	/// </summary>
//	readonly public static string	c_OriginalBGFileName = "original.png";

	/// <summary>
	/// 最大音量
	/// </summary>
	readonly public static float	c_fMaxVolume = 1;

	/// <summary>
	/// 最小音量
	/// </summary>
	readonly public static float	c_fMInVolume = 0;

	/// <summary>
	/// 設定言語による画像の命名規則
	/// </summary>
	public static string			c_TextureLanguageStr
	{
		get
		{
			switch(GameInfo.instance.language)
			{
				case GameInfo.eLanguage.JPN:
				{
					return	"_j";
				}

				case GameInfo.eLanguage.ENG:
				{
					return "_e";
				}

				case GameInfo.eLanguage.PRT:
				{
					return "_p";
				}
			}

			return	"";
		}
	}

	/// <summary>
	/// 国旗のアスペクト比
	/// </summary>
	public static Dictionary<string, Vector2>	c_NationalFlag_Aspect = new Dictionary<string, Vector2>()
	{
		{"日本",		new Vector2(3, 2)},
		{"ブラジル",	new Vector2(10, 7)},
		{"スペイン",	new Vector2(3, 2)}
	};

	/// <summary>
	/// 国の略称を取得
	/// </summary>
	public static Dictionary<string, string> c_CountryAbbreviation = new Dictionary<string, string>()
	{
		{"日本",		"JPN"},
		{"ブラジル",	"BRA"},
		{"スペイン",	"ESP"},
        {"アメリカ",    "USA"}
    };


	/// <summary>
	/// ラケットエディタで撮影した画像ファイルの保存先のパス名
	/// </summary>
	public static string	OriginalBGFilePath_Get(int p_nID)
	{
		switch (Application.platform)
		{
			case RuntimePlatform.Android:
			case RuntimePlatform.IPhonePlayer:
				{
					return Application.persistentDataPath + "/" + OriginalBGFileName_Get(p_nID);
				}

			default:
				{
					return	OriginalBGFileName_Get(p_nID);
				}
		}
	}

    /// <summary>
    /// 背景を生成する
    /// </summary>
    /// <param name="p_FileName"></param>
    /// <param name="p_ObjName"></param>
    /// <param name="p_Parent"></param>
    public static void  BGCreate(string p_FileName, string p_ObjName, GameObject p_Parent)
    {
        //	背景の読み込み
        UITexture   a_Bg    = ResourceManager.TextureLoadAndInstantiate(p_FileName);
        a_Bg.name = p_ObjName;
        GameUtils.AttachChild(p_Parent, a_Bg.gameObject);

        GameUtils.WidgetSize_Set(a_Bg.gameObject, c_BGSize);
    }

	/// <summary>
	/// ラケットエディタで撮影した画像ファイル名
	/// </summary>
	/// <param name="p_nID"></param>
	/// <returns></returns>
	public static string OriginalBGFileName_Get(int p_nID)
	{
		return string.Format("original{0}.png", p_nID);
	}

	/// <summary>
	/// 選手の人数（ペア数（ＣＯＭを除く））
	/// </summary>
	public static int	c_nPlayerNum
	{
		get
		{
			if (instance == null || instance.playerTable == null) return 0;

			int a_nNum		= 0;
			int	a_nCount	= instance.playerTable.paramList.Count;
			for (int i = 0 ; i < a_nCount ; i+=2)
			{
				PlayerData.Param	a_Param = instance.playerTable.paramList[i];
				if (!instance.playerTable.isCOM(a_Param))
				{
					a_nNum++;
				}
			}

			return	a_nNum;
		}
	}

	/// <summary>
	/// 選手の最高ランク
	/// </summary>
	public static int	c_nPlayerRankMax = 1;

    /// <summary>
    /// 選手の最低ランク
    /// </summary>
    public static int c_nPlayerRankMin
    {
        get { return c_nPlayerNum + 1; }
    }


	static def s_Instance;
	public static def	instance
	{
		get { return s_Instance; }
	}

	/// <summary>
	/// ショットデータ
	/// </summary>
	public ShotTable	shotTable
	{
		get { return m_ShotTable; }
	}
	ShotTable	m_ShotTable;

	/// <summary>
	/// 選手データ（日本）
	/// </summary>
	public PlayerTable	playerTable
	{
		get { return m_PlayerTable; }
	}
	PlayerTable	m_PlayerTable;

	/// <summary>
	/// 判定データ
	/// </summary>
	public Shot_AdjustTable	adjustTable
	{
		get { return m_AdjustTable; }
	}
	Shot_AdjustTable m_AdjustTable;

	/// <summary>
	/// その他のパラメータデータ
	/// </summary>
	public EtcParamTable	etcTable
	{
		get { return m_EtcTable; }
	}
	EtcParamTable	m_EtcTable;

	/// <summary>
	/// 攻守交替時に必要なデータ
	/// </summary>
	public AtkChangeTable	atkChangeTable
	{
		get { return m_AtkChangeTable; }
	}
	AtkChangeTable	m_AtkChangeTable;

    /// <summary>
    /// フレスコボールの説明に必要なデータ
    /// </summary>
    public DescriptionTable descriptionTable
    {
        get { return m_DescriptionTable; }
    }
    DescriptionTable    m_DescriptionTable;

	/// <summary>
	/// スイングスピード（-1000～1000）
	/// </summary>
	public static Vector3 swingSpeed
	{
		get
		{
			Vector3	a_Speed = (Input.acceleration * c_nMultiAccValue) * GameInfo.instance.sensitivityRev;
			return	a_Speed;
		}
	}

	/// <summary>
	/// 広告マネージャー
	/// </summary>
	public static GoogleMobileAdsDemoScript	AdManager
	{
		get
		{
			return	GoogleMobileAdsDemoScript.instance;
		}
	}

	/// <summary>
	/// 全シーンの共通処理
	/// </summary>
	public static void	CommonInit(System.Action p_CallBack)
	{
		if (s_Instance != null)
		{
			if (PanelManager.instance.dontDestroy) PanelManager.instance.Clean();
			if (p_CallBack != null)	p_CallBack();
			return;
		}

		def	a_This			= new GameObject("def").AddComponent<def>();
		a_This.hideFlags	= HideFlags.HideInHierarchy;	//	Hierarchyには表示しない
		s_Instance			= a_This;

		//	削除しない
		DontDestroyOnLoad(a_This.gameObject);

		Application.targetFrameRate = c_FPS;    //	デフォルトは６０
		Time.timeScale				= 1;		//	デフォルトは１

		// 言語設定
		const string	JP = "Japanese";
		const string	EN = "English";
		if (Application.systemLanguage == SystemLanguage.Japanese)
		{
			Localization.language = JP;
		}
		else
		{
			Localization.language = EN;
		}

		s_Instance.StartCoroutine("CommonInit_Exec", p_CallBack);
	}

	IEnumerator	CommonInit_Exec(System.Action p_Callback)
	{
		//	サウンド関連生成
		SoundManager.Create();
		SoundHelper.Create();

		//	サウンドシステムの準備が出来るまで待機
		while (!SoundHelper.instance.isReady)
		{
			yield return 0;
		}

		// UIRoot (PanelManager)
		GameObject	a_UIRoot = GameObject.FindGameObjectWithTag("uiRoot");
		a_UIRoot = ResourceManager.PrefabLoadAndInstantiate("Common/UIRootTemplate", Vector3.zero, null);

		//	uiRootの準備が出来るまで待機
		while(!PanelManager.isReady)
		{
			yield return 0;
		}

		//	共通テーブル作成
		CreateTable();

		//	プレイヤー情報
		PlayerInfo.Create();

		// ゲーム情報
		GameInfo.Create();

		//	リソースマネージャー
		ResourceManager.Create();

		//	デバッグ管理
		DebugManager.Create();

		//	アドモブマネージャー生成
		GoogleMobileAdsDemoScript.Create();

		//	タッチエフェクト
		TouchEffect.Create();

		//	タッチ制御
		TouchDisable.Create();

		if (p_Callback != null)	p_Callback();
	}

	/// <summary>
	/// シーン共通で行う初期化が完了した時の処理
	/// </summary>
	public static void	CommonInit_Completed()
	{
		//	タッチ制限を解除
		TouchDisable.instance.SetDisable(false);
		//	フェードイン
		PanelManager.instance.fade.In(0, Color.clear);
	}

	/// <summary>
	/// 共通テーブルを作成する
	/// </summary>
	public void	CreateTable()
	{
		//	ショットデータ
		m_ShotTable         = ShotTable.Create();

		//	プレイヤー（日本）データ
		m_PlayerTable       = PlayerTable.Create();

		//	判定データ
		m_AdjustTable       = Shot_AdjustTable.Create();

		//	その他データ
		m_EtcTable          = EtcParamTable.Create();

		//	攻守交替データ
		m_AtkChangeTable    = AtkChangeTable.Create();

        //  説明データ
        m_DescriptionTable  = DescriptionTable.Create();
	}

	Scene	m_PastScene;
	/// <summary>
	/// １つ前のシーン
	/// </summary>
	public Scene pastScene
	{
		get { return m_PastScene; }
	}

	Scene	m_CurrentScene;
	/// <summary>
	/// 現在のシーン
	/// </summary>
	public Scene	currentScene
	{
		get { return m_CurrentScene; }	
	}

	/// <summary>
	/// 指定したシーンに遷移する（一時的なシーンを含む）
	/// </summary>
	/// <param name="p_Scene"></param>
	/// <param name="p_bFade"></param>
	/// <returns></returns>
	public IEnumerator	SceneMove(Scene p_Scene, bool p_bFade, bool p_bShowInterstitial = false)
	{
		//	タッチを制限する
		TouchDisable.instance.SetDisable(true);

        //  インタースティシャル広告のリクエスト
#if !UNITY_EDITOR
		bool	a_bClosed = false;
        //	インタースティシャル広告のコールバック設定
        if (p_bShowInterstitial)
        {
            //  広告を閉じた時の処理
            AdManager.interstitial.AdClosed +=
                ((sender, args) =>
                    {
                        a_bClosed = true;
                    }
            );
		}
#endif

		float	a_fFadeTime = 0.5f;
		//	フェードアウト
		if(p_bFade)
		{
			PanelManager.instance.fade.Out(a_fFadeTime, Color.black, true);
		}

		//	フェードが終了するまで待機
		while (PanelManager.instance.fade.isFade)	yield return 0;

		//	タッチエフェクト削除
		TouchEffect.instance.AllDestroy();

		// SEを止める
		SoundHelper.instance.SeStop();

        //  広告の表示とウェイト
#if !UNITY_EDITOR
        if(p_bShowInterstitial)
		{
			//	インタースティシャル広告が表示されるまで待機
			yield return StartCoroutine(AdManager.ShowInterstitial_Exec());

			//	インタースティシャル広告が閉じられるまで待機
			while (!a_bClosed)
			{
				yield return 0;
			}
		}
#endif

        //	一時的なシーンをはさむ
        SceneManager.LoadScene(c_SceneFileName[(int)Scene.TEMPORARY], LoadSceneMode.Additive);

		//	現在のシーンデータ
		UnityEngine.SceneManagement.Scene a_CurrentSceneData	= SceneManager.GetSceneByName(c_SceneFileName[(int)m_CurrentScene]);
		//	次のシーンデータ
		UnityEngine.SceneManagement.Scene a_NextSceneData		= SceneManager.GetSceneByName(c_SceneFileName[(int)Scene.TEMPORARY]);

		//	遷移する前に現在のシーンを１つ前のシーンとして保存
		m_PastScene = m_CurrentScene;

		//	遷移先のシーンに更新
		m_CurrentScene = p_Scene;

		//	シーンが変更され、次のシーンデータのロードが完了するまで待機
		while (!a_CurrentSceneData.isDirty && !a_NextSceneData.isLoaded)
		{
			DebugManager.Log("Scene loading...");
			yield return 0;
		}

		//	シーンのロード
		SceneManager.LoadScene(c_SceneFileName[(int)p_Scene], LoadSceneMode.Single);

		//	現在のシーンデータ
		a_CurrentSceneData	= SceneManager.GetSceneByName(c_SceneFileName[(int)m_CurrentScene]);
		//	次のシーンデータ
		a_NextSceneData		= SceneManager.GetSceneByName(c_SceneFileName[(int)p_Scene]);

		//	遷移先のシーンに更新
		m_CurrentScene = p_Scene;

		//	シーンが変更され、次のシーンデータのロードが完了するまで待機
		while(!a_CurrentSceneData.isDirty && !a_NextSceneData.isLoaded)
		{
			DebugManager.Log("Scene loading...");
			yield return 0;
		}

		yield return 0;
	}

	/// <summary>
	/// 言語環境による設定
	/// </summary>
	public static void Language_Set()
	{
		if (GameInfo.instance == null)	return;
		if (GameInfo.instance.language != GameInfo.eLanguage.NONE || 
			GameInfo.instance.language != GameInfo.eLanguage.MAX)
		{
			return;
		}

		//	言語設定
		SystemLanguage	a_Language = Application.systemLanguage;

		//	日本語
		if(a_Language == SystemLanguage.Japanese)
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.JPN);
		}
		else
		//	ポルトガル（ブラジル）
		if (a_Language == SystemLanguage.Portuguese)
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.PRT);
		}
		//	他は全て英語
		else
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.ENG);
		}
	}


	public void	QuitScene(bool p_bShowInterstitial)
	{
		StartCoroutine(QuitScene_Exec(p_bShowInterstitial));
	}

	/// <summary>
	/// ゲームを終了する時の遷移処理
	/// </summary>
	/// <returns></returns>
	IEnumerator	QuitScene_Exec(bool p_bShowInterstitial)
	{
		Time.timeScale = 1;

		if (GameInfo.instance.gameMode == GameInfo.eGameMode.FREE)
		{
			//	タイトル
			StartCoroutine(SceneMove(Scene.TITLE, true, p_bShowInterstitial));
		}
		else
		{
			//	選手選択画面に遷移（インタースティシャル広告表示）
			StartCoroutine(SceneMove(Scene.SELECT, true, p_bShowInterstitial));
		}

		yield return 0;
	}

	void LateUpdate()
	{
		UIButtonTween.clearOnClick();
	}
}
