﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameInfo{

	[System.NonSerializedAttribute]
	public const string	eTAG = "GameInfo";

	[System.NonSerializedAttribute]
	static GameInfo s_Instance;
	/// <summary>
	/// シングルトンインスタンス
	/// </summary>
	public static GameInfo	instance
	{
		get{ return	s_Instance; }
	}

	public static void	Create()
	{
		if (s_Instance == null)
		{
			s_Instance = new GameInfo();
			s_Instance.Load();
		}
	}

	/// <summary>
	/// 言語
	/// </summary>
	public enum eLanguage : int
	{
		NONE = -1,

		JPN,	//	日本語
		ENG,	//	英語
		PRT,	//	ポルトガル

		MAX
	}

	eLanguage	m_Language;
	/// <summary>
	/// 設定言語
	/// </summary>
	public eLanguage	language
	{
		get { return m_Language; }
	}

	/// <summary>
	/// 設定言語を切り替える
	/// </summary>
	public void	Language_Add()
	{
		m_Language++;
		if (m_Language == eLanguage.MAX)	m_Language = eLanguage.JPN;

		Save();
	}

	/// <summary>
	/// 言語の設定
	/// </summary>
	public void	Language_Set(eLanguage p_Language)
	{
		m_Language = p_Language;
		Save();
	}

	/// <summary>
	/// 振り始めのタイプ
	/// </summary>
	public enum eBeginSwing
	{
		TOUCH,	//	タッチが振り始め
		AUTO,	//	自動

		MAX
	}

	/// <summary>
	/// 背景タイプ
	/// </summary>
	public enum eBGType : int
	{
		Default,	//	デフォルト
		Original,	//	オリジナル

		MAX
	}

	/// <summary>
	/// ゲームのモード
	/// </summary>
	public enum eGameMode : int
	{
		TOURNAMENT,		//	トーナメントモード
		FREE			//	フリーモード
	}

	[System.NonSerializedAttribute]
	eGameMode m_GameMode;
	/// <summary>
	/// 選択しているゲームモード
	/// </summary>
	public eGameMode	gameMode
	{
		get { return m_GameMode; }
	}

    /// <summary>
    /// ゲームモードを設定する
    /// </summary>
    /// <param name="p_GameMode"></param>
    public void	GameMode_Set(eGameMode p_GameMode)
	{
		m_GameMode = p_GameMode;
		Save();
	}

	[System.NonSerializedAttribute]
	int m_nSelectPlayerID;
	/// <summary>
	/// ＣＰＵとなる選手のＩＤ
	/// </summary>
	public int selectPlayerID
	{
		get { return m_nSelectPlayerID; }
	}

	/// <summary>
	/// ＣＰＵとなる選手のパラメータを設定する
	/// </summary>
	public void SelectPlayerID_Set(int p_nID)
	{
		m_nSelectPlayerID = p_nID;
	}

	/// <summary>
	///	バトルの結果報告用データ
	/// </summary>
	public struct stBattleResult
	{
		public List<stProfile>	playerProfileList;
		public List<stProfile>	userProfileList;
		public int				playerScore;
		public int				userScore;
		public bool				enableResult;

		public bool	isUserWin
		{
			get
			{
				return	(userScore > playerScore) ? true : false;
			}
		}

		/// <summary>
		/// 選手情報を登録する
		/// </summary>
		/// <param name="p_PlayerProfileList"></param>
		/// <param name="p_UserProfileList"></param>
		/// <param name="p_nPlayerScore"></param>
		/// <param name="p_nUserScore"></param>
		public void Profile_Set(List<stProfile> p_PlayerProfileList, List<stProfile> p_UserProfileList)
		{
			playerProfileList	= p_PlayerProfileList;
			userProfileList		= p_UserProfileList;

			enableResult = false;
		}

		/// <summary>
		/// ユーザー側の選手情報を登録する
		/// </summary>
		/// <param name="p_UserProfileList"></param>
		public void UserProfile_Set(List<stProfile> p_UserProfileList)
		{
			userProfileList = p_UserProfileList;
		}

		/// <summary>
		/// 結果（スコア）情報をセットする
		/// </summary>
		/// <param name="p_nPlayerScore"></param>
		/// <param name="p_nUserScore"></param>
		public void ScoreSet(int p_nPlayerScore, int p_nUserScore)
		{
			playerScore		= p_nPlayerScore;
			userScore		= p_nUserScore;

			enableResult	= true;
		}
	}

	/// <summary>
	/// 結果用データ
	/// </summary>
	[System.NonSerializedAttribute]
	public stBattleResult	resultData;

	/// <summary>
	/// プロフィール情報
	/// </summary>
	[System.Serializable]
	public struct stProfile
	{
		public int		id;
		public int		worldRank;
		public string	rank;
		public string	name;
		public string	fileName;

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="p_Rank"></param>
		/// <param name="p_Name"></param>
		public stProfile(int p_nID, int p_nWorldRank, string p_Rank, string p_Name, string p_FileName)
		{
			id			= p_nID;
			worldRank	= p_nWorldRank;
			rank		= p_Rank;
			name		= p_Name;
			fileName	= p_FileName;
		}

		/// <summary>
		/// データが存在するか
		/// </summary>
		public bool	isEnableData
		{
			get
			{
				if (!string.IsNullOrEmpty(rank) && !string.IsNullOrEmpty(name))
				{
					return true;
				}

				return false;
			}
		}

		/// <summary>
		/// 比較
		/// </summary>
		/// <param name="p_Target"></param>
		/// <returns></returns>
		public bool	Compare(stProfile p_Target)
		{
			if(id == p_Target.id && name == p_Target.name)
			{
				return true;
			}

			return false;
		}
	}

	/// <summary>
	/// 背景データ
	/// </summary>
	[System.Serializable]
	public struct stBGDData
	{
		public int		id;
		public eBGType	type;
		public string	texName;

		public stBGDData(int p_nID, eBGType p_Type, string p_TexName)
		{
			id		= p_nID;
			type	= p_Type;
			texName = p_TexName;
		}
	}

	/// <summary>
	/// ゲーム画面に表示する背景用
	/// </summary>
	[System.Serializable]
	public class GameBGInfo
	{
		stBGDData	m_BGData;
		/// <summary>
		/// 背景データ
		/// </summary>
		public stBGDData	BGData
		{
			get { return m_BGData; }
		}
		
		/// <summary>
		/// 保存
		/// </summary>
		void	Save()
		{
			GameInfo.instance.Save();
		}

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public GameBGInfo(int p_nID, eBGType p_Type, string p_TexName)
		{
			m_BGData = new stBGDData(p_nID, p_Type,  p_TexName);
		}

		/// <summary>
		/// 設定
		/// </summary>
		/// <param name="p_Type"></param>
		/// <param name="p_TexName"></param>
		public void	Set(stBGDData p_BGData)
		{
			m_BGData = p_BGData;
			Save();
		}
	}

	GameBGInfo	m_GameBGInfo;
	/// <summary>
	/// ゲーム画面で表示する背景情報
	/// </summary>
	public GameBGInfo	gameBGInfo
	{
		get 
		{
			if (m_GameBGInfo == null)	m_GameBGInfo = new GameBGInfo(def.c_nOriginalBGNum, eBGType.Default, "Bg/bg_1");
			return	m_GameBGInfo; 
		}
		set { m_GameBGInfo = value; }
	}

	stProfile m_PartnerProfile;
	/// <summary>
	/// パートナーに設定している選手データ
	/// </summary>
	public stProfile	partnerProfile
	{
		get 
		{ 
			//	パートナーデータが存在しない時はデフォルト設定
			if(!m_PartnerProfile.isEnableData)
			{
				m_PartnerProfile = def.c_DefaultPartner;
			}
			return m_PartnerProfile; 
		}
	}

	/// <summary>
	/// パートナー選手の設定を行う
	/// </summary>
	public void	PartnerProfile_Set(stProfile p_Profile)
	{
		m_PartnerProfile = p_Profile;
		Save();
	}

	float	m_fSensitivityRev;
	/// <summary>
	/// スイングの感度補正（０～１）
	/// </summary>
	public float	sensitivityRev
	{
		get 
		{ 
			if(m_fSensitivityRev == 0)	m_fSensitivityRev = 1;
			return	m_fSensitivityRev;
		}

		set 
		{
			m_fSensitivityRev = 1f + (value * 2);
			Save();
		}
	}

    public bool isFreeMode
    {
        get
        {
            return (gameMode == eGameMode.FREE);
        }
    }

    public bool isChallengeMode
    {
        get
        {
            return (gameMode == eGameMode.TOURNAMENT);
        }
    }

    int m_nBootCount;
	/// <summary>
	/// 起動回数
	/// </summary>
	public int	bootCount
	{
		get { return m_nBootCount; }
	}

	/// <summary>
	/// 起動回数を増やす
	/// </summary>
	public void	BootCount_Add()
	{
//		m_nBootCount = 0;	//	起動時に言語選択ダイアログを表示する
		m_nBootCount++;
		Save();
	}

	/// <summary>
	/// 初回起動か
	/// </summary>
	public bool isFirstBoot { get { return	(m_nBootCount == 1) ? true : false; } }

	/// <summary>
	/// ロード
	/// </summary>
	public void	Load()
	{
		string	a_Serialized = PlayerPrefs.GetString(eTAG);
//        a_Serialized = "";    //  データを初期化
		if (!string.IsNullOrEmpty(a_Serialized))
		{
			s_Instance = Deserialize(a_Serialized);
		}
		else
		{
			// 初回起動時
			s_Instance = new GameInfo();
			Save();
		}
	}

	/// <summary>
	/// 自身をシリアル化して返す
	/// </summary>
	/// <returns></returns>
	public string Serialize()
	{
		string	a_SerializedStr = EasySerializer.SerializeObjectToString(this);
		return	a_SerializedStr;
	}

	/// <summary>
	/// セーブデータを戻す
	/// </summary>
	/// <param name="p_SerializedStr"></param>
	/// <returns></returns>
	static GameInfo	Deserialize(string p_SerializedStr)
	{
		GameInfo	a_GameInfo = (GameInfo)EasySerializer.DeserializeObjectFromString(p_SerializedStr);
		return		a_GameInfo;
	}

	/// <summary>
	/// 自身をセーブする
	/// </summary>
	public void	Save()
	{
		if(s_Instance != null)
		{
			//	自身をシリアル化して取得
			string	a_Serialize = s_Instance.Serialize();
			// GameInfoの保存
			PlayerPrefs.SetString(eTAG, a_Serialize);
		}
	}
}
