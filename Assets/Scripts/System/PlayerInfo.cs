﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class PlayerInfo{

	public const string	eTAG = "PlayerInfo";

	/// <summary>
	/// 保存するキー
	/// </summary>
	public enum eKey : int
	{
		UserName,			//	ユーザー名
		Language,			//	言語（日・英）
		Dominant_Hand,		//	利き手（左・右）
		UserRank,			//	ユーザーランク
		ArrivalTopRank,		//	１度でもランクが１位になったことがあるか

		PlayTournament,		//	トーナメントモードをプレイしたことがあるか
		IsRank1,			//	ランク１位を取ったことがあるか

		Max
	}

	/// <summary>
	/// 言語
	/// </summary>
	public enum eLanguage : int
	{
		JAPANESE,	//	日本語
		ENGLISH		//	英語
	}

	/// <summary>
	/// 利き手
	/// </summary>
	public enum eHand : int
	{
		LEFT,		//	左
		RIGHT,		//	右
		
		MAX
	}

	/// <summary>
	/// プレイヤー情報データの配列
	/// </summary>
	public string[]	m_Info;

	[System.NonSerializedAttribute]
	static PlayerInfo s_Instance;
	/// <summary>
	/// シングルトンインスタンス
	/// </summary>
	public static PlayerInfo instance
	{
		get { return s_Instance; }
	}

	public static void Create()
	{
		if (s_Instance == null)
		{
			s_Instance = new PlayerInfo();
			s_Instance.Load();
		}
	}

	/// <summary>
	///	int型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public int getInt(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return 0;

		int a_nValue = 0;
		int.TryParse(m_Info[(int)p_Key], out a_nValue);
		return a_nValue;
	}

	/// <summary>
	/// int型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setInt(eKey p_Key, int p_nValue)
	{
		m_Info[(int)p_Key] = p_nValue.ToString();
		Save();
	}

	/// <summary>
	///	float型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public float getFloat(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return 0;

		float	a_fValue = 0;
		float.TryParse(m_Info[(int)p_Key], out a_fValue);
		return a_fValue;
	}

	/// <summary>
	/// float型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setFloat(eKey p_Key, float p_Value)
	{
		m_Info[(int)p_Key] = p_Value.ToString();
		Save();
	}

	/// <summary>
	/// string型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public string getStr(eKey p_Key)
	{
		return (string)m_Info[(int)p_Key];
	}

	/// <summary>
	/// string型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setStr(eKey p_Key, string p_Value)
	{
		m_Info[(int)p_Key] = p_Value;
		Save();
	}

	/// <summary>
	///	bool型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public bool getBool(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return false;

		bool	a_bValue = false;
		bool.TryParse(m_Info[(int)p_Key], out a_bValue);
		return	a_bValue;
	}

	/// <summary>
	/// float型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setBool(eKey p_Key, bool p_bValue)
	{
		m_Info[(int)p_Key] = p_bValue.ToString();
		Save();
	}

	/// <summary>
	/// ロード
	/// </summary>
	public void Load()
	{
		string	a_Serialized = PlayerPrefs.GetString(eTAG);
//        a_Serialized = "";    //  データを初期化
		if (!string.IsNullOrEmpty(a_Serialized))
		{
			s_Instance = Deserialize(a_Serialized);

			// データが拡張されたら再生成する
			if (s_Instance.m_Info.Length < (int)eKey.Max)
			{
				string[]	a_KeepPref = s_Instance.m_Info;
				s_Instance.m_Info = new string[(int)(int)eKey.Max];
				for (int i = 0; i < a_KeepPref.Length; i++)
				{
					s_Instance.m_Info[i] = a_KeepPref[i];
				}
			}
		}
		else
		{
			// 初回起動時
			s_Instance = new PlayerInfo();
			s_Instance.DefaultSet();
			Save();
		}
	}

	/// <summary>
	/// デフォルト設定
	/// </summary>
	void	DefaultSet()
	{
		m_Info = new string[(int)eKey.Max];
		for (int i = 0; i < (int)eKey.Max; i++)
		{
			m_Info[i] = "";
		}
	}

	/// <summary>
	/// セーブ
	/// </summary>
	static void Save()
	{
		if (s_Instance != null)
		{
			string	a_SerializedStr = EasySerializer.SerializeObjectToString(s_Instance);
			PlayerPrefs.SetString(eTAG, a_SerializedStr);
		}
	}

	/// <summary>
	/// 自身をシリアル化して返す
	/// </summary>
	/// <returns></returns>
	public string Serialize()
	{
		string a_SerializedStr = EasySerializer.SerializeObjectToString(this);
		return a_SerializedStr;
	}

	/// <summary>
	/// セーブデータを戻す
	/// </summary>
	/// <param name="p_SerializedStr"></param>
	/// <returns></returns>
	static PlayerInfo Deserialize(string p_SerializedStr)
	{
		PlayerInfo	a_PlayerInfo = (PlayerInfo)EasySerializer.DeserializeObjectFromString(p_SerializedStr);
		return	a_PlayerInfo;
	}

	/// <summary>
	/// ユーザー名
	/// </summary>
	public string	userName
	{
		get { return getStr(eKey.UserName); }
		set { setStr(eKey.UserName, value); }
	}

	/// <summary>
	/// 言語
	/// </summary>
	public eLanguage language
	{
		get { return (eLanguage)getInt(eKey.Language); }
		set 
		{
			//	列挙に定義されている値か調べる
			if(Enum.IsDefined(typeof(eLanguage), value))
			{
				setInt(eKey.Language, (int)value); 
			}
			else
			{
				DebugManager.Log(string.Format("Not Defined {0} to eLanguage", value));
			}
		}
	}

	/// <summary>
	/// 利き手
	/// </summary>
	public eHand hand
	{
		get { return (eHand)getInt(eKey.Dominant_Hand); }
		set 
		{
			//	列挙に定義されている値か調べる
			if(Enum.IsDefined(typeof(eHand), value))
			{
				setInt(eKey.Dominant_Hand, (int)value); 
			}
			else
			{
				DebugManager.Log(string.Format("Not Defined {0} to eDominant_Hand", value));
			}
		}
	}

	/// <summary>
	/// ユーザーランク
	/// </summary>
	public int	userRank
	{
		get 
		{
			int a_nUserRank = getInt(eKey.UserRank);
			//	設定されていない場合は最低ランクに設定
			if (a_nUserRank <= 0)
			{
				a_nUserRank = def.c_nPlayerRankMin;
				setInt(eKey.UserRank, a_nUserRank);
			}

			//	ランク１位を取った事を保存
			if(a_nUserRank == 1 && !isRank1)
			{
				isRank1 = true;
			}

			return a_nUserRank; 
		}
		set { setInt(eKey.UserRank, value); }
	}

	/// <summary>
	/// 一度でもランク１位を取ったことがあるか
	/// </summary>
	public bool	isRank1
	{
		get
        {
            return  (bool)getBool(eKey.IsRank1);
        }
		set { setBool(eKey.IsRank1, value); }
	}
}
