﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundHelper : MonoBehaviour
{

	//
	// Change by app
	//
	public enum eNo : int
	{
		Dummy,

		bgm_goal_brazil,

		jgl_clear,

		se_shot_weak,
		se_shot_normal1,
		se_shot_hard1,
		se_whistle_start,

		se_shot_fore,
		se_shot_dust,
		se_shot_fook,
		se_shot_back,

		se_grip_s,
		se_grip_b,

		se_voice_oh,
		se_miss,

		se_hmlogo,
		se_set,
		se_voice1,
		se_voice2,

		se_cpu_atk,
		se_cpu_def,

		se_shutter,

		se_whistle_end,

		se_change,
		se_gameend,

		se_hmbutton,
		se_btnok,

		bgm_title,

		se_change2,
		se_input,
		se_bastreturn,

		se_shot_fore_poor0,
		se_shot_fore_poor1,
		se_shot_fore_poor2,

		se_shot_fore_good0,
		se_shot_fore_good1,
		se_shot_fore_good2,
		se_shot_fore_good3,

		se_shot_fore_perfect0,
		se_fu,

		se_voice_dust,
		se_voice_fook,

        bgm_sea,

        se_combo1,
        se_combo2,
        se_combo3,
    };

	//
	// Change by app
	//
	static stSoundTable[] c_SoundTable = 
	{
		//                eNo					fileName				            track, priority, comment
		new stSoundTable( eNo.Dummy,			null, 					            0, 0,	"ダミー"		),

//	BGM
		new stSoundTable( eNo.bgm_goal_brazil,	"bgm_goal_brazil",		            0, 0,	"タイトル"		),

//	JGL
		new stSoundTable( eNo.jgl_clear,		"jgl_clear", 			            2, 0,	"クリアした瞬間のジングル"),
		
//	SE
		new stSoundTable( eNo.se_shot_weak,		"se_shot_weak", 		            1, 0,	"ボールが当たった時（弱）"),
		new stSoundTable( eNo.se_shot_normal1,	"se_shot_normal1", 		            1, 0,	"ボールが当たった時（中）"),
		new stSoundTable( eNo.se_shot_hard1,	"se_shot_hard1", 		            1, 0,	"ボールが当たった時（強）"),
		new stSoundTable( eNo.se_whistle_start,	"se_whistle_start", 	            1, 0,	"プレイヤーのサーブ開始時"),

		new stSoundTable( eNo.se_shot_fore,		"se_shot_foret", 		            1, 0,	"ダストで打った時の音"),		//	使用しない
		new stSoundTable( eNo.se_shot_dust,		"se_shot_dust", 		            1, 0,	"ダストで打った時の音"),
		new stSoundTable( eNo.se_shot_fook,		"se_shot_fook", 		            1, 0,	"フックで打った時の音"),
		new stSoundTable( eNo.se_shot_back,		"se_shot_back", 		            1, 0,	"バックで打った時の音"),

		new stSoundTable( eNo.se_grip_s,		"se_grip_s", 			            1, 0,	"弱アタック"),
		new stSoundTable( eNo.se_grip_b,		"se_grip_b", 			            1, 0,	"強アタック"),

		new stSoundTable( eNo.se_voice1,		"se_voice1", 			            1, 0,	""),
		new stSoundTable( eNo.se_voice2,		"se_voice2", 			            1, 0,	""),
		new stSoundTable( eNo.se_voice_oh,		"se_voice_oh", 			            1, 0,	""),
		new stSoundTable( eNo.se_miss,			"se_miss", 				            1, 0,	"プレイヤーがミスした時"),

		new stSoundTable( eNo.se_hmlogo,		"se_hmlogo", 			            1, 0,	"起動時"),

		new stSoundTable( eNo.se_set,			"se_set", 				            1, 0,	"振り始めが開始した時"),

		new stSoundTable( eNo.se_cpu_atk,		"se_cpu_atk", 			            1, 0,	"ＣＰＵがディフェンスからオフェンスに切り替える時"),
		new stSoundTable( eNo.se_cpu_def,		"se_cpu_def", 			            1, 0,	"ＣＰＵがディフェンスからオフェンスに切り替える時"),

		new stSoundTable( eNo.se_shutter,		"se_shutter", 			            1, 0,	"撮影時の音"),
		new stSoundTable( eNo.se_whistle_end,	"se_whistle_end", 		            1, 0,	"試合時間終了時"),

		new stSoundTable( eNo.se_change,		"se_change", 			            1, 0,	"ＣＰＵがディフェンスからオフェンスに切り替える時"),
		new stSoundTable( eNo.se_gameend,		"se_gameend", 			            1, 1,	"ゲーム終了時に鳴らす"),

		new stSoundTable( eNo.se_hmbutton,		"se_hmbutton", 			            1, 0,	"ＨＭボタン"),
		new stSoundTable( eNo.se_btnok,			"se_btnok", 			            1, 0,	"ボタンを押した時"),

		new stSoundTable( eNo.bgm_title,		"bgm_title", 			            1, 0,	"タイトル画面"),
		new stSoundTable( eNo.se_change2,		"se_change2", 			            1, 0,	"ＣＰＵがオフェンスからディフェンスに切り替える時"),
		new stSoundTable( eNo.se_input,			"se_input", 			            1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_bastreturn,	"se_bestreturn", 		            1, 0,	"攻守交替時の返しがベストの時"),

		new stSoundTable( eNo.se_shot_fore_poor0,		"se_shot_fore_poor0", 		1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_shot_fore_poor1,		"se_shot_fore_poor1", 		1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_shot_fore_poor2,		"se_shot_fore_poor2", 		1, 0,	"攻守交替時の返しがベストの時"),

		new stSoundTable( eNo.se_shot_fore_good0,		"se_shot_fore_good0", 		1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_shot_fore_good1,		"se_shot_fore_good1", 		1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_shot_fore_good2,		"se_shot_fore_good2", 		1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_shot_fore_good3,		"se_shot_fore_good3", 		1, 0,	"攻守交替時の返しがベストの時"),

		new stSoundTable( eNo.se_shot_fore_perfect0,	"se_shot_fore_perfect0", 	1, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_fu,					"se_fu", 					2, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_voice_dust,			"se_voice_dust", 			2, 0,	"攻守交替時の返しがベストの時"),
		new stSoundTable( eNo.se_voice_fook,			"se_voice_fook", 			2, 0,	"攻守交替時の返しがベストの時"),
        new stSoundTable( eNo.bgm_sea,                  "bgm_sea",                  2, 0,   "ゲーム中のBGM"),
        new stSoundTable( eNo.se_combo1,                "se_combo1",                2, 0,   "コンボ時の効果音"),
        new stSoundTable( eNo.se_combo2,                "se_combo2",                2, 0,   "コンボ時の効果音"),
        new stSoundTable( eNo.se_combo3,                "se_combo3",                2, 0,   "コンボ時の効果音"),
    };

	/// <summary>
	/// フォアハンドのＳＥの数
	/// </summary>
	public Dictionary<Shot_AdjustTable.eAdjust, int> foreShotCountList = new Dictionary<Shot_AdjustTable.eAdjust, int>()
	{
		{Shot_AdjustTable.eAdjust.POOR,		3},
		{Shot_AdjustTable.eAdjust.GOOD,		4},
		{Shot_AdjustTable.eAdjust.PERFECT,	1}
	};

	/// <summary>
	/// データテーブル
	/// </summary>
	struct stSoundTable
	{
		public eNo no;
		public string fileName;

		public int trackNo;
		public int priority;

		public string comment;

		public stSoundTable(eNo p_No, string p_FileName, int p_TrackNo, int p_Priority, string p_Comment)
		{
			no = p_No;
			fileName = p_FileName;
			trackNo = p_TrackNo;
			priority = p_Priority;
			comment = p_Comment;
		}
	};


	Dictionary<int, AudioClip> m_Clips = new Dictionary<int, AudioClip>();		// = new Dictionary<int, AudioClip>();

	static SoundHelper s_Instance;
	public static SoundHelper instance
	{
		get
		{
			if (s_Instance == null)
			{
				Create();
			}
			return s_Instance;
		}
	}

	public static SoundHelper Create()
	{
		if (s_Instance != null)
		{
			return s_Instance;
		}

		GameObject obj = new GameObject("SoundHelper");
		s_Instance = obj.AddComponent<SoundHelper>();

		return s_Instance;

	}

	// テストモード関連
	int m_nTestNo = 1;
	bool m_bTestMode = true;
	bool testmode
	{
		get { return m_bTestMode; }
		set { m_bTestMode = value; }
	}

	bool m_bInitialized;
	public bool isReady
	{
		get { return m_bInitialized; }
	}

	/// <summary>
	/// Awake
	/// </summary>
	void Awake()
	{
		if ((s_Instance != null) && (s_Instance != this))
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		m_bInitialized = false;

		//		TADebug.Create();
		//		TADebug.GUIDrawDelegateAppend("Sound", OnGUISoundTest);

		s_Instance = this;
	}

	/// <summary>
	/// Start
	/// </summary>
	/// <returns></returns>
	IEnumerator Start()
	{
		yield return 0;
		foreach (stSoundTable table in c_SoundTable)
		{
			if (table.fileName != null)
			{
				AudioClip clip = ResourceManager.AudioClipLoad(table.fileName, false);
				m_Clips.Add((int)table.no, clip);
//				Debug.Log("AudioClip Loaded = " + table.fileName);
			}
		}
		yield return 0;

		SoundManager.instance.clips = m_Clips;

		m_bInitialized = true;
	}

	// Update is called once per frame
	void Update()
	{

	}

	/// <summary>
	/// 全サウンド停止
	/// </summary>
	public void StopAll()
	{
		BgmStop();
		SeStop();
	}

	/// <summary>
	/// SE再生
	/// </summary>
	/// <param name="p_No"></param>
	public void SePlay(eNo p_No)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false);
	}

	public void	SePlay(eNo p_No, bool p_bLoop, float p_fVolume)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false, false, p_fVolume);
	}

	public void SePlay(eNo p_No, bool p_bLoop)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false, p_bLoop);
	}

	public void SePlay(eNo p_No, float p_Pitch)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, p_Pitch, false);
	}

	public void SePlay(eNo p_No, float p_Pitch, bool p_bMultiplex)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, p_Pitch, p_bMultiplex);
	}

	public void SeStop()
	{
		SoundManager.instance.SeStop();
	}

	public void BgmPlay(eNo p_No)
	{
		SoundManager.instance.BgmPlay((int)p_No, true);
	}

	public void BgmPlay(eNo p_No, bool p_Loop)
	{
		SoundManager.instance.BgmPlay((int)p_No, p_Loop);
	}

	public void BgmPlay(eNo p_No, bool p_Loop, float p_fVol)
	{
		SoundManager.instance.BgmPlay((int)p_No, p_Loop, p_fVol);
	}

	public void BgmStop(float p_FadeTime = 0)
	{
		SoundManager.instance.BgmStop(p_FadeTime);
	}

	public void	BgmVolumeSet(float p_fVol)
	{
		SoundManager.instance.BgmVolumeSet(p_fVol);
	}

	#region FOR DEBUG

	Rect m_TestWinRect = new Rect(0, 0, 480, 320);

	/// <summary>
	/// サウンドテストモード
	/// </summary>
	public void OnGUISoundTest(int p_nWindowID)
	{
		m_TestWinRect = GUI.Window(p_nWindowID, m_TestWinRect, drawGUI, "Sound Test");
	}

	public void drawGUI(int p_WindowID)
	{

		GUILayout.BeginVertical("box");

		GUILayout.BeginHorizontal("box");

		if (GUILayout.Button(" < "))
		{
			m_nTestNo--;
			if (m_nTestNo <= 0)
			{
				m_nTestNo = c_SoundTable.Length - 1;
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button("PLAY"))
		{
			if (c_SoundTable[m_nTestNo].trackNo == 0)
			{
				BgmPlay((eNo)m_nTestNo);
			}
			else
			{
				SePlay((eNo)m_nTestNo);
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button("STOP"))
		{
			if (c_SoundTable[m_nTestNo].trackNo == 0)
			{
				BgmStop();
			}
			else
			{
				SeStop();
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button(" > "))
		{
			m_nTestNo++;
			if (m_nTestNo >= c_SoundTable.Length)
			{
				m_nTestNo = 1;
			}
		}

		GUILayout.EndHorizontal();


		GUILayout.BeginHorizontal("box");
		GUILayout.Label(c_SoundTable[m_nTestNo].fileName);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal("box");
		GUILayout.Label(c_SoundTable[m_nTestNo].comment);
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();

		GUI.DragWindow(new Rect(0, 0, 300, 150));
	}

	#endregion

}
