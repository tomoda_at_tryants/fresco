using UnityEngine;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
    internal class DummyClient : IGoogleMobileAdsBannerClient, IGoogleMobileAdsInterstitialClient
    {
        public DummyClient(IAdListener listener)
        {
            DebugManager.Log("Created DummyClient");
        }

        public void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position)
        {
            DebugManager.Log("Dummy CreateBannerView");
        }

        public void LoadAd(AdRequest request)
        {
            DebugManager.Log("Dummy LoadAd");
        }

        public void ShowBannerView()
        {
            DebugManager.Log("Dummy ShowBannerView");
        }

        public void HideBannerView()
        {
            DebugManager.Log("Dummy HideBannerView");
        }

        public void DestroyBannerView()
        {
            DebugManager.Log("Dummy DestroyBannerView");
        }

        public void CreateInterstitialAd(string adUnitId) {
            DebugManager.Log("Dummy CreateIntersitialAd");
        }

        public bool IsLoaded() {
            DebugManager.Log("Dummy IsLoaded");
            return true;
        }

        public void ShowInterstitial() {
            DebugManager.Log("Dummy ShowInterstitial");
        }

        public void DestroyInterstitial() {
            DebugManager.Log("Dummy DestroyInterstitial");
        }
    }
}
